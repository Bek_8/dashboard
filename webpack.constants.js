"use strict";

const _ = require('lodash');
const path = require('path');
const util = require('util');

const DEV = process.env.NODE_ENV === 'development';

const ROOT = path.resolve();

const PORT = 8080;
const HOST = 'localhost';

const PUBLIC_PATH = '/static/';
const INPUT_PATH = path.join(ROOT, 'src/assets');
const OUTPUT_PATH = path.join(ROOT, PUBLIC_PATH);


const DEV_PORT = PORT + 1;

const DEV_HOST = util.format('http://%s:%d', HOST, DEV_PORT);
const DEV_SERVER = util.format('%s/webpack-dev-server.js', DEV_HOST);

module.exports = {
    ROOT: ROOT,

    HOST: HOST,
    PORT: PORT,

    INPUT_PATH: INPUT_PATH,
    PUBLIC_PATH: PUBLIC_PATH,
    OUTPUT_PATH: OUTPUT_PATH,

    DEV: DEV,
    DEV_PORT: DEV_PORT,
    DEV_HOST: DEV_HOST,
    DEV_SERVER: DEV_SERVER
};