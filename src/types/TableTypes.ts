import React from "react";

export interface ITableCol {
    label: string | React.ReactElement,
    fieldKey: string,
    sortable: boolean,
    padding?: 'none' | 'default' | number,
    align: "left" | "right" | "center",
    width: number | string,
    cellRenderer: (v: any) => React.ReactElement
}