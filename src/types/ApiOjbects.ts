import SortOrder from "./SortOrder";

export interface ListingObj<T> {
    list: T[],
    count: number
}

export interface ListingFilter {
    start?: number,
    limit?: number,
    sortField?: string,
    sortType?: SortOrder.ASC | SortOrder.DESC,
    searchKey?: string,
    filterKey?: string
}