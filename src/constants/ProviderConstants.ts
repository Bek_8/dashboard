export const PROVIDER_ITEM_TABS = [
    {
        id: 0,
        name: "Детали",
        value: "details",
        disabled: false
    },
    {
        id: 1,
        name: "Сервисы",
        value: "services",
        disabled: false
    },
    {
        id: 2,
        name: "Специалисты ",
        value: "technicians",
        disabled: false
    }
];
