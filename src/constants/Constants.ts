import SelectItem from "../types/SelectItem";


export const DEV = process.env.NODE_ENV === 'development';

export const HttpConfig: any = {
    API_VERSION: "v1/",
    STORE_VERSION: "1.0.0",
    API_PATH: "https://api.avtobio.uz/api/backend/v1",
    BASE_URL: "https://api.avtobio.uz",
};


// if (DEV) {
//     HttpConfig.API_PATH ="https://api.autobio.tk/api/backend";
//     HttpConfig.BASE_URL = "https://api.autobio.tk/";
// }

export const PAGE_LIMIT = 25;
export const PAGE_ROWS_LIMIT_LIST = [5, 10, 25, 50];

export const DEFAULT_FILTER = {start: 0, limit: PAGE_LIMIT}

export const STATUS_OPTIONS: SelectItem[] = [
    {id: 1, name: "Активный", code: "ACTIVE"},
    {id: 2, name: "Заблокированные", code: "BLOCKED"},
    {id: 2, name: "В ожидании", code: "PENDING"},
]

export const OIL_STATION_TABS = [
    {
        id: 0,
        name: "Категория",
        value: "category",
        disabled: false
    },
    {
        id: 1,
        name: "Марки",
        value: "makes",
        disabled: false
    },
];
export const AUTO_SERVICE_TABS = [
    {
        id: 0,
        name: "Категория",
        value: "category",
        disabled: false
    },
    {
        id: 1,
        name: "Сервисы",
        value: "services",
        disabled: false
    },
];

export const MAKE_MODELS_TABS = [
    {
        id: 0,
        name: "Категория",
        value: "category",
        disabled: false
    },
    {
        id: 1,
        name: "Модель",
        value: "models",
        disabled: false
    },
];

export const MAKE_MODIFICATIONS_TABS = [
    {
        id: 0,
        name: "Категория",
        value: "category",
        disabled: false
    },
    {
        id: 1,
        name: "Модификация",
        value: "modifications",
        disabled: false
    },
];


export const ReferenceParentCode={

}
