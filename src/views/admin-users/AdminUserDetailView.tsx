import React, {Component} from "react";
import CardComponent from "../../widgets/CardComponent";
import AdminUserDetailForm from "./AdminUserDetailForm";

export default class AdminUserDetailView extends Component {

    render() {
        return (
            <CardComponent title="Редактирование админа">
                <AdminUserDetailForm/>
            </CardComponent>
        )
    }
}

