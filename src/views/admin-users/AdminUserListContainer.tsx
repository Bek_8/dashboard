import React, {Fragment, useEffect} from "react";
import {useDispatch, useSelector} from "react-redux";
import * as serviceActions from "../../redux/actions/ServiceActions";
import {Card} from "@material-ui/core";

import IconButton from "@material-ui/core/IconButton";
import DeleteIcon from "@material-ui/icons/Delete";
import EditIcon from "@material-ui/icons/Edit";
import {Link, useHistory, useLocation, useParams} from "react-router-dom";
import {setDeleteConfirmationItem} from "../../redux/actions/MessageActions";
import DataTable, {DataCol} from "../../components/table-core/DataTable";
import * as H from "history";
import {updateQuery} from "../../utils/UrlUtils";
import useQueryFilter from "../../hooks/useQueryFilter";

import {createSelector} from 'reselect'
import * as adminUserActions from "../../redux/actions/AdminUserActions";

import CardComponent from "../../widgets/CardComponent";

const getListingReducers = createSelector(
    (state: any) => state.adminUser,
    ({list, count, loader}) => ({list, count, loader})
);
export default function AdminUserListContainer(props: any) {
    const params = useParams()
    const dispatch = useDispatch()
    const location: H.Location = useLocation();
    const filter = useQueryFilter(params)
    const history: H.History = useHistory()
    const {list, count, loader} = useSelector(getListingReducers)

    useEffect(() => {
        dispatch(adminUserActions.listAction(filter))
    }, [filter])

    return (
        <CardComponent title="Администраторы">
            <DataTable
                listing={{list, count}}
                filter={filter}
                loader={loader}
                addLink={'/admin-user/add'}
                onDeleteItem={(itemId) => dispatch(adminUserActions.deleteItemAction(itemId))}
                getList={(filter) => dispatch(adminUserActions.listAction(filter))}
                onFilterChange={query => history.push(updateQuery(location, query))}
            >
                <DataCol label={"ID"}
                         sortable={true}
                         fieldKey={"id"}
                         cellRenderer={row =>
                             <Link to={`/admin-user/${row.get("id")}`}>{row.get("id")}</Link>}/>

                <DataCol label={"Имя"}
                         sortable={true}
                         fieldKey={"firstName"}
                         cellRenderer={row =>
                             <Link to={`/admin-user/${row.get("id")}`}>{row.get("firstName")}</Link>}/>

                <DataCol label={"Фамилия"}
                         sortable={true}
                         fieldKey={"lastName"}
                         cellRenderer={row => row.get("lastName")}/>

                <DataCol label={"Телефон"}
                         fieldKey={"phone"}
                         cellRenderer={row => row.get("phone")}/>

                <DataCol label={"Эл. адрес"}
                         fieldKey={"email"}
                         cellRenderer={row => row.get("email")}/>

                <DataCol label={"Роль"}
                         fieldKey={"userRole"}
                         cellRenderer={row => row.get("userRole")}/>


                <DataCol label={"Действия"}
                         width={100}
                         cellRenderer={row => (
                             <Fragment>
                                 <IconButton aria-label="Редактировать"
                                             component={Link}
                                             to={`/admin-user/${row.get("id")}`}
                                             size="small">
                                     <EditIcon/>
                                 </IconButton>
                                 <IconButton aria-label="Удалить"
                                             onClick={() => dispatch(setDeleteConfirmationItem(row))}
                                             size="small">
                                     <DeleteIcon/>
                                 </IconButton>
                             </Fragment>
                         )}/>


            </DataTable>
        </CardComponent>
    )
}
