import React, {useEffect} from "react";
import * as adminUserActions from "../../redux/actions/AdminUserActions";
import {useDispatch} from "react-redux";
import {goBack} from "react-router-redux";
import AdminUserSaveForm from "../../components/user-core/AdminUserSaveForm";
import CardComponent from "../../widgets/CardComponent";


function AdminUserAddContainer(props: any) {
    const dispatch = useDispatch();
    const id = props.match.params.id;

    useEffect(() => {
        function fetchData() {
            dispatch(adminUserActions.itemAction(id));
        }

        if (id) fetchData();

        return () => {
            dispatch(adminUserActions.clearItem());
        }
    }, [id]);

    function submitForm(values) {
        dispatch(adminUserActions.saveItem(values));
    }

    return (
        <CardComponent title="Добавить администратора">

            <AdminUserSaveForm
                handleBack={() => dispatch(goBack())}
                submitForm={submitForm}
                initialValues={{
                    firstName:''
                }}
            />

        </CardComponent>
    );
}

export default AdminUserAddContainer;
