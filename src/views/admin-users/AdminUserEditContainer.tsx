import React, {useEffect} from "react";
import * as adminUserActions from "../../redux/actions/AdminUserActions";
import {useDispatch, useSelector} from "react-redux";
import {goBack} from "react-router-redux";
import {createSelector} from "reselect";
import AdminUserSaveForm from "../../components/user-core/AdminUserSaveForm";
import CardComponent from "../../widgets/CardComponent";


const getServiceReducers = createSelector(
    (state: any) => state.adminUser,
    (adminUser: any) => adminUser.item
);
function AdminUserEditContainer(props: any) {
    const dispatch = useDispatch();
    const item = useSelector(getServiceReducers);
    const id = props.match.params.id;
    useEffect(() => {
        function fetchData() {
            dispatch(adminUserActions.itemAction(id));
        }
        if (id) fetchData();
        return () => {
            dispatch(adminUserActions.clearItem());
        }
    }, [id]);

    function submitForm(values) {
        dispatch(adminUserActions.saveItem(values));
    }
    return (
        <CardComponent title="Изменить администратора">

            <AdminUserSaveForm
                handleBack={() => dispatch(goBack())}
                submitForm={submitForm}
                initialValues={item}
            />
        </CardComponent>
    );
}

export default AdminUserEditContainer;
