import React from "react";
import * as PropTypes from "prop-types";
import {reduxForm} from "redux-form";
import TextField from "@material-ui/core/TextField";
import makeStyles from "@material-ui/core/styles/makeStyles";
import DateFnsUtils from '@date-io/date-fns';

import {KeyboardDatePicker, MuiPickersUtilsProvider,} from '@material-ui/pickers'
import Grid from "@material-ui/core/Grid";
import InputLabel from "@material-ui/core/InputLabel";
import FormControl from "@material-ui/core/FormControl";
import Select from "@material-ui/core/Select";
import OutlinedInput from "@material-ui/core/OutlinedInput";
import MenuItem from "@material-ui/core/MenuItem";
import Button from "@material-ui/core/Button";

const useStyles = makeStyles(theme => ({
    container: {
        display: 'flex',
        flexWrap: 'wrap',
    },
    textField: {
        marginLeft: theme.spacing(1),
        marginRight: theme.spacing(1),
    },
    dense: {
        marginTop: theme.spacing(2),
    },
    menu: {
        width: 200,
    },
    formControl: {
        width: "100%",

    },
    button: {
        marginLeft: theme.spacing(1)
    },
    grid: {
        textAlign: "right"
    }
}));

AdminUserDetailForm.propTypes = {
    saveHandler: PropTypes.func.isRequired,
    closeHandler: PropTypes.func.isRequired
};

function AdminUserDetailForm(props:any) {
    const classes = useStyles(props);
    const {saveHandler, closeHandler} = props;

    const inputLabel = React.useRef(null);
    const [labelWidth, setLabelWidth] = React.useState(0);
    const [date, setDate] = React.useState(null);
    React.useEffect(() => {
        setLabelWidth(inputLabel.current.offsetWidth);
    }, []);


    function setDatePickerValue(value) {
        console.log(value);
        setDate(value);
    }

    return (
        <React.Fragment>
            <Grid container spacing={3}>
                <Grid item xs={4}>
                    <TextField
                        label="Фамилия"
                        className={classes.formControl}
                        margin="normal"
                        variant="outlined"
                    />
                </Grid>
                <Grid item xs={4}>
                    <TextField
                        label="Имя"
                        className={classes.formControl}
                        margin="normal"
                        variant="outlined"
                    />
                </Grid>
                <Grid item xs={4}>
                    <TextField
                        label="Отчество"
                        className={classes.formControl}
                        margin="normal"
                        variant="outlined"
                    />
                </Grid>

                <Grid item xs={6}>
                    <TextField
                        label="Электронная почта"
                        className={classes.formControl}
                        margin="normal"
                        variant="outlined"
                    />
                </Grid>

                <Grid item xs={6}>
                    <TextField
                        label="Телефон"
                        className={classes.formControl}
                        margin="normal"
                        variant="outlined"
                    />
                </Grid>

                <Grid item xs={4}>
                    <MuiPickersUtilsProvider utils={DateFnsUtils}>
                        <KeyboardDatePicker
                            className={classes.formControl}
                            disableToolbar
                            format="dd/MM/yyyy"
                            margin="normal"
                            label="Дата рождения"
                            value={date}
                            onChange={(value:any) => setDatePickerValue(value)}
                            KeyboardButtonProps={{
                                'aria-label': 'change date',
                            }}
                            InputProps={{
                                variant: "outlined"
                            } as any}
                        />
                    </MuiPickersUtilsProvider>
                </Grid>
                <Grid item xs={4}>
                    <FormControl variant="outlined" className={classes.formControl} margin="normal">
                        <InputLabel ref={inputLabel} htmlFor="outlined-age-simple">
                            Статус
                        </InputLabel>
                        <Select
                            input={
                                <OutlinedInput
                                    name="age"
                                    labelWidth={labelWidth}/>
                            }>
                            <MenuItem value={10}>Активный</MenuItem>
                            <MenuItem value={20}>Не активный</MenuItem>
                            <MenuItem value={30}>Заблокирован</MenuItem>
                        </Select>
                    </FormControl>
                </Grid>
                <Grid item xs={4}>
                    <TextField
                        label="Отчество"
                        className={classes.formControl}
                        margin="normal"
                        variant="outlined"
                    />
                </Grid>

                <Grid item xs={12} className={classes.grid}>
                    <Button
                        color="primary"
                        variant="outlined"
                        className={classes.button}
                        onClick={saveHandler}>
                        Сохранить
                    </Button>
                    <Button
                        variant="outlined"
                        onClick={closeHandler}
                        className={classes.button}>
                        Закрыть
                    </Button>
                </Grid>
            </Grid>
        </React.Fragment>
    )
}
export default reduxForm({
    form: "AdminUserDetailForm",
    enableReinitialize: true,

})(AdminUserDetailForm);

