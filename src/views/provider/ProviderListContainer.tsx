import React, {Fragment, useEffect, useState} from "react";
import CardComponent from "../../widgets/CardComponent";
import {useDispatch, useSelector} from "react-redux";
import * as providerActions from "../../redux/actions/ProviderActions";
import {Button, Card} from "@material-ui/core";

import IconButton from "@material-ui/core/IconButton";
import DeleteIcon from "@material-ui/icons/Delete";
import EditIcon from "@material-ui/icons/Edit";
import DepositIcon from "@material-ui/icons/AccountBalanceWallet";
// import ReportIcon from "@material-ui/icons/FileCopy";
import ListingTable from "../../components/table-core/ListingTable";
import {Link, useHistory, useLocation, useParams} from "react-router-dom";
import {setDeleteConfirmationItem} from "../../redux/actions/MessageActions";
import AddIcon from "@material-ui/core/SvgIcon/SvgIcon";
import DataTable, {DataCol} from "../../components/table-core/DataTable";
import * as H from "history";
import {updateQuery} from "../../utils/UrlUtils";
import useQueryFilter from "../../hooks/useQueryFilter";

import {createSelector} from 'reselect'

const getProviderReducers = createSelector(
    (state: any) => state.provider,
    ({list, count, loader}) => ({list, count, loader})
);
export default function ProviderListContainer(props: any) {
    const params = useParams()
    const dispatch = useDispatch()
    const location: H.Location = useLocation();
    const filter = useQueryFilter(params)
    const history: H.History = useHistory()
    const {list, count, loader} = useSelector(getProviderReducers)

    useEffect(() => {
        dispatch(providerActions.getProviderList(filter))
    }, [filter])

    return (
        <CardComponent title="Поставщик">
            <DataTable
                listing={{list, count}}
                title="Поставщик"
                filter={filter}
                loader={loader}
                addLink={'/provider/add'}
                getList={(filter) => dispatch(providerActions.getProviderList(filter))}
                onDeleteItem={(itemId) => dispatch(providerActions.deleteItemAction(itemId))}
                onFilterChange={query => history.push(updateQuery(location, query))}
            >
                <DataCol label={"ID"}
                         sortable={true}
                         fieldKey={"id"}
                         cellRenderer={row => row.get("id")}/>

                <DataCol label={"Название"}
                         sortable={true}
                         fieldKey={"name"}
                         cellRenderer={row => row.get("name") || row.get("providerName")}/>

                <DataCol label={"Категория"}
                         fieldKey={"categoryName"}
                         cellRenderer={row => row.get("categoryName")}/>

                <DataCol label={"Специалисты"}
                         fieldKey={"technicianCount"}
                         cellRenderer={row => row.get("technicianCount", 0)}/>

                <DataCol label={"Текущий депозит"}
                         fieldKey={"currentDeposit"}
                         cellRenderer={row => row.get("currentDeposit")}/>

                <DataCol label={"Статус"}
                         fieldKey={"status"}
                         cellRenderer={row => row.get("status")}/>


                <DataCol
                    label={"Действия"}
                    width={160}
                         cellRenderer={row => (
                             <Fragment>
                                 <IconButton aria-label="Удалить"
                                             onClick={() => dispatch(setDeleteConfirmationItem(row))}
                                             size="small">
                                     <DeleteIcon/>
                                 </IconButton>
                                 <IconButton aria-label="Deposit history"
                                             component={Link}
                                             to={`/provider/${row.get("id")}/deposit-history`}
                                             size="small">
                                     <DepositIcon/>
                                 </IconButton>
                                 <IconButton aria-label="Редактировать"
                                             component={Link}
                                             to={`/provider/${row.get("id")}`}
                                             size="small">
                                     <EditIcon/>
                                 </IconButton>
                             </Fragment>
                         )}/>
            </DataTable>
        </CardComponent>
    )
}
