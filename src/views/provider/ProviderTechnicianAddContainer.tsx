import React, {useEffect} from "react";
import * as technicianActions from "../../redux/actions/TechnicianActions";
import {useDispatch, useSelector} from "react-redux";
import {goBack} from "react-router-redux";
import TechnicianSaveForm from "../../components/technician-core/TechnicianSaveForm";
import {createSelector} from "reselect";
import * as serviceActions from "../../redux/actions/ServiceActions";
import * as providerActions from "../../redux/actions/ProviderActions";


const getServiceReducers = createSelector(
    (state: any) => state.technician,
    (technician: any) => technician.item
);

function ProviderTechnicianAddContainer(props: any) {
    const dispatch = useDispatch();
    const id = props.match.params.id;
    const item = useSelector(getServiceReducers);
    const providerId = +props.match.params.providerId;

    useEffect(() => {
        function fetchData() {
            Promise.all([
                // dispatch(technicianActions.itemAction(id)),
                dispatch(providerActions.getProviderItemAction(providerId)),
            ])
        }

        fetchData();

        return () => {
            dispatch(technicianActions.clearItem());
        }
    }, [id]);

    function submitForm(values) {
        dispatch(technicianActions.saveItem(values));
    }

    return (
        <div>
            <TechnicianSaveForm
                handleBack={() => dispatch(goBack())}
                submitForm={submitForm}
                initialValues={{...item, providerId}}
            />
        </div>
    );
}

export default ProviderTechnicianAddContainer;
