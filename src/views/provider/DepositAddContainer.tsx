import React, {useEffect} from "react";
import * as depositActions from "../../redux/actions/DepositActions";
import {useDispatch} from "react-redux";
import DepositSaveForm from "../../components/deposit-core/DepositSaveForm";
import {goBack} from "react-router-redux";


function DepositAddContainer(props: any) {
    const dispatch = useDispatch();
    const id = props.match.params.id;
    const providerId = props.match.params.providerId;

    useEffect(() => {
        function fetchData() {
            dispatch(depositActions.getDepositItemAction(id));
        }

        if (id) fetchData();

        return () => {
            dispatch(depositActions.clearItem());
        }
    }, [id]);

    function submitForm(values) {
        dispatch(depositActions.saveItem(values));
    }

    return (
        <div>
            <DepositSaveForm
                handleBack={() => dispatch(goBack())}
                submitForm={submitForm}
                initialValues={{
                    providerId
                }}
            />

        </div>
    );
}

export default DepositAddContainer;
