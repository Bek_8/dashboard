import React, {useEffect} from "react";
import * as serviceActions from "../../redux/actions/ServiceActions";
import * as providerActions from "../../redux/actions/ProviderActions";
import {useDispatch} from "react-redux";
import {goBack} from "react-router-redux";
import ServiceSaveForm from "../../components/service-core/ServiceSaveForm";
import CardComponent from "../../widgets/CardComponent";


function ProviderServiceAddContainer(props: any) {
    const dispatch = useDispatch();
    const id = props.match.params.id;
    const providerId = props.match.params.providerId;

    useEffect(() => {
        function fetchData() {
            Promise.all([
                // dispatch(serviceActions.itemAction(id)),
                dispatch(providerActions.getProviderItemAction(providerId)),
            ])
        }

        fetchData();

        return () => {
            dispatch(serviceActions.clearItem());
        }
    }, [id]);

    function submitForm(values) {
        dispatch(serviceActions.saveItem(values));
    }

    return (
        <CardComponent title={`Новый Сервис`}>
            <ServiceSaveForm
                handleBack={() => dispatch(goBack())}
                submitForm={submitForm}
                initialValues={{
                    providerId
                }}
            />
        </CardComponent>
    );
}

export default ProviderServiceAddContainer;
