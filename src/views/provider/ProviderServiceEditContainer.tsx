import React, {useEffect} from "react";
import * as serviceActions from "../../redux/actions/ServiceActions";
import {useDispatch, useSelector} from "react-redux";
import {goBack} from "react-router-redux";
import ServiceSaveForm from "../../components/service-core/ServiceSaveForm";
import {createSelector} from "reselect";
import * as providerActions from "../../redux/actions/ProviderActions";
import ProviderServicePriceList from "../../components/provider-core/ProviderServicePriceList";
import CardComponent from "../../widgets/CardComponent";


const getServiceReducers = createSelector(
    (state: any) => state.service,
    (service: any) => service.item
);

function ProviderServiceEditContainer(props: any) {
    const dispatch = useDispatch();
    const serviceItem = useSelector(getServiceReducers);
    const id = props.match.params.id;
    const providerId = props.match.params.providerId;


    useEffect(() => {
        function fetchData() {
            Promise.all([
                dispatch(serviceActions.itemAction(id)),
                dispatch(providerActions.getProviderItemAction(providerId)),
            ])
        }

        if (id) fetchData();

        return () => {
            dispatch(serviceActions.clearItem());
        }
    }, [id]);

    function submitForm(values) {
        dispatch(serviceActions.saveItem(values));
    }

    return (
        <CardComponent title={`Редактирование`}>
            <ServiceSaveForm
                handleBack={() => dispatch(goBack())}
                submitForm={submitForm}
                initialValues={serviceItem}
            />
            <ProviderServicePriceList/>
        </CardComponent>
    );
}

export default ProviderServiceEditContainer;
