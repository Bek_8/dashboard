import React, {useEffect} from "react";
import * as depositActions from "../../redux/actions/DepositActions";
import {useDispatch, useSelector} from "react-redux";
import DepositSaveForm from "../../components/deposit-core/DepositSaveForm";
import {goBack} from "react-router-redux";


import {createSelector} from 'reselect'

const getDepositReducers = createSelector(
    (state: any) => state.deposit,
    (deposit: any) => deposit.item
);

function DepositEditContainer(props: any) {
    const dispatch = useDispatch();
    const id = props.match.params.id;
    // const providerId = props.match.params.providerId;
    const item = useSelector(getDepositReducers)

    useEffect(() => {
        function fetchData() {
            dispatch(depositActions.getDepositItemAction(id));
        }

        if (id) fetchData();

        return () => {
            dispatch(depositActions.clearItem());
        }
    }, [id]);

    function submitForm(values) {
        dispatch(depositActions.saveItem(values));
        dispatch(goBack());
    }

    return (
        <div>
            <DepositSaveForm
                handleBack={() => dispatch(goBack())}
                submitForm={submitForm}
                initialValues={item}
            />

        </div>
    );
}

export default DepositEditContainer;
