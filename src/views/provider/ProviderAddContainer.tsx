import React, {useState} from "react";
import {useSelector, useDispatch} from "react-redux";
import CardComponent from "../../widgets/CardComponent";
import ProviderSaveDetailsForm from "../../components/provider-core/ProviderSaveDetailsForm";
import {saveItem} from "../../redux/actions/ProviderActions";

function ProviderAddContainer(props: any) {
    const dispatch = useDispatch();
    const providerItem = useSelector((state: any) => state.provider.item)

    return (
        <CardComponent title="Добавить нового поставщика ">
            <ProviderSaveDetailsForm
                initialValues={providerItem}
                submitForm={(data) => dispatch(saveItem(data))}
            />
        </CardComponent>
    );
}

export default ProviderAddContainer;
