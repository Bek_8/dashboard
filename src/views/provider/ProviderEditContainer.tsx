import React, {useEffect} from "react";
import fp from "lodash/fp"
import {useSelector, useDispatch} from "react-redux";
import CardComponent from "../../widgets/CardComponent";
import ProviderSaveDetailsForm from "../../components/provider-core/ProviderSaveDetailsForm";
import {getProviderItemAction, saveItem} from "../../redux/actions/ProviderActions";
import {getProviderTab} from "../../utils/ProviderUtils";
import ProviderItemTabs from "../../components/provider-core/ProviderItemTabs";
import ProviderServicesTab from "../../components/provider-core/ProviderServicesTab";
import ProviderTechniciansTab from "../../components/provider-core/ProviderTechniciansTab";

const hasProviderItem = fp.flow(fp.get("id"), Boolean);

function ProviderEditContainer(props: any) {
    const dispatch = useDispatch()
    const {providerId} = props.match.params;
    const providerItem = useSelector((state: any) => state.provider.item)

    useEffect(() => {
        dispatch(getProviderItemAction(providerId));
    }, [])

    const selectedTab = getProviderTab(props.location);

    return (
        <CardComponent title="Добавить нового поставщика ">
            <ProviderItemTabs
                selectedTab={selectedTab}
                location={props.location}
            />

            {hasProviderItem(providerItem) && selectedTab === "details" &&
            <ProviderSaveDetailsForm
                initialValues={providerItem}
                providerItem={providerItem}
                submitForm={(data) => dispatch(saveItem(data))}
            />}
            {selectedTab === "services" && <ProviderServicesTab/>}
            {selectedTab === "technicians" && <ProviderTechniciansTab/>}

        </CardComponent>
    );
}

export default ProviderEditContainer;
