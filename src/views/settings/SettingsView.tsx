import React from "react";
import CardComponent from "../../widgets/CardComponent";
import SettingsTheme from "../../widgets/SettingsTheme";
import {useDispatch} from "react-redux";
import {changeThemeColor} from "../../redux/actions/ChangeThemeColorActions";
export default function SettingsView(props:any) {


    const dispatch = useDispatch();
    return (
        <CardComponent title="Настройки">
            <SettingsTheme
                changeTheme = {(color:string) => (
                    dispatch(changeThemeColor(color))
                )}
            />
        </CardComponent>
    )
}
