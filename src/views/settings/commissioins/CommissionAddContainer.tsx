import React, {useEffect} from "react";
import * as action from "../../../redux/actions/CommissionActions";
import {useDispatch} from "react-redux";
import {goBack} from "react-router-redux";
import CommissionSaveForm from "../../../components/settings-core/CommissionSaveForm";


function CommissionAddContainer(props: any) {
    const dispatch = useDispatch();
    const id = props.match.params.id;

    useEffect(() => {
        function fetchData() {
            dispatch(action.getCommissionItemAction(id));
        }

        if (id) fetchData();

        return () => {
            dispatch(action.clearItem());
        }
    }, [id]);

    function submitForm(values) {
        dispatch(action.saveItem(values));
    }

    return (
        <div>
            <CommissionSaveForm
                handleBack={() => dispatch(goBack())}
                submitForm={submitForm}
            />

        </div>
    );
}

export default CommissionAddContainer;
