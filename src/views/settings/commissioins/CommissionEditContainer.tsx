import React, {useEffect} from "react";
import * as action from "../../../redux/actions/CommissionActions";
import {useDispatch, useSelector} from "react-redux";
import {goBack} from "react-router-redux";
import {createSelector} from "reselect";
import CommissionSaveForm from "../../../components/settings-core/CommissionSaveForm";


const getItemReducers = createSelector(
    (state: any) => state.commission,
    (stateSection: any) => stateSection.item
);

function CommissionEditContainer(props: any) {
    const dispatch = useDispatch();
    const item = useSelector(getItemReducers);
    const id = props.match.params.id;

    useEffect(() => {
        function fetchData() {
            dispatch(action.getCommissionItemAction(id));
        }

        if (id) fetchData();

        return () => {
            dispatch(action.clearItem());
        }
    }, [id]);

    function submitForm(values) {
        dispatch(action.saveItem(values));
    }

    return (
        <div>
            <CommissionSaveForm
                handleBack={() => dispatch(goBack())}
                submitForm={submitForm}
                initialValues={item}
            />

        </div>
    );
}

export default CommissionEditContainer;
