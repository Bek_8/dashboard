import React from "react";
import {reduxForm} from "redux-form";
import * as PropTypes from "prop-types";
import DialogContent from "@material-ui/core/DialogContent";
import DialogActions from "@material-ui/core/DialogActions";
import Button from "@material-ui/core/Button";
import makeStyles from "@material-ui/core/styles/makeStyles";
import {required} from "../../utils/reduxFormatUtils";
import Dialog from "@material-ui/core/Dialog";
import IconButton from "@material-ui/core/IconButton";
import CloseIcon from "@material-ui/icons/Close";
import DialogTitle from "@material-ui/core/DialogTitle";
import FormTextInputField from "../../widgets/redux/FormTextInputField";

const useStyles = makeStyles(theme => ({
    headerColor: {
        backgroundColor: theme.palette.primary.main,
        display: "flex",
        justifyContent: "space-between",
        alignItems: "center",
        padding: theme.spacing(0, 2)
    },
    form: {
        width: "100%",
        marginTop: theme.spacing(1)
    },
    submit: {},
    actionsBlock: {
        margin: theme.spacing(2, 2, 2, 2),
    },
    
}));

FuelTypeEditForm.propTypes = {
    handleClose: PropTypes.func,
    genderTypes: PropTypes.array,
    statuses: PropTypes.array
};

function FuelTypeEditForm(props:any) {
    const classes = useStyles(props);
    const {
        pristine,
        submitting,
        handleClose,
        handleSubmit,
        submitForm,
        initialValues: {
            name
        }
    } = props;

    return (
        <Dialog
            disableBackdropClick={true}
            disableEscapeKeyDown={true}
            open={true}
            onClose={handleClose}>
            <DialogTitle disableTypography className={classes.headerColor}>
                {name && (
                    <h3>Редактирование {name}</h3>
                )}
                {!name && (
                    <h3>Новый</h3>
                )}

                <IconButton onClick={handleClose}>
                    <CloseIcon/>
                </IconButton>
            </DialogTitle>
            <form className={classes.form} noValidate onSubmit={handleSubmit(submitForm)}>
                <DialogContent>
                    <FormTextInputField
                        validate={required}
                        name="name"
                        InputProps={{
                            fullWidth: true,
                            required: true,
                            label: "Название",
                            variant: "outlined"
                        }}
                    />
                    <FormTextInputField
                        name="code"
                        InputProps={{
                            fullWidth: true,
                            required: true,
                            label: "Код",
                            variant: "outlined"
                        }}
                    />

                </DialogContent>
                <DialogActions className={classes.actionsBlock}>
                    <Button
                        disabled={submitting}
                        className={classes.submit}
                        onClick={handleClose}
                        color="secondary"
                        variant="outlined">
                        Закрыть
                    </Button>
                    <Button
                        disabled={pristine || submitting}
                        className={classes.submit}
                        type="submit"
                        color="primary"
                        variant="contained">
                        Сохранить
                    </Button>
                </DialogActions>
            </form>
        </Dialog>
    );
}

export default reduxForm({
    form: "FuelTypeEditForm",
    enableReinitialize: true
})(FuelTypeEditForm);