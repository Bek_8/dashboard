import React, {Fragment, useState} from "react";
import {connect} from "react-redux";
import {push} from "react-router-redux";
import Immutable from "immutable";

import Button from "@material-ui/core/Button/index";
import CardComponent from "../../widgets/CardComponent";
import IconButton from "@material-ui/core/IconButton/index";
import DeleteIcon from "@material-ui/icons/Delete";
import AddIcon from "@material-ui/icons/Add";
import EditIcon from "@material-ui/icons/Edit";

import * as fuelTypeActions from "../../redux/actions/FuelTypeActions";
import ListPanelComponent from "../../widgets/listPanel/ListPanelComponent";

import SweetAlert from "react-bootstrap-sweetalert";
import FuelTypeEditModalView from "./FuelTypeEditModalView";

function FuelTypeListView(props:any) {
    const {fuelType, location} = props;
    const [itemToDelete, setItemToDelete] = useState(null);

    const columns = [
        {
            columnCode: "action",
            columnName: "Действия",
            width: "100px",
            align: "left",
            value: item => {
                return (
                    <Fragment>
                        <IconButton aria-label="Удалить" onClick={() => handleDeleteButtonClick(item)}
                                    size="small">
                            <DeleteIcon/>
                        </IconButton>
                        <IconButton aria-label="Редактировать" onClick={() => handleEditButtonClick(item)}
                                    size="small">
                            <EditIcon/>
                        </IconButton>
                    </Fragment>
                );
            }
        },
        {
            columnCode: "NAME",
            columnName: "Название",
            sortable: true,
            align: "left",
            value: item => (item.get("name"))
        },
        {
            columnCode: "CODE",
            columnName: "Код",
            sortable: false,
            align: "left",
            value: item => (item.get("code"))
        }
    ];

    function handleEditButtonClick(item) {
        if (!item) {
            return
        }
        props.dispatch(push(`/fuel-type/${item.get("id")}#EV`));
    }

    function handleDeleteButtonClick(item) {
        if (!item) {
            return
        }
        setItemToDelete(item);
    }

    function getAddNewButton() {
        return (
            <Button
                variant="outlined"
                onClick={() => props.dispatch(push(`/fuel-type#EV`))}>
                <AddIcon color="primary"/>
                Добавить
            </Button>
        );
    }

    const getRequestProvider = (limit, start, searchKey, sortField, sortOrder, key) => {
        props.dispatch(fuelTypeActions.listAction({start, limit, searchKey, sortField, sortOrder}));
    };

    function deleteItem(itemToDelete) {
        if (!itemToDelete) {
            return;
        }
        setItemToDelete(null);
        props.dispatch(fuelTypeActions.deleteItemAction(itemToDelete.get("id")));
    }

    function renderSweetAlertBlock() {
        if (itemToDelete) {
            return (
                <SweetAlert
                    showCancel
                    title="Внимание"
                    type="warning"
                    confirmBtnBsStyle="danger"
                    cancelBtnBsStyle="default"
                    cancelBtnCssClass="MuiButtonBase-root MuiButton-root MuiButton-contained MuiButton-containedSecondary"
                    confirmBtnCssClass="MuiButtonBase-root MuiButton-root jss1086 MuiButton-contained MuiButton-containedPrimary"
                    onConfirm={() => deleteItem(itemToDelete)}
                    onCancel={() => setItemToDelete(null)}>
                    {`Вы действительно хотите удалить эту запись?`}
                </SweetAlert>
            );
        }
        return null;
    }

    function handleCloseEditButtonClick() {
        props.dispatch(push("/fuel-type"));
    }

    const sweetAlertBlock = renderSweetAlertBlock();

    const newButton = getAddNewButton();

    return (
        <CardComponent title="Виды топлива">
            {location.hash === "#EV" && (
                <FuelTypeEditModalView
                    {...props}
                    handleClose={() => handleCloseEditButtonClick()}
                />
            )}

            {sweetAlertBlock}
            <ListPanelComponent
                showDeleteModal={itemToDelete != null}
                loader={props.loader}
                list={fuelType.list}
                count={fuelType.count}
                addNewHandler={newButton}
                requestProvider={getRequestProvider}
                columnsConfig={Immutable.fromJS(columns)}
            />
        </CardComponent>
    );
}

export default connect(({fuelTypeListView, fuelType, loader}:any) => ({
    fuelTypeListView,
    fuelType,
    loader
}))(FuelTypeListView);
