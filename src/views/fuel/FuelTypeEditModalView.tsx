import React, {useEffect} from "react";
import {connect} from "react-redux";
import * as PropTypes from "prop-types";
import * as fuelTypeActions from "../../redux/actions/FuelTypeActions";
import FuelTypeEditForm from "./FuelTypeEditForm";

FuelTypeEditModalView.propTypes = {
    handleClose: PropTypes.func,
};

function FuelTypeEditModalView(props:any) {
    const {handleClose} = props;
    const {
        fuelType: {
            item: {
                id,
                name,
                code
            }
        },
        dispatch
    } = props;

    const viewId = props.match.params.view;

    useEffect(() => {
        const fetchData = (id) => {
            return dispatch(fuelTypeActions.itemAction(id));
        };

        fetchData(viewId);

        return () => {
            dispatch(fuelTypeActions.clearItem());
        }
    }, [dispatch, viewId]);

    function submitForm(values) {
        props.dispatch(fuelTypeActions.saveItem(values));
    }

    return (
        <FuelTypeEditForm
            submitForm={submitForm}
            handleClose={handleClose}
            initialValues={{
                id,
                name,
                code
            }}
        />
    );
}

export default connect(({fuelTypeEditModalView, fuelType}:any) => ({
    fuelTypeEditModalView,
    fuelType,
}))(FuelTypeEditModalView);
