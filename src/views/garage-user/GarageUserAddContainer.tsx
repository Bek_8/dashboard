import React, { useEffect } from "react";
import * as garageUserActions from "../../redux/actions/GarageUserActions";
import { useDispatch } from "react-redux";
import { goBack } from "react-router-redux";
import GarageUserSaveForm from "../../components/user-core/GarageUserSaveForm";
import CardComponent from "../../widgets/CardComponent";

function GarageUserAddContainer(props: any) {
  const dispatch = useDispatch();
  const id = props.match.params.id;

  useEffect(() => {
    function fetchData() {
      dispatch(garageUserActions.itemAction(id));
    }

    if (id) fetchData();

    return () => {
      dispatch(garageUserActions.clearItem());
    };
  }, [id]);

  function submitForm(values) {
    dispatch(garageUserActions.saveItem(values));
  }

  return (
    <CardComponent title="Добавить администратора">
      <GarageUserSaveForm
        handleBack={() => dispatch(goBack())}
        submitForm={submitForm}
        initialValues={{
          firstName: "",
        }}
      />
    </CardComponent>
  );
}

export default GarageUserAddContainer;
