import React, { Component, Fragment } from "react";
import Immutable from "immutable";
import { Link } from "react-router-dom";
import { push } from "react-router-redux";
import Button from "@material-ui/core/Button";
import AddIcon from "@material-ui/icons/Add";
import DeleteIcon from "@material-ui/icons/Delete";
import EditIcon from "@material-ui/icons/Edit";

import IconButton from "@material-ui/core/IconButton";
import * as garageUserActions from "../../redux/actions/GarageUserActions";
import ListPanelComponent from "../../widgets/listPanel/ListPanelComponent";
import CardComponent from "../../widgets/CardComponent";
import { connect } from "react-redux";
import SweetAlert from "react-bootstrap-sweetalert";

class GarageUserListView extends Component<any, any> {
  columns: any[] = [];

  constructor(props: any) {
    super(props);
    this.state = {
      list: Immutable.List(),
      count: 0,
      itemToDelete: null,
    };

    this.columns = [
      {
        columnCode: "action",
        columnName: "Действия",
        width: "100px",
        align: "left",
        value: (item) => {
          return (
            <Fragment>
              <IconButton
                aria-label="Удалить"
                onClick={() => this.handleDeleteButtonClick(item)}
              >
                <DeleteIcon />
              </IconButton>
              <IconButton
                aria-label="Редактировать"
                component={Link}
                to={`/garage-user/${item.get("id")}`}
              >
                <EditIcon />
              </IconButton>
            </Fragment>
          );
        },
      },
      {
        columnCode: "FIRST_NAME",
        columnName: "Имя",
        sortable: true,
        align: "left",
        value: (item) => {
          return (
            <Link to={`/garage-user/${item.get("id")}`}>
              {item.get("firstName")}
            </Link>
          );
        },
      },
      {
        columnCode: "LAST_NAME",
        columnName: "Фамилия",
        sortable: true,
        align: "left",
        value: (item) => {
          return item.get("lastName");
        },
      },
      {
        columnCode: "PHONE",
        columnName: "Телефон",
        sortable: false,
        align: "left",
        value: (item) => {
          return item.get("phone");
        },
      },
      {
        columnCode: "EMAIL",
        columnName: "Эл. адрес",
        sortable: false,
        align: "left",
        value: (item) => {
          return item.get("email");
        },
      },
      {
        columnCode: "ROLE",
        columnName: "Роль",
        sortable: false,
        align: "left",
        value: (item) => {
          return item.getIn(["userRole", "name"]);
        },
      },
    ];
  }

  handleEditButtonClick(item) {
    if (!item) {
      return;
    }
    this.props.dispatch(push(`/garage-user/${item.get("id")}`));
  }

  handleDeleteButtonClick(item) {
    if (!item) {
      return;
    }
    this.setState({ itemToDelete: item });
  }

  getAddNewButton = () => {
    return (
      <Button
        variant="outlined"
        onClick={() => this.props.dispatch(push("/garage-user/add"))}
      >
        <AddIcon color="primary" />
        Добавить
      </Button>
    );
  };

  getRequestProvider = (limit, start, searchKey, sortField, sortOrder, key) => {
    this.props.dispatch(
      garageUserActions.listAction({
        start,
        limit,
        searchKey,
        sortField,
        sortOrder,
      })
    );
  };

  deleteItem(itemToDelete) {
    if (!itemToDelete) {
      return;
    }
    this.setState({ itemToDelete: null });
    this.props.dispatch(
      garageUserActions.deleteItemAction(itemToDelete.get("id"))
    );
  }

  render() {
    const { itemToDelete } = this.state;
    const { garageUser } = this.props;
    const newButton = this.getAddNewButton();

    let sweetAlertBlock = null;

    if (itemToDelete) {
      sweetAlertBlock = (
        <SweetAlert
          showCancel
          title="Внимание"
          type="warning"
          confirmBtnBsStyle="danger"
          cancelBtnBsStyle="default"
          cancelBtnCssClass="MuiButtonBase-root MuiButton-root MuiButton-contained MuiButton-containedSecondary"
          confirmBtnCssClass="MuiButtonBase-root MuiButton-root jss1086 MuiButton-contained MuiButton-containedPrimary"
          onConfirm={() => this.deleteItem(itemToDelete)}
          onCancel={() => this.setState({ itemToDelete: null })}
        >
          {`Вы действительно хотите удалить эту запись?`}
        </SweetAlert>
      );
    }

    return (
      <CardComponent title="Пользователи">
        {sweetAlertBlock}
        <ListPanelComponent
          showDeleteModal={itemToDelete != null}
          ref="listComponent"
          loader={this.props.loader}
          list={garageUser.list}
          count={garageUser.count}
          addNewHandler={newButton}
          requestProvider={this.getRequestProvider}
          columnsConfig={Immutable.fromJS(this.columns)}
        />
      </CardComponent>
    );
  }
}

export default connect(({ garageUserListView, garageUser, loader }: any) => ({
  garageUserListView,
  garageUser,
  loader,
}))(GarageUserListView);
