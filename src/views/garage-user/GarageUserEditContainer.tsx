import React, { useEffect } from "react";
import * as garageUserActions from "../../redux/actions/GarageUserActions";
import { useDispatch, useSelector } from "react-redux";
import { goBack } from "react-router-redux";
import { createSelector } from "reselect";
import GarageUserSaveForm from "../../components/user-core/GarageUserSaveForm";
import CardComponent from "../../widgets/CardComponent";

const getServiceReducers = createSelector(
  (state: any) => state.garageUser,
  (garageUser: any) => garageUser.item
);
function GarageUserEditContainer(props: any) {
  const dispatch = useDispatch();
  const item = useSelector(getServiceReducers);
  const id = props.match.params.id;
  useEffect(() => {
    function fetchData() {
      dispatch(garageUserActions.itemAction(id));
    }
    if (id) fetchData();
    return () => {
      dispatch(garageUserActions.clearItem());
    };
  }, [id]);

  function submitForm(values) {
    dispatch(garageUserActions.saveItem(values));
  }
  return (
    <CardComponent title="Изменить администратора">
      <GarageUserSaveForm
        handleBack={() => dispatch(goBack())}
        submitForm={submitForm}
        initialValues={item}
      />
    </CardComponent>
  );
}

export default GarageUserEditContainer;
