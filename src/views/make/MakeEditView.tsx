import React, {useEffect} from "react";
import * as PropTypes from "prop-types";
import * as makeActions from "../../redux/actions/MakeActions";
import {connect} from "react-redux";
import MakeEditForm from "./MakeEditForm";
import Tabs from "@material-ui/core/Tabs";
import Tab from "@material-ui/core/Tab";
import AppBar from "@material-ui/core/AppBar";
import {Link} from "react-router-dom";
import ModelListView from "./model/ModelListView";
import {push} from "react-router-redux";
import {isNumber} from "../../utils/DataUtils";

MakeEditView.propTypes = {
    handleClose: PropTypes.func
};

function MakeEditView(props:any) {
    const {dispatch} = props;
    const viewId = props.match.params.view;
    const tab = props.match.params.tab || "make";

    const tabsLabels = [
        {
            id: 0,
            name: "Марка",
            value: "make",
            url: `/make/item/${props.match.params.view}/`,
            disabled: !viewId
        }
    ];
    if (viewId) {
        tabsLabels.push({
            id: 1,
            name: "Модели",
            value: "model",
            url: `/make/item/${props.match.params.view}/model`,
            disabled: false
        })
    }

    const {
        make: {
            item: {
                id,
                name,
                code
            }
        }
    } = props;

    useEffect(() => {
        function fetchData() {
            dispatch(makeActions.itemAction(viewId));
        }

        fetchData();

        return () => {
            dispatch(makeActions.clearItem());
        }
    }, [dispatch, viewId]);

    function submitForm(values) {
        props.dispatch(makeActions.saveItem(values));
    }

    function handleClose() {
        dispatch(push(`/make`));
    }

    return (
        <div>
            {viewId &&
                <AppBar position="static" color="default">
                    <Tabs
                        value={tab}
                        indicatorColor="primary"
                        textColor="primary"
                        variant="scrollable"
                        scrollButtons="auto">
                        {tabsLabels.map(tab => (
                            <Tab
                                disabled={tab.disabled}
                                label={tab.name}
                                key={tab.id}
                                component={Link}
                                to={tab.url}
                                value={tab.value}
                            />
                        ))}

                    </Tabs>
                </AppBar>}

            {(!tab || tab === "make") && (
                <MakeEditForm
                    handleClose={() => handleClose()}
                    submitForm={submitForm}
                    initialValues={{
                        id, name, code
                    }}
                />
            )}
            {isNumber(viewId) && tab === "model" && (
                <ModelListView
                    makeName={name}
                    makeId={viewId}
                    handleClose={() => handleClose()}
                />

            )}

        </div>
    );
}

export default connect(({makeEditView, make}:any) => ({
    makeEditView,
    make,
}))(MakeEditView);
