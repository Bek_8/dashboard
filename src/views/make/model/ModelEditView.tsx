import React, {useEffect} from "react";
import {push} from "react-router-redux";
import {connect} from "react-redux";
import {Link} from "react-router-dom";
import AppBar from "@material-ui/core/AppBar";
import Tabs from "@material-ui/core/Tabs";
import Tab from "@material-ui/core/Tab";

import ModelEditForm from "./ModelEditForm";
import * as modelActions from "../../../redux/actions/ModelActions";
import ModificationListView from "../../modification/ModificationListView";
import {isNumber} from "../../../utils/DataUtils";

function ModelEditView(props:any) {
    const {dispatch, location} = props;
    const viewId = props.match.params.view;
    const make = props.match.params.make;
    const tab = props.match.params.tab || "model";

    const tabsLabels = [
        {
            id: 0,
            name: "Модель",
            value: "model",
            url: `/model/item/${props.match.params.make}/${props.match.params.view}/`,
            disabled: !viewId
        }
    ];
    if (viewId) {
        tabsLabels.push({
            id: 1,
            name: "Модификации",
            value: "modification",
            url: `/model/item/${props.match.params.make}/${props.match.params.view}/modification`,
            disabled: false
        })
    }

    useEffect(() => {
        function fetchData() {
            dispatch(modelActions.itemAction(viewId));
        }

        fetchData();

        return () => {
            dispatch(modelActions.clearItem());
        }
    }, [dispatch, viewId]);

    function submitForm(values) {
        props.dispatch(modelActions.saveItem(values));
    }

    function handleClose() {
        dispatch(push(`/make/item/${make}/model`));
    }

    const {
        model: {
            item: {
                id,
                name,
                code,
                makeId
            }
        }
    } = props;

    console.log("isNumber: ", isNumber(viewId));
    return (
        <div>
            <AppBar position="static" color="default">
                <Tabs
                    value={tab}
                    indicatorColor="primary"
                    textColor="primary"
                    variant="scrollable"
                    scrollButtons="auto">
                    {tabsLabels.map(tab => (
                        <Tab
                            disabled={tab.disabled}
                            label={tab.name}
                            key={tab.id}
                            component={Link}
                            to={tab.url}
                            value={tab.value}
                        />
                    ))}

                </Tabs>
            </AppBar>
            {(!tab || tab === "model") && (
                <ModelEditForm
                    handleClose={() => handleClose()}
                    submitForm={submitForm}
                    initialValues={{
                        id, name, code, makeId: makeId || make
                    }}
                />
            )}
            {isNumber(viewId) && tab === "modification" && (
                <ModificationListView
                    {...props}
                    location={location}
                    modelName={name}
                    modelId={viewId}
                    handleClose={() => handleClose()}
                />

            )}
        </div>
    )
}

export default connect(({modelEditView, model}:any) => ({
    modelEditView,
    model,
}))(ModelEditView);
