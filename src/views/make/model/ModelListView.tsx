import React, {Fragment, useState} from "react";
import {connect} from "react-redux";
import Immutable from "immutable";
import * as PropTypes from "prop-types";

import SweetAlert from "react-bootstrap-sweetalert";
import IconButton from "@material-ui/core/IconButton/index";
import DeleteIcon from "@material-ui/icons/Delete";
import AddIcon from "@material-ui/icons/Add";
import EditIcon from "@material-ui/icons/Edit";
import Button from "@material-ui/core/Button/index";

import ListPanelComponent from "../../../widgets/listPanel/ListPanelComponent";
import CardComponent from "../../../widgets/CardComponent";
import * as modelActions from "../../../redux/actions/ModelActions";
import {push} from "react-router-redux";

ModelListView.propTypes = {
    handleClose: PropTypes.func,
    makeId: PropTypes.string.isRequired
};

function ModelListView(props:any) {
    const {model, makeName, makeId} = props;
    const [itemToDelete, setItemToDelete] = useState(null);

    const columns = [
        {
            columnCode: "action",
            columnName: "Действия",
            width: "100px",
            align: "left",
            value: item => {
                return (
                    <Fragment>
                        <IconButton aria-label="Удалить" onClick={() => handleDeleteButtonClick(item)}
                                    size="small">
                            <DeleteIcon/>
                        </IconButton>
                        <IconButton aria-label="Редактировать" onClick={() => handleEditButtonClick(item)}
                                    size="small">
                            <EditIcon/>
                        </IconButton>
                    </Fragment>
                );
            }
        },
        {
            columnCode: "NAME",
            columnName: "Название",
            sortable: true,
            align: "left",
            value: item => (item.get("name"))
        },
        {
            columnCode: "CODE",
            columnName: "Код",
            sortable: false,
            align: "left",
            value: item => (item.get("code"))
        }
    ];
    function handleEditButtonClick(item) {
        if (!item) {
            return
        }
        props.dispatch(push(`/model/item/${makeId}/${item.get("id")}`));
    }

    function handleDeleteButtonClick(item) {
        if (!item) {
            return
        }
        setItemToDelete(item);
    }

    function getAddNewButton() {
        return (
            <Button
                variant="outlined"
                onClick={() => props.dispatch(push(`/model/item/${makeId}`))}>
                <AddIcon color="primary"/>
                Добавить
            </Button>
        );
    }

    const getRequestProvider = (limit, start, searchKey, sortField, sortOrder, key) => {
        props.dispatch(modelActions.listAction({makeId, start, limit, searchKey, sortField, sortOrder}as any));
    };

    function deleteItem(itemToDelete) {
        if (!itemToDelete) {
            return;
        }
        setItemToDelete(null);
        props.dispatch(modelActions.deleteItemAction(itemToDelete.get("id")));
    }

    function renderSweetAlertBlock() {
        if (itemToDelete) {
            return (
                <SweetAlert
                    showCancel
                    title="Внимание"
                    type="warning"
                    confirmBtnBsStyle="danger"
                    cancelBtnBsStyle="default"
                    cancelBtnCssClass="MuiButtonBase-root MuiButton-root MuiButton-contained MuiButton-containedSecondary"
                    confirmBtnCssClass="MuiButtonBase-root MuiButton-root jss1086 MuiButton-contained MuiButton-containedPrimary"
                    onConfirm={() => deleteItem(itemToDelete)}
                    onCancel={() => setItemToDelete(null)}>
                    {`Вы действительно хотите удалить эту запись?`}
                </SweetAlert>
            );
        }
        return null;
    }

    const sweetAlertBlock = renderSweetAlertBlock();
    const newButton = getAddNewButton();

    return (
        <CardComponent title={`Марки ${makeName}`}>
            {sweetAlertBlock}
            <ListPanelComponent
                showDeleteModal={itemToDelete != null}
                loader={props.loader}
                list={model.list}
                count={model.count}
                addNewHandler={newButton}
                requestProvider={getRequestProvider}
                columnsConfig={Immutable.fromJS(columns)}
            />
            <Button
                variant="outlined"
                color="secondary"
                onClick={() => props.handleClose()}>
                Закрыть
            </Button>
        </CardComponent>
    );
}

export default connect(({modelListView, model, loader}:any) => ({
    modelListView,
    model,
    loader
}))(ModelListView);
