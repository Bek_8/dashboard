import React from "react";
import CardComponent from "../../../widgets/CardComponent";
import makeStyles from "@material-ui/core/styles/makeStyles";
import * as PropTypes from "prop-types";
import {reduxForm} from "redux-form";
import Grid from "@material-ui/core/Grid";
import FormTextInputField from "../../../widgets/redux/FormTextInputField";
import {isEmpty} from "../../../widgets/redux/Validator";
import Button from "@material-ui/core/Button";

const useStyles = makeStyles(theme => ({
    headerColor: {
        backgroundColor: theme.palette.primary.main,
        display: "flex",
        justifyContent: "space-between",
        alignItems: "center",
        padding: theme.spacing(0, 2)
    },
    form: {
        width: "100%", // Fix IE 11 issue.
        marginTop: theme.spacing(1)
    },
    submit: {
        margin: theme.spacing(0, 2, 0, 0),
    },
}));

ModelEditForm.propTypes = {
    handleClose: PropTypes.func
};

function ModelEditForm(props:any) {
    const classes = useStyles(props);
    const {
        pristine,
        submitting,
        handleClose,
        handleSubmit,
        submitForm,
        initialValues: {
            name
        }
    } = props;


    return (
        <CardComponent title={name ? `Редактирование ${name}` : "Новая марка"}>
            <form className={classes.form} noValidate onSubmit={handleSubmit(submitForm)}>
                <Grid
                    container
                    alignItems="flex-start"
                    spacing={3}>
                    <Grid item xs={12} sm={6}>
                        <FormTextInputField
                            validate={isEmpty}
                            name="name"
                            InputProps={{
                                fullWidth: true,
                                required: true,
                                label: "Наименование",
                                variant: "outlined"
                            } as any}
                        />
                    </Grid>
                    <Grid item xs={12} sm={6}>
                        <FormTextInputField
                            name="code"
                            InputProps={{
                                fullWidth: true,
                                label: "Код",
                                variant: "outlined"
                            } as any}
                        />
                    </Grid>
                    <Grid item>
                        <Button
                            disabled={pristine || submitting}
                            className={classes.submit}
                            type="submit"
                            color="primary"
                            variant="contained">
                            Сохранить
                        </Button>
                        <Button
                            disabled={submitting}
                            className={classes.submit}
                            onClick={handleClose}
                            color="secondary"
                            variant="outlined">
                            Закрыть
                        </Button>
                    </Grid>

                </Grid>
            </form>
        </CardComponent>
    )
}

export default reduxForm({
    form: "model",
    enableReinitialize: true
})(ModelEditForm);