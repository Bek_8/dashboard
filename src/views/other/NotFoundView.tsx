import React from "react";
import CardComponent from "../../widgets/CardComponent";
import makeStyles from "@material-ui/core/styles/makeStyles";
import Grid from "@material-ui/core/Grid";
import WarningIcon from '@material-ui/icons/Warning';
import Button from "@material-ui/core/Button";
import HomeOutlinedIcon from '@material-ui/icons/HomeOutlined';
import {Link} from "react-router-dom";

const useStyles = makeStyles(theme => ({
    grid: {
        minHeight: "80vh"
    },
    warningIcon: {
        fontSize: "150px"
    },
    button: {
        padding: theme.spacing(2, 10)
    }
}));

export default function NOtFoundView(props:any) {
    const classes = useStyles(props);

    return (
        <CardComponent>
            <Grid
                container
                spacing={0}
                direction="column"
                alignItems="center"
                justify="center"
                className={classes.grid}
            >
                <Grid item xs={3}>
                    <WarningIcon
                        color="secondary"
                        className={classes.warningIcon}/>
                </Grid>
                <h2>404 - Страница не найдена</h2>

                <Button
                    variant="contained"
                    color="primary"
                    component={Link}
                    to={"/"}
                    className={classes.button}>
                    <HomeOutlinedIcon/>
                    На главную
                </Button>
            </Grid>
        </CardComponent>
    )
}