import React, {Fragment, useState} from "react";
import Immutable from "immutable";
import IconButton from "@material-ui/core/IconButton";
import Button from "@material-ui/core/Button";
import DeleteIcon from "@material-ui/icons/Delete";
import EditIcon from "@material-ui/icons/Edit";
import AddIcon from "@material-ui/icons/Add";
import {Link} from "react-router-dom";
import CardComponent from "../../widgets/CardComponent";
import ListPanelComponent from "../../widgets/listPanel/ListPanelComponent";
import * as motoristActions from "../../redux/actions/MotoristUserActions";

import SweetAlert from "react-bootstrap-sweetalert";
import {connect} from "react-redux";
import {push} from "react-router-redux";
import MotoristEditModalView from "./MotoristEditModalView";


function MotoristListView(props:any) {
    const {motorist, location} = props;
    const [itemToDelete, setItemToDelete] = useState(null);

    const columns = [
        {
            columnCode: "action",
            columnName: "Действия",
            width: "100px",
            align: "left",
            value: item => {
                return (
                    <Fragment>
                        <IconButton aria-label="Удалить" onClick={() => handleDeleteButtonClick(item)}
                                    size="small">
                            <DeleteIcon/>
                        </IconButton>
                        <IconButton aria-label="Редактировать" onClick={() => handleEditButtonClick(item)}
                                    size="small">
                            <EditIcon/>
                        </IconButton>
                    </Fragment>
                );
            }
        },
        {
            columnCode: "FIRST_NAME",
            columnName: "Имя",
            sortable: true,
            align: "left",
            value: item => {
                return (
                    <Link to={`/motorist/${item.get("id")}#EV`}>
                        {item.get("firstName")}
                    </Link>
                );
            }
        },
        {
            columnCode: "LAST_NAME",
            columnName: "Фамилия",
            sortable: true,
            align: "left",
            value: item => {
                return item.get("lastName");
            }
        },
        {
            columnCode: "PHONE",
            columnName: "Телефон",
            sortable: false,
            align: "left",
            value: item => {
                return item.get("phone");
            }
        },
        {
            columnCode: "EMAIL",
            columnName: "Эл. адрес",
            sortable: false,
            align: "left",
            value: item => {
                return item.get("email");
            }
        },
        {
            columnCode: "ROLE",
            columnName: "Роль",
            sortable: false,
            align: "left",
            value: item => {
                return item.getIn(["userRole", "name"]);
            }
        }
    ];

    function handleEditButtonClick(item) {
        if (!item) {
            return
        }
        props.dispatch(push(`/motorist/${item.get("id")}#EV`));
    }

    function handleDeleteButtonClick(item) {
        if (!item) {
            return
        }
        setItemToDelete(item);
    }

    function handleCloseEditButtonClick() {
        props.dispatch(push(`/motorist`));
    }


    const getAddNewButton = () => {
        return (
            <Button
                variant="outlined"
                onClick={() => props.dispatch(push(`/motorist#EV`))}>
                <AddIcon color="primary"/>
                Добавить
            </Button>
        );
    };

    const getRequestProvider = (limit, start, searchKey, sortField, sortOrder, key) => {
        props.dispatch(motoristActions.listAction({start, limit, searchKey, sortField, sortOrder}));
    };

    function deleteItem(itemToDelete) {
        if (!itemToDelete) {
            return;
        }
        setItemToDelete(null);
        props.dispatch(motoristActions.deleteItemAction(itemToDelete.get("id")));
    }

    function renderSweetAlertBlock() {
        if (itemToDelete) {
            return (
                <SweetAlert
                    showCancel
                    title="Внимание"
                    type="warning"
                    confirmBtnBsStyle="danger"
                    cancelBtnBsStyle="default"
                    cancelBtnCssClass="MuiButtonBase-root MuiButton-root MuiButton-contained MuiButton-containedSecondary"
                    confirmBtnCssClass="MuiButtonBase-root MuiButton-root jss1086 MuiButton-contained MuiButton-containedPrimary"
                    onConfirm={() => deleteItem(itemToDelete)}
                    onCancel={() => setItemToDelete(null)}>
                    {`Вы действительно хотите удалить эту запись?`}
                </SweetAlert>
            );
        }
        return null;
    }

    const newButton = getAddNewButton();
    const sweetAlertBlock = renderSweetAlertBlock();

    return (
        <CardComponent title="Автомобилисты">
            {location.hash === "#EV" && (
                <MotoristEditModalView
                    {...props}
                    show={location.hash === "#EV"}
                    handleClose={() => handleCloseEditButtonClick()}
                />)}
            {sweetAlertBlock}
            <ListPanelComponent
                showDeleteModal={itemToDelete != null}
                loader={props.loader}
                list={motorist.list}
                count={motorist.count}
                addNewHandler={newButton}
                requestProvider={getRequestProvider}
                columnsConfig={Immutable.fromJS(columns)}
            />
        </CardComponent>
    )

}

export default connect(({motoristListView, motorist, loader}:any) => ({
    motoristListView, motorist, loader
}))(MotoristListView)
