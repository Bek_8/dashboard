import React, {useEffect} from "react";
import {connect} from "react-redux";
import * as PropTypes from "prop-types";
import * as motoristUserActions from "../../redux/actions/MotoristUserActions";
import MotoristEditModalForm from "./MotoristEditModalForm";

MotoristEditModalView.propTypes = {
    show: PropTypes.bool.isRequired,
    handleClose: PropTypes.func,
    genderTypes: PropTypes.array,
    statuses: PropTypes.array
};

function MotoristEditModalView(props:any) {
    const {show, handleClose} = props;
    const {
        motorist,
        dispatch
    } = props;
    const
        {
            item: {
                id,
                phone,
                email,
                firstName,
                lastName,
                middleName,
                birthDate,
                sex,
                userRole,
                status,
                latitude,
                longitude,
                postcode,
                addressLine1,
                addressLine2,
                notes
            },
            statuses,
            genderTypes

        } = motorist;

    const viewId = props.match.params.view;

    useEffect(() => {
        const fetchData = (id) => {
            return dispatch(motoristUserActions.itemAction(id));
        };

        fetchData(viewId);

        return () => {
            dispatch(motoristUserActions.clearItem());
        }
    }, [dispatch, viewId]);

    function submitForm(values) {
        props.dispatch(motoristUserActions.saveItem(values));
    }

    if (!show) {
        return <div className="hide"/>
    }

    return (
        <MotoristEditModalForm
            statuses={statuses}
            genderTypes={genderTypes}
            submitForm={submitForm}
            handleClose={handleClose}
            initialValues={{
                id,
                phone,
                email,
                firstName,
                lastName,
                middleName,
                birthDate,
                sex,
                userRole,
                status,
                latitude,
                longitude,
                postcode,
                addressLine1,
                addressLine2,
                notes
            }}
        />
    );
}

export default connect(({motoristEditModalView, motorist}:any) => ({
    motoristEditModalView,
    motorist,
}))(MotoristEditModalView);
