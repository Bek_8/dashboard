import React from "react";
import {reduxForm} from "redux-form";
import * as PropTypes from "prop-types";
import Grid from "@material-ui/core/Grid";
import DialogContent from "@material-ui/core/DialogContent";
import DialogActions from "@material-ui/core/DialogActions";
import Button from "@material-ui/core/Button";
import makeStyles from "@material-ui/core/styles/makeStyles";
import {email, phoneNumber, required} from "../../utils/reduxFormatUtils";
import Dialog from "@material-ui/core/Dialog";
import IconButton from "@material-ui/core/IconButton";
import CloseIcon from "@material-ui/icons/Close";
import DialogTitle from "@material-ui/core/DialogTitle";
import FormDatePickerField from "../../widgets/redux/FormDatePickerField";
import FormTextInputField from "../../widgets/redux/FormTextInputField";
import FormSelectField from "../../widgets/redux/FormSelectField";
import {isValidDate} from "../../widgets/redux/Validator";

const useStyles = makeStyles(theme => ({
    headerColor: {
        backgroundColor: theme.palette.primary.main,
        display: "flex",
        justifyContent: "space-between",
        alignItems: "center",
        padding: theme.spacing(0, 2)
    },
    form: {
        width: "100%",
        marginTop: theme.spacing(1)
    },
    submit: {},
    actionsBlock: {
        margin: theme.spacing(2, 2, 2, 2),
    },
    dialogPaper: {
        minHeight: "80vh",
        maxHeight: "80vh",
    },
}));

MotoristEditModalForm.propTypes = {
    handleClose: PropTypes.func,
    genderTypes: PropTypes.array,
    statuses: PropTypes.array
};

function MotoristEditModalForm(props:any) {
    const classes = useStyles(props);
    const {
        pristine,
        submitting,
        handleClose,
        handleSubmit,
        submitForm,
        statuses,
        genderTypes,
        initialValues: {
            firstName
        }
    } = props;

    return (
        <Dialog
            classes={{paper: classes.dialogPaper}}
            disableBackdropClick={true}
            disableEscapeKeyDown={true}
            fullWidth={true}
            maxWidth="lg"
            open={true}
            onClose={handleClose}>
            <DialogTitle disableTypography className={classes.headerColor}>
                {firstName && (
                    <h3>Редактирование {firstName}</h3>
                )}
                {!firstName && (
                    <h3>Новый авто пользователь</h3>
                )}

                <IconButton onClick={handleClose}>
                    <CloseIcon/>
                </IconButton>
            </DialogTitle>
            <form className={classes.form} noValidate onSubmit={handleSubmit(submitForm)}>
                <DialogContent style={{minHeight: "60vh"}}>
                    <Grid container spacing={3}>
                        <Grid item xs={4}>
                            <FormTextInputField
                                validate={required}
                                name="lastName"
                                InputProps={{
                                    fullWidth: true,
                                    required: true,
                                    label: "Фамилия",
                                    variant: "outlined"
                                }}
                            />
                        </Grid>
                        <Grid item xs={4}>
                            <FormTextInputField
                                validate={required}
                                name="firstName"
                                InputProps={{
                                    fullWidth: true,
                                    required: true,
                                    label: "Имя",
                                    variant: "outlined"
                                }}
                            />
                        </Grid>
                        <Grid item xs={4}>
                            <FormTextInputField
                                name="middleName"
                                InputProps={{
                                    fullWidth: true,
                                    label: "Отчество",
                                    variant: "outlined"
                                }}
                            />
                        </Grid>
                        <Grid item xs={6}>
                            <FormTextInputField
                                validate={email}
                                name="email"
                                InputProps={{
                                    fullWidth: true,
                                    required: true,
                                    label: "Эл. почта",
                                    variant: "outlined"
                                }}
                            />
                        </Grid>
                        <Grid item xs={6}>
                            <FormTextInputField
                                validate={phoneNumber}
                                name="phone"
                                InputProps={{
                                    fullWidth: true,
                                    required: true,
                                    label: "Телефон",
                                    variant: "outlined"
                                }}
                            />
                        </Grid>
                        <Grid item xs={4}>
                                <FormDatePickerField
                                    name="birthDate"
                                    label="Дата рождения"
                                    validate={isValidDate}
                                    fullWidth={true}
                                    InputProps={{
                                        fullWidth: true,
                                    } as any}
                                />
                        </Grid>
                        <Grid item xs={4}>
                            <FormSelectField
                                required
                                name="status"
                                label="Статус"
                                variant="outlined"
                                compareOptions={(option, value)=> (option.id === value.id)}
                                fullWidth={true}
                                options={statuses}
                                formatOption={v=>(v.name)}
                            />
                        </Grid>
                        <Grid item xs={4}>
                            <FormSelectField
                                required
                                name="sex"
                                label="Пол"
                                variant="outlined"
                                fullWidth={true}
                                compareOptions={(option, value)=> (option.id === value.id)}
                                options={genderTypes}
                                formatOption={v=>(v.name)}
                            />
                        </Grid>
                        <Grid item xs={4}>
                            <FormTextInputField
                                name="postcode"
                                InputProps={{
                                    fullWidth: true,
                                    label: "Почтовый индекс",
                                    variant: "outlined"
                                }}
                            />
                        </Grid>
                        <Grid item xs={4}>
                            <FormTextInputField
                                name="addressLine1"
                                InputProps={{
                                    fullWidth: true,
                                    label: "Адресс 1",
                                    variant: "outlined"
                                }}
                            />
                        </Grid>

                        <Grid item xs={4}>
                            <FormTextInputField
                                name="addressLine2"
                                InputProps={{
                                    fullWidth: true,
                                    label: "Адресс 2",
                                    variant: "outlined"
                                }}
                            />
                        </Grid>

                        <Grid item xs={12}>
                            <FormTextInputField
                                name="notes"
                                multiline={true}
                                rowsMax="5"
                                rows="3"
                                InputProps={{
                                    fullWidth: true,
                                    label: "Заметки",
                                    variant: "outlined"
                                }}
                            />
                        </Grid>
                    </Grid>

                </DialogContent>
                <DialogActions className={classes.actionsBlock}>
                    <Button
                        disabled={submitting}
                        className={classes.submit}
                        onClick={handleClose}
                        color="secondary"
                        variant="outlined">
                        Отменить
                    </Button>
                    <Button
                        disabled={pristine || submitting}
                        className={classes.submit}
                        type="submit"
                        color="primary"
                        variant="contained">
                        Сохранить
                    </Button>
                </DialogActions>
            </form>
        </Dialog>
    );
}

export default reduxForm({
    form: "motoristEditForm",
    enableReinitialize: true
})(MotoristEditModalForm);