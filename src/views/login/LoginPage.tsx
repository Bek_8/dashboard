import React from 'react';

import makeStyles from '@material-ui/core/styles/makeStyles';
import Container from '@material-ui/core/Container';
import CssBaseline from '@material-ui/core/CssBaseline';
import Avatar from '@material-ui/core/Avatar';
import LockOutlinedIcon from '@material-ui/icons/LockOutlined';
import Typography from '@material-ui/core/Typography';
import IconButton from '@material-ui/core/IconButton';
import SvgIcon from '@material-ui/core/SvgIcon';

import LoginForm from './LoginForm';
import * as authActions from '../../redux/actions/AuthActions';
import {connect} from 'react-redux';
import {Paper} from "@material-ui/core";
import createMuiTheme, {Theme} from "@material-ui/core/styles/createMuiTheme";

const useStyles = makeStyles((theme:Theme) => ({
    '@global': {
        body: {
            //    backgroundColor: 'radial-gradient(circle at 19% 65%, #870000, #190a05)',
            background:theme.palette.grey[100] ,

        }
    },
    root: {
        display: 'flex',
        height: '100vh',
       // flexDirection: "column",
        alignItems: "center",
        justifyContent: 'center'
    },
    paper: {
        padding: theme.spacing(2, 7),
        display: 'flex',
        flexDirection: 'column',
        alignItems: 'center',
        backgroundColor: theme.palette.common.white,

    },
    avatar: {
        margin: theme.spacing(1),
        backgroundColor: theme.palette.secondary.main
    },

}));

function LoginPage(props: any) {
    const classes = useStyles(props);

    function submitForm({login, password}) {
        props.dispatch(authActions.auth({login, password}));
    }

    return (
        <Container component="main" maxWidth="xs" className={classes.root}>
            <CssBaseline/>
            <Paper className={classes.paper}>
                <Avatar className={classes.avatar}>
                    <LockOutlinedIcon/>
                </Avatar>
                <Typography component="h1" variant="h5">
                    AutoBio.uz
                </Typography>


                <LoginForm
                    submitForm={submitForm}/>
            </Paper>
        </Container>
    );
}

export default connect(({loginPage}: any) => ({
    loginPage
}))(LoginPage);
