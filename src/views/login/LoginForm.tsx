import React from "react";
import {reduxForm} from "redux-form";
import Button from "@material-ui/core/Button";
import makeStyles from "@material-ui/core/styles/makeStyles";
import IconButton from '@material-ui/core/IconButton';
import SvgIcon from '@material-ui/core/SvgIcon';

import FormTextInputField from "../../widgets/redux/FormTextInputField";
import {isEmpty} from "../../widgets/redux/Validator";

const useStyles = makeStyles(theme => ({
	form: {
		width: "100%", // Fix IE 11 issue.
		display: "flex",
		marginTop: theme.spacing(1),
		flexDirection: "column",
		jsutifyContent: "center",

	},
	submit: {
		margin: "20px 0px",
		padding: "12px",
		fontSize: "1em",
	}
}));

function LoginForm(props:any) {
	const classes = useStyles(props);
	const {handleSubmit, submitForm, submitting} = props;

	return (
		<form className={classes.form} noValidate onSubmit={handleSubmit(submitForm)}>
            <FormTextInputField
                validate={isEmpty}
                name="login"
                InputProps={{
                    width: true,
                    required: true,
                    label: "Логин",
                    variant: "outlined"
                }}

            />
            <FormTextInputField
                validate={isEmpty}
                type="password"
                name="password"
                InputProps={{
                    fullWidth: true,
                    required: true,
                    label: "Пароль",
                    variant: "outlined",
                    type: "password"
                }}
            />

			<Button

                disabled={submitting}
				type="submit"
				variant="contained"
				color="primary"
				className={classes.submit}
			>
				Войти
			</Button>
		</form>
	);
}

export default reduxForm({
	form: "login",
	enableReinitialize: true
})(LoginForm);