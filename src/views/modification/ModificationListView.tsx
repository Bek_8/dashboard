import React, {Fragment, useState} from "react";
import * as PropTypes from "prop-types";
import {connect} from "react-redux";
import Immutable from "immutable";

import SweetAlert from "react-bootstrap-sweetalert";
import IconButton from "@material-ui/core/IconButton";
import DeleteIcon from "@material-ui/icons/Delete";
import AddIcon from "@material-ui/icons/Add";
import EditIcon from "@material-ui/icons/Edit";
import Button from "@material-ui/core/Button";

import ListPanelComponent from "../../widgets/listPanel/ListPanelComponent";
import CardComponent from "../../widgets/CardComponent";
import * as modificationActions from "../../redux/actions/ModificationActions";
import ModificationEditModalView from "./ModificationEditModalView";
import {push} from "react-router-redux";

ModificationListView.propTypes = {
    modification: PropTypes.object,
    modelId: PropTypes.string.isRequired
};

function ModificationListView(props:any) {
    const {modification, modelId, location} = props;

    const [itemToDelete, setItemToDelete] = useState(null);
    const columns = [
        {
            columnCode: "action",
            columnName: "Действия",
            width: "100px",
            align: "left",
            value: item => {
                return (
                    <Fragment>
                        <IconButton aria-label="Удалить" onClick={() => handleDeleteButtonClick(item)}
                                    size="small">
                            <DeleteIcon/>
                        </IconButton>
                        <IconButton aria-label="Редактировать" onClick={() => handleEditButtonClick(item)}
                                    size="small">
                            <EditIcon/>
                        </IconButton>
                    </Fragment>
                );
            }
        },
        {
            columnCode: "NAME",
            columnName: "Название",
            sortable: true,
            align: "left",
            value: item => (item.get("name"))
        },
        {
            columnCode: "DESCRIPTION",
            columnName: "Описание",
            sortable: false,
            align: "left",
            value: item => (item.get("description"))
        }
    ];


    function handleCloseEditButtonClick() {
        props.dispatch(push(`/model/item/${props.match.params.make}/${props.match.params.view}/modification`));
    }

    function handleEditButtonClick(item) {
        if (!item) {
            return
        }
        props.dispatch(push(`/model/item/${props.match.params.make}/${props.match.params.view}/modification/${item.get("id")}#EV`));
    }

    function handleDeleteButtonClick(item) {
        if (!item) {
            return
        }
        setItemToDelete(item);
    }

    function getAddNewButton() {
        return (
            <Button
                variant="outlined"
                onClick={() => props.dispatch(push(`/model/item/${props.match.params.make}/${props.match.params.view}/modification#EV`))}>
                <AddIcon color="primary"/>
                Добавить
            </Button>
        );
    }

    const getRequestProvider = (limit, start, searchKey, sortField, sortOrder, key) => {
        props.dispatch(modificationActions.listAction({modelId, start, limit, searchKey, sortField, sortOrder}));
    };

    function deleteItem(itemToDelete) {
        if (!itemToDelete) {
            return;
        }
        setItemToDelete(null);
        props.dispatch(modificationActions.deleteItemAction(itemToDelete.get("id"), props.match.params.view));
    }

    function renderSweetAlertBlock() {
        if (itemToDelete) {
            return (
                <SweetAlert
                    showCancel
                    title="Внимание"
                    type="warning"
                    confirmBtnBsStyle="danger"
                    cancelBtnBsStyle="default"
                    cancelBtnCssClass="MuiButtonBase-root MuiButton-root MuiButton-contained MuiButton-containedSecondary"
                    confirmBtnCssClass="MuiButtonBase-root MuiButton-root jss1086 MuiButton-contained MuiButton-containedPrimary"
                    onConfirm={() => deleteItem(itemToDelete)}
                    onCancel={() => setItemToDelete(null)}>
                    {`Вы действительно хотите удалить эту запись?`}
                </SweetAlert>
            );
        }
        return null;
    }

    const sweetAlertBlock = renderSweetAlertBlock();
    const newButton = getAddNewButton();

    return (
        <CardComponent title="Модификации">
            {location.hash === "#EV" && (
                <ModificationEditModalView
                    {...props}
                    modificationId={props.match.params.modificationId}
                    handleClose={() => handleCloseEditButtonClick()}
                />)}
            {sweetAlertBlock}
            <ListPanelComponent
                showDeleteModal={itemToDelete != null}
                loader={props.loader}
                list={modification.list}
                count={modification.count}
                addNewHandler={newButton}
                requestProvider={getRequestProvider}
                columnsConfig={Immutable.fromJS(columns)}
            />
        </CardComponent>
    );
}

export default connect(({modificationListView, modification, loader}:any) => ({
    modificationListView,
    modification,
    loader
}))(ModificationListView);
