import React, {useEffect} from "react";
import {connect} from "react-redux";
import * as PropTypes from "prop-types";
import ModificationEditForm from "./ModificationEditForm";
import * as modificationActions from "../../redux/actions/ModificationActions";
import {push} from "react-router-redux";

ModificationEditModalView.propTypes = {
    handleClose: PropTypes.func,
    modificationId: PropTypes.any
};

function ModificationEditModalView(props:any) {
    const {
        handleClose,
        dispatch,
        modificationId,
        modification: {
            item: {
                id,
                name,
                description
            }
        }
    } = props;

    useEffect(() => {
        const fetchData = (id) => {
            return dispatch(modificationActions.itemAction(id));
        };

        fetchData(modificationId);

        return () => {
            dispatch(modificationActions.clearItem());
        }
    }, [dispatch, modificationId]);

    function submitForm(values) {
        props.dispatch(modificationActions.saveItem(values));
        props.dispatch(push(`/model/item/${props.match.params.make}/${props.match.params.view}/modification`))
    }

    return (
        <ModificationEditForm
            submitForm={submitForm}
            handleClose={handleClose}
            initialValues={{
                id,
                name,
                description,
                modelId: props.match.params.view
            }}
        />
    )
}

export default connect(({modificationEditModalView, modification}:any) => ({
    modificationEditModalView,
    modification,
}))(ModificationEditModalView);
