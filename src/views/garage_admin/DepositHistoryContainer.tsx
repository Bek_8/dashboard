import React, {useEffect} from "react";
import CardComponent from "../../widgets/CardComponent";
import {useDispatch, useSelector} from "react-redux";
import fp from "lodash/fp"
import * as depositActions from "../../redux/actions/DepositActions";
import {getDepositItemByProviderIdAction} from "../../redux/actions/DepositActions";
import {Grid} from "@material-ui/core";

import IconButton from "@material-ui/core/IconButton";
import EditIcon from "@material-ui/icons/Edit";
import {Link, useHistory, useLocation, useParams} from "react-router-dom";
// import {setDeleteConfirmationItem} from "../../actions/MessageActions";
import * as providerActions from "../../redux/actions/ProviderActions";
import {updateQuery} from "../../utils/UrlUtils";
import DataTable, {DataCol} from "../../components/table-core/DataTable";
import useQueryFilter from "../../hooks/useQueryFilter";

const getProviderId: (props: any) => number = fp.get("match.params.providerId")

export default function ProviderDepositHistoryContainer(props: any) {
    const dispatch = useDispatch()
    const history = useHistory()
    const location = useLocation()
    const filter = useQueryFilter(useParams());
    const providerId = props.match.params.providerId

    const list = useSelector((state: any) => state.deposit.list)
    const depositInfo = useSelector((state: any) => state.deposit.depositInfo)
    const loader = useSelector((state: any) => state.deposit.loader)
    const count = useSelector((state: any) => state.deposit.count)

    useEffect(() => {
        dispatch(getDepositItemByProviderIdAction(getProviderId(props)))
    }, [filter])

    useEffect(() => {
        dispatch(depositActions.getDepositList(filter))

    }, [filter])

    return (
        <CardComponent title="Депозиты провайдера">
            {depositInfo && <Grid
                container
                alignItems="flex-end"
            >
                <Grid item xs={12} md={6}>
                    {depositInfo.providerName}
                </Grid>
                <Grid item xs={12} md={6} style={{textAlign: "right"}}>
                    Current Deposit: {depositInfo.amount || 0}
                </Grid>
            </Grid>}

            <DataTable
                listing={{list, count}}
                title="История депозита"
                filter={filter}
                addLink={`/provider/${providerId}/deposit/add`}
                getList={(filter) => dispatch(providerActions.getProviderList(filter))}
                onFilterChange={query => history.push(updateQuery(location, query))}
            >
                <DataCol label={"ID"}
                         sortable={true}
                         fieldKey={"id"}
                         cellRenderer={row => row.get("id")}/>

                <DataCol label={"Сумма депозита"}
                         sortable={true}
                         fieldKey={"amount"}
                         cellRenderer={row => row.get("amount")}/>

                <DataCol label={"Дата депозита"}
                         fieldKey={"createDate"}
                         cellRenderer={row => row.get("createdDate")}/>

                <DataCol label={"Инициатор"}
                         fieldKey={"createdBy"}
                         cellRenderer={row => row.get("createdBy")}/>


                <DataCol label={"Действие"}
                         cellRenderer={row => (
                             row.get("editable") && <IconButton aria-label="Редактировать"
                                                                component={Link}
                                                                to={`/provider/${providerId}/deposit/${row.get("id")}`}
                                                                size="small">
                                 <EditIcon/>
                             </IconButton>
                         )}/>


            </DataTable>
        </CardComponent>
    )
}
