import React, {useEffect} from "react";
import * as adminUserActions from "../../redux/actions/AdminUserActions";
import {useDispatch, useSelector} from "react-redux";
import {createSelector} from "reselect";
import CardComponent from "../../widgets/CardComponent";
import Grid from "@material-ui/core/Grid";

import Table from '@material-ui/core/Table';
import TableBody from '@material-ui/core/TableBody';
import TableCell from '@material-ui/core/TableCell';
import TableRow from '@material-ui/core/TableRow';

const getUser = createSelector(
    (state: any) => state.auth,
    (auth: any) => auth.user
);

function UserProfileContainer(props: any) {
    const dispatch = useDispatch();
    const item = useSelector(getUser);
    const id = props.match.params.id;
    useEffect(() => {
        function fetchData() {
            dispatch(adminUserActions.itemAction(id));
        }

        if (id) fetchData();
        return () => {
            dispatch(adminUserActions.clearItem());
        }
    }, [id]);


    return (
        <CardComponent title="Информация о пользователе">
            <Grid container spacing={3}>
                <Grid item xs={12} sm={6}>
                    <Table aria-label="simple table">
                        <TableBody>
                            <TableRow>
                                <TableCell component="th" scope="row">
                                    ID:
                                </TableCell>
                                <TableCell align="right">{item.id}</TableCell>
                            </TableRow>
                            <TableRow>
                                <TableCell component="th" scope="row">
                                    Логин:
                                </TableCell>
                                <TableCell align="right">{item.login}</TableCell>
                            </TableRow>
                            <TableRow>
                                <TableCell component="th" scope="row">
                                    ФИО:
                                </TableCell>
                                <TableCell align="right">{item.firstName} {item.lastName}</TableCell>
                            </TableRow>
                            <TableRow>
                                <TableCell component="th" scope="row">
                                    Тип:
                                </TableCell>
                                <TableCell align="right">{item.userRole}</TableCell>
                            </TableRow>
                            <TableRow>
                                <TableCell component="th" scope="row">
                                    Статус:
                                </TableCell>
                                <TableCell align="right">{item.status}</TableCell>
                            </TableRow>
                            <TableRow>
                                <TableCell component="th" scope="row">
                                    Дата рождения:
                                </TableCell>
                                <TableCell align="right">{item.birthDate}</TableCell>
                            </TableRow>
                        </TableBody>
                    </Table>
                </Grid>
            </Grid>
        </CardComponent>
    );
}

export default UserProfileContainer;
