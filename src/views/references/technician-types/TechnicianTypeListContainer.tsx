import React, {Fragment, useEffect} from "react";
import {useDispatch, useSelector} from "react-redux";
import * as action from "../../../redux/actions/ReferenceActions";

import IconButton from "@material-ui/core/IconButton";
import DeleteIcon from "@material-ui/icons/Delete";
import EditIcon from "@material-ui/icons/Edit";
import {Link, useHistory, useLocation, useParams} from "react-router-dom";
import {setDeleteConfirmationItem} from "../../../redux/actions/MessageActions";
import DataTable, {DataCol} from "../../../components/table-core/DataTable";
import * as H from "history";
import {updateQuery} from "../../../utils/UrlUtils";
import useQueryFilter from "../../../hooks/useQueryFilter";

import {createSelector} from 'reselect'
import CardComponent from "../../../widgets/CardComponent";

const getListingReducers = createSelector(
    (state: any) => state.reference,
    ({list, count, loader}) => ({list, count, loader})
);
export default function TechnicianTypeListContainer(props: any) {
    const params = useParams()
    const dispatch = useDispatch()
    const location: H.Location = useLocation();
    const filter = useQueryFilter({...params, parentCode: "_EXPENSE_TYPES"})
    const history: H.History = useHistory()
    const {list, count, loader} = useSelector(getListingReducers)

    useEffect(() => {
        dispatch(action.listAction(filter))
    }, [filter])

    return (
        <CardComponent title="Затраты">
            <DataTable
                listing={{list, count}}
                filter={filter}
                loader={loader}
                addLink={'/reference/expense-type/add'}
                onDeleteItem={(itemId) => dispatch(action.deleteItemAction(itemId))}
                getList={(filter) => dispatch(action.listAction(filter))}
                onFilterChange={query => history.push(updateQuery(location, query))}
            >
                <DataCol label={"ID"}
                         sortable={true}
                         fieldKey={"id"}
                         cellRenderer={row => row.get("id")}/>

                <DataCol label={"Название"}
                         sortable={true}
                         fieldKey={"nameRu"}
                         cellRenderer={row => row.get("nameRu")}
                />


                <DataCol label={"Действия"}
                         cellRenderer={row => (
                             <Fragment>
                                 <IconButton aria-label="Редактировать"
                                             component={Link}
                                             to={`/reference/expense-type/${row.get("id")}`}
                                             size="small">
                                     <EditIcon/>
                                 </IconButton>
                                 <IconButton aria-label="Удалить"
                                             onClick={() => dispatch(setDeleteConfirmationItem(row))}
                                             size="small">
                                     <DeleteIcon/>
                                 </IconButton>
                             </Fragment>
                         )}/>


            </DataTable>
        </CardComponent>
    )
}
