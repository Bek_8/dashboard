import React, {useEffect} from "react";
import * as action from "../../../redux/actions/AutoServiceActions";
import {useDispatch} from "react-redux";
import {goBack} from "react-router-redux";
import AutoServiceSaveForm from "../../../components/service-core/AutoServiceSaveForm";


function TechnicianTypeAddContainer(props: any) {
    const dispatch = useDispatch();
    const id = props.match.params.id;

    useEffect(() => {
        function fetchData() {
            dispatch(action.itemAction(id));
        }

        if (id) fetchData();

        return () => {
            dispatch(action.clearItem());
        }
    }, [id]);

    function submitForm(values) {
        dispatch(action.saveItem(values));
    }

    return (
        <div>
            <AutoServiceSaveForm
                handleBack={() => dispatch(goBack())}
                submitForm={submitForm}
                initialValues={{
                    parentCode: "_EXPENSE_TYPES"
                }}
            />

        </div>
    );
}

export default TechnicianTypeAddContainer;
