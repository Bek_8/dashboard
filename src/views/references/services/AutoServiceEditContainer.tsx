import React, {useEffect} from "react";
import * as action from "../../../redux/actions/AutoServiceActions";
import {useDispatch, useSelector} from "react-redux";
import {goBack} from "react-router-redux";
import {createSelector} from "reselect";
import AutoServiceSaveForm from "../../../components/service-core/AutoServiceSaveForm";
import ReferenceEditItemTabs from "../../../components/reference-core/ReferenceEditItemTabs";
import fp from "lodash/fp";
import AutoServiceChildrenTab from "../../../components/reference-core/AutoServiceChildrenTab";
import {AUTO_SERVICE_TABS} from "../../../constants/Constants";


const getItemReducers = createSelector(
    (state: any) => state.autoService,
    (service: any) => service.item
);

export const getTab = fp.flow(
    fp.get("hash"),
    hash => hash || "#category",
    str => str.substring(1)
);
export const hasItem = fp.has("id")

function AutoServiceEditContainer(props: any) {
    const dispatch = useDispatch();
    const serviceItem = useSelector(getItemReducers);
    const id = props.match.params.id;

    useEffect(() => {
        function fetchData() {
            dispatch(action.itemAction(id));
        }

        if (id) fetchData();

        return () => {
            dispatch(action.clearItem());
        }
    }, [id]);

    function submitForm(values) {
        dispatch(action.saveItem(values));
    }

    const selectedTab = getTab(props.location);

    return (
        <div>
            <ReferenceEditItemTabs
                selectedTab={selectedTab}
                tabs={AUTO_SERVICE_TABS}
                location={props.location}
            />
            {selectedTab === 'category' && <AutoServiceSaveForm
                handleBack={() => dispatch(goBack())}
                submitForm={submitForm}
                initialValues={serviceItem}
            />}

            {selectedTab === 'services' && hasItem(serviceItem) &&
            <AutoServiceChildrenTab parentItem={serviceItem}/>
            }

        </div>
    );
}

export default AutoServiceEditContainer;
