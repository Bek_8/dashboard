import React, {useEffect} from "react";
import * as action from "../../../redux/actions/MakeModelsActions";
import {useDispatch} from "react-redux";
import {goBack} from "react-router-redux";
import ReferenceChildSaveForm from "../../../components/reference-core/ReferenceChildSaveForm";
import useQueryFilter from "../../../hooks/useQueryFilter";


function MakeChildAddContainer(props: any) {
    const dispatch = useDispatch();
    const id = props.match.params.id;
    const query = useQueryFilter();

    useEffect(() => {
        function fetchData() {
            dispatch(action.itemAction(id));
        }

        if (id) fetchData();

        return () => {
            dispatch(action.clearItem());
        }
    }, [id]);

    function submitForm(values) {
        dispatch(action.saveItem(values));
    }

    return (
        <div>
            <ReferenceChildSaveForm
                handleBack={() => dispatch(goBack())}
                submitForm={submitForm}
                initialValues={{
                    parentCode: query.parentCode
                }}
            />

        </div>
    );
}

export default MakeChildAddContainer;
