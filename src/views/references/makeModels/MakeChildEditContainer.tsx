import React, {useEffect} from "react";
import * as action from "../../../redux/actions/MakeModelsActions";
import {useDispatch, useSelector} from "react-redux";
import {goBack} from "react-router-redux";
import {createSelector} from "reselect";
import ReferenceChildSaveForm from "../../../components/reference-core/ReferenceChildSaveForm";
import {MAKE_MODELS_TABS, MAKE_MODIFICATIONS_TABS} from "../../../constants/Constants";
import ReferenceEditItemTabs from "../../../components/reference-core/ReferenceEditItemTabs";
import {getTab, hasItem} from "./MakeEditContainer";
import MakeChildrenTab from "../../../components/reference-core/MakeChildrenTab";


const getItemReducers = createSelector(
    (state: any) => state.makeModels,
    (service: any) => service.item
);

function MakeChildEditContainer(props: any) {
    const dispatch = useDispatch();
    const childItem = useSelector(getItemReducers);
    const id = props.match.params.id;

    useEffect(() => {
        function fetchData() {
            dispatch(action.itemAction(id));
        }

        if (id) fetchData();

        return () => {
            dispatch(action.clearItem());
        }
    }, [id]);

    function submitForm(values) {
        dispatch(action.saveItem(values));
    }

    const selectedTab = getTab(props.location);
    return (
        <div>
            <ReferenceEditItemTabs
                selectedTab={selectedTab}
                tabs={MAKE_MODIFICATIONS_TABS}
                location={props.location}
            />
            {selectedTab === 'category' &&<ReferenceChildSaveForm
                handleBack={() => dispatch(goBack())}
                submitForm={submitForm}
                initialValues={childItem}
            />}
            {selectedTab === 'modifications' && hasItem(childItem) &&
            <MakeChildrenTab parentItem={childItem}/>
            }
        </div>
    );
}

export default MakeChildEditContainer;
