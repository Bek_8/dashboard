import React, {useEffect} from "react";
import * as action from "../../../redux/actions/MakeModelsActions";
import {useDispatch} from "react-redux";
import {goBack} from "react-router-redux";
import AutoServiceSaveForm from "../../../components/service-core/AutoServiceSaveForm";


function MakeAddContainer(props: any) {
    const dispatch = useDispatch();
    const id = props.match.params.id;

    useEffect(() => {
        function fetchData() {
            dispatch(action.itemAction(id));
        }

        if (id) fetchData();

        return () => {
            dispatch(action.clearItem());
        }
    }, [id]);

    function submitForm(values) {
        dispatch(action.saveItem(values));
    }

    return (
        <div>
            <AutoServiceSaveForm
                handleBack={() => dispatch(goBack())}
                hasImage={true}
                submitForm={submitForm}
                initialValues={{
                    parentCode: "_VEHICLE_MAKE"
                }}
            />

        </div>
    );
}

export default MakeAddContainer;
