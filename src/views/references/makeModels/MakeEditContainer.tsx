import React, {useEffect} from "react";
import * as action from "../../../redux/actions/MakeModelsActions";
import {useDispatch, useSelector} from "react-redux";
import {goBack} from "react-router-redux";
import {createSelector} from "reselect";
import AutoServiceSaveForm from "../../../components/service-core/AutoServiceSaveForm";
import ReferenceEditItemTabs from "../../../components/reference-core/ReferenceEditItemTabs";
import fp from "lodash/fp";
import {MAKE_MODELS_TABS} from "../../../constants/Constants";
import MakeChildrenTab from "../../../components/reference-core/MakeChildrenTab";


const getItemReducers = createSelector(
    (state: any) => state.makeModels,
    (service: any) => service.item
);

export const getTab = fp.flow(
    fp.get("hash"),
    hash => hash || "#category",
    str => str.substring(1)
);
export const hasItem = fp.has("id")

function MakeEditContainer(props: any) {
    const dispatch = useDispatch();
    const item = useSelector(getItemReducers);
    const id = props.match.params.id;

    useEffect(() => {
        function fetchData() {
            dispatch(action.itemAction(id));
        }

        if (id) fetchData();

        return () => {
            dispatch(action.clearItem());
        }
    }, [id]);

    function submitForm(values) {
        dispatch(action.saveItem(values));
    }

    const selectedTab = getTab(props.location);

    return (
        <div>
            <ReferenceEditItemTabs
                selectedTab={selectedTab}
                tabs={MAKE_MODELS_TABS}
                location={props.location}
            />
            {selectedTab === 'category' && <AutoServiceSaveForm
                handleBack={() => dispatch(goBack())}
                hasImage={true}
                submitForm={submitForm}
                item={item}
                initialValues={item}
            />}

            {selectedTab === 'models' && hasItem(item) &&
            <MakeChildrenTab parentItem={item}/>
            }

        </div>
    );
}

export default MakeEditContainer;
