import React, {useEffect} from "react";
import * as action from "../../../redux/actions/ReferenceActions";
import {useDispatch, useSelector} from "react-redux";
import {goBack} from "react-router-redux";
import {createSelector} from "reselect";
import ReferenceItemSaveForm from "../../../components/reference-core/ReferenceItemSaveForm";


const getItemReducers = createSelector(
    (state: any) => state.reference,
    (stateSection: any) => stateSection.item
);

function ExpenseTypeEditContainer(props: any) {
    const dispatch = useDispatch();
    const item = useSelector(getItemReducers);
    const id = props.match.params.id;

    useEffect(() => {
        function fetchData() {
            dispatch(action.itemAction(id));
        }

        if (id) fetchData();

        return () => {
            dispatch(action.clearItem());
        }
    }, [id]);

    function submitForm(values) {
        dispatch(action.saveItem(values));
    }

    return (
        <div>
            <ReferenceItemSaveForm
                handleBack={() => dispatch(goBack())}
                submitForm={submitForm}
                initialValues={item}
            />

        </div>
    );
}

export default ExpenseTypeEditContainer;
