import React, {useEffect} from "react";
import * as action from "../../../redux/actions/OilStationActions";
import {useDispatch, useSelector} from "react-redux";
import {goBack} from "react-router-redux";
import {createSelector} from "reselect";
import AutoServiceSaveForm from "../../../components/service-core/AutoServiceSaveForm";
import ReferenceEditItemTabs from "../../../components/reference-core/ReferenceEditItemTabs";
import fp from "lodash/fp";
import OilStationChildrenTab from "../../../components/reference-core/OilStationChildrenTab";
import {OIL_STATION_TABS} from "../../../constants/Constants";
import OilStationSaveForm from "../../../components/reference-core/OilStationSaveForm";


export const getTab = fp.flow(
    fp.get("hash"),
    hash => hash || "#category",
    str => str.substring(1)
);
export const hasItem = fp.has("id")
const getItemReducers = createSelector(
    (state: any) => state.oilStation,
    (service: any) => service.item
);

function OilStationEditContainer(props: any) {
    const dispatch = useDispatch();
    const oilStation = useSelector(getItemReducers);
    const id = props.match.params.id;

    useEffect(() => {
        function fetchData() {
            dispatch(action.itemAction(id));
        }

        if (id) fetchData();

        return () => {
            dispatch(action.clearItem());
        }
    }, [id]);

    function submitForm(values) {
        dispatch(action.saveItem(values));
    }

    const selectedTab = getTab(props.location);

    return (
        <div>
            <ReferenceEditItemTabs
                tabs={OIL_STATION_TABS}
                selectedTab={selectedTab}
                location={props.location}
            />
            {selectedTab === 'category' && <OilStationSaveForm
                handleBack={() => dispatch(goBack())}
                submitForm={submitForm}
                initialValues={oilStation}
            />}

            {selectedTab === 'makes' && hasItem(oilStation) &&
            <OilStationChildrenTab
                parentItem={oilStation}/>}
        </div>
    );
}

export default OilStationEditContainer;
