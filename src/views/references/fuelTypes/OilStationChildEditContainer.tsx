import React, {useEffect} from "react";
import * as action from "../../../redux/actions/OilStationActions";
import {useDispatch, useSelector} from "react-redux";
import {goBack} from "react-router-redux";
import {createSelector} from "reselect";
import ReferenceChildSaveForm from "../../../components/reference-core/ReferenceChildSaveForm";


const getItemReducers = createSelector(
    (state: any) => state.oilStation,
    (service: any) => service.item
);

function OilStationChildEditContainer(props: any) {
    const dispatch = useDispatch();
    const childItem = useSelector(getItemReducers);
    const id = props.match.params.id;

    useEffect(() => {
        function fetchData() {
            dispatch(action.itemAction(id));
        }

        if (id) fetchData();

        return () => {
            dispatch(action.clearItem());
        }
    }, [id]);

    function submitForm(values) {
        dispatch(action.saveItem(values));
    }

    return (
        <div>

            <ReferenceChildSaveForm
                handleBack={() => dispatch(goBack())}
                submitForm={submitForm}
                initialValues={childItem}
            />

        </div>
    );
}

export default OilStationChildEditContainer;
