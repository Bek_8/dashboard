import {goBack, push} from "react-router-redux";
import * as adminUser from "../actionTypes/AdminUserActionTypes";
import {list, item, createItem, updateItem, deleteItem} from "../../api/AdminUserApi";
import {toggleBackdropLoader} from "./MessageActions";

export const listAction = (params = {
    start: 0,
    limit: 10,
    searchKey: null,
    sortField: null,
    sortOrder: null
}) => (dispatch) => {
    dispatch({
        api: list,
        types: [
            adminUser.LOAD_ADMIN_USER_LIST_START,
            adminUser.LOAD_ADMIN_USER_LIST_SUCCESS,
            adminUser.LOAD_ADMIN_USER_LIST_ERROR
        ],
        query: params
    })
};

export const itemAction = (id) => (dispatch) => {
    if (!id) {
        return;
    }
    dispatch({
        api: item,
        types: [
            adminUser.LOAD_ADMIN_USER_ITEM_START,
            adminUser.LOAD_ADMIN_USER_ITEM_SUCCESS,
            adminUser.LOAD_ADMIN_USER_ITEM_ERROR
        ],
        query: {
            id
        }
    });
};

export const clearItem = () => (dispatch) => {
    dispatch({
        type: adminUser.LOAD_ADMIN_USER_ITEM_ERROR,
        payload: null
    });
};

export const deleteItemAction = (id) => (dispatch) => {
    dispatch({
        api: deleteItem,
        types: [
            adminUser.DELETE_ADMIN_USER_ITEM_START,
            adminUser.DELETE_ADMIN_USER_ITEM_SUCCESS,
            adminUser.DELETE_ADMIN_USER_ITEM_ERROR
        ],
        query: {
            id
        }
    })
        .then(() => dispatch(listAction()))
};

export const saveItem = (item) => (dispatch) => {
    if (item === null) {
        return
    }
    let api = updateItem;
    if (!item.id) {
        api = createItem;
    }
    dispatch(toggleBackdropLoader(true))

    dispatch({
        api,
        types: [
            adminUser.SAVE_ADMIN_USER_ITEM_START,
            adminUser.SAVE_ADMIN_USER_ITEM_SUCCESS,
            adminUser.SAVE_ADMIN_USER_ITEM_ERROR
        ],
        query: item
    })
        .then(() => {
            dispatch(goBack());
            dispatch(toggleBackdropLoader(false))
        })
        .catch(() => {
            dispatch(toggleBackdropLoader(false))
        })
};
