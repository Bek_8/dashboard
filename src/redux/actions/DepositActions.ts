import { goBack} from "react-router-redux";
import * as DepositActionTypes from "../actionTypes/DepositActionTypes";
import {list, item, createItem, updateItem, deleteItem, getItemByProviderId} from "../../api/DepositApi";
import {toggleBackdropLoader} from "./MessageActions";
// import {getDepositItemPayload} from "../utils/ProviderUtils";

export const getDepositList = (params = {
    start: 0,
    limit: 10,
    searchKey: null,
    sortField: null,
    sortOrder: null
}) => (dispatch) => {
   return  dispatch({
        api: list,
        types: [
            DepositActionTypes.LOAD_DEPOSIT_LIST_START,
            DepositActionTypes.LOAD_DEPOSIT_LIST_SUCCESS,
            DepositActionTypes.LOAD_DEPOSIT_LIST_ERROR
        ],
        query: params
    })
};

export const getDepositItemByProviderIdAction = (providerId) =>
    (dispatch) => {
        if (!providerId) {
            return;
        }
        dispatch({
            api: getItemByProviderId,
            types: [
                DepositActionTypes.LOAD_DEPOSIT_ITEM_BY_PROVIDER_ID_START,
                DepositActionTypes.LOAD_DEPOSIT_ITEM_BY_PROVIDER_ID_SUCCESS,
                DepositActionTypes.LOAD_DEPOSIT_ITEM_BY_PROVIDER_ID_ERROR
            ],
            query:providerId
        });
    };

export const getDepositItemAction = (id) =>
    (dispatch) => {
        if (!id) {
            return;
        }
        dispatch({
            api: item,
            types: [
                DepositActionTypes.LOAD_DEPOSIT_ITEM_START,
                DepositActionTypes.LOAD_DEPOSIT_ITEM_SUCCESS,
                DepositActionTypes.LOAD_DEPOSIT_ITEM_ERROR
            ],
            query:id
        });
    };


export const clearItem = () => (dispatch) => {
    dispatch({
        type: DepositActionTypes.LOAD_DEPOSIT_ITEM_ERROR,
        payload: null
    });
};

export const deleteItemAction = (id) => (dispatch) => {
    dispatch({
        api: deleteItem,
        types: [
            DepositActionTypes.DELETE_DEPOSIT_ITEM_START,
            DepositActionTypes.DELETE_DEPOSIT_ITEM_SUCCESS,
            DepositActionTypes.DELETE_DEPOSIT_ITEM_ERROR
        ],
        query: {
            id
        }
    })
};

export const saveItem = (item) => (dispatch) => {
    if (item === null) {
        return
    }
    let api = updateItem;
    if (!item.id) {
        api = createItem;
    }
    dispatch(toggleBackdropLoader(true))

    dispatch({
        api,
        types: [
            DepositActionTypes.SAVE_DEPOSIT_ITEM_START,
            DepositActionTypes.SAVE_DEPOSIT_ITEM_SUCCESS,
            DepositActionTypes.SAVE_DEPOSIT_ITEM_ERROR
        ],
        query: item
    })
        .then(() => {
            dispatch(goBack());
            dispatch(toggleBackdropLoader(false))
        })
        .catch(() => {
            dispatch(toggleBackdropLoader(false))
        })
};
