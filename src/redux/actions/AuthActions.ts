import { push } from "react-router-redux";
import * as authActionTypes from "../actionTypes/AuthActionTypes";
import * as authApi from "../../api/AuthApi";
import { toggleBackdropLoader, toggleSnackbar } from "./MessageActions";
// import {notification} from "antd";

export const getUser = (payload?: any) => (dispatch, getState) => {
    dispatch(toggleBackdropLoader(true));

    return dispatch({
        api: authApi.getUser,
        types: [
            authActionTypes.GET_USER_INFO_REQUEST,
            authActionTypes.GET_USER_INFO_SUCCESS,
            authActionTypes.GET_USER_INFO_ERROR,
        ],
        query: {},
    })
        .then((response) => {
            dispatch(toggleBackdropLoader(false));
            return response;
        })
        .catch((error) => {
            dispatch(toggleBackdropLoader(false));
        });
};
export const auth = (payload) => (dispatch, getState) => {
    const {
        form: { login },
    } = getState();

    let username, password;

    if (payload) {
        username = payload.login;
        password = payload.password;
    } else {
        username = login.values.login;
        password = login.values.password;
    }
    dispatch(toggleBackdropLoader(true));

    dispatch({
        api: authApi.loginUser,
        types: [
            authActionTypes.AUTH_GET_USER_TOKEN_REQUEST,
            authActionTypes.AUTH_GET_USER_TOKEN_SUCCESS,
            authActionTypes.AUTH_GET_USER_TOKEN_ERROR,
        ],
        query: {
            username,
            password,
        },
        hideErrorNotification: true,
    })
        .then((response) => {
            dispatch(toggleBackdropLoader(false));

            if (
                response &&
                response.payload &&
                response.payload &&
                response.responseStatus &&
                response.responseStatus === 200
            ) {
                localStorage.setItem("token", response.payload.data.accessToken);
                localStorage.setItem("tokenType", response.payload.data.tokenType);

                dispatch(getUser()).then(response => {
                    if (response && response.payload) {
                        console.log("get user ", response)
                        if (response.payload.userRole === "ADMIN" || response.payload.userRole === "GARAGE_ADMIN") {

                            dispatch(push("/"));
                        } else {
                            localStorage.removeItem("token");
                            localStorage.removeItem("tokenType");
                            dispatch(toggleSnackbar("You haven't access"))
                        }
                    }
                })
            }
        })
        .catch((error) => {
            dispatch(toggleBackdropLoader(false));
        });
};

export const restoreAuth = () => (dispatch, getState) => {
    dispatch({
        type: authActionTypes.AUTH_RESTORE,
    });
};

export const logout = () => (dispatch, getState) => {
    dispatch({
        type: authActionTypes.AUTH_LOGOUT,
    });
};
