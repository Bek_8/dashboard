import {push, goBack} from "react-router-redux";
import * as ActionType from "../actionTypes/AutoServiceActionTypes";
import {list, item, createItem, updateItem, deleteItem} from "../../api/ReferenceApi";
import {toggleBackdropLoader} from "./MessageActions";


export const listAction = (params = {
    start: 0,
    limit: 10,
    searchKey: null,
    sortField: null,
    sortOrder: null
}) => (dispatch) => {
    dispatch({
        api: list,
        types: [
            ActionType.LOAD_AUTO_SERVICE_LIST_START,
            ActionType.LOAD_AUTO_SERVICE_LIST_SUCCESS,
            ActionType.LOAD_AUTO_SERVICE_LIST_ERROR
        ],
        query: params
    })
};
// Aziz Aka @ Java, [13.12.19 11:15]
// Spravochnik #1: SP Categories (Parent Code: "_SERVICE_PROVIDER_CATEGORIES")
//
// Aziz Aka @ Java, [13.12.19 11:15]
// Spravochnik #2: Auto Service Categories (Parent Code: "AUTO_SERVICE")
//
// Aziz Aka @ Java, [13.12.19 11:16]
// Spravochnik #3: Oil Station Categories: (Parent Code: "OIL_STATION")

export const itemAction = (id) => (dispatch) => {
    if (!id) {
        return;
    }
    dispatch({
        api: item,
        types: [
            ActionType.LOAD_AUTO_SERVICE_ITEM_START,
            ActionType.LOAD_AUTO_SERVICE_ITEM_SUCCESS,
            ActionType.LOAD_AUTO_SERVICE_ITEM_ERROR
        ],
        query: id
    });
};


export const clearItem = () => (dispatch) => {
    dispatch({
        type: ActionType.LOAD_AUTO_SERVICE_ITEM_ERROR,
        payload: null
    });
};

export const deleteItemAction = (id) => (dispatch) => {
    return dispatch({
        api: deleteItem,
        types: [
            ActionType.DELETE_AUTO_SERVICE_ITEM_START,
            ActionType.DELETE_AUTO_SERVICE_ITEM_SUCCESS,
            ActionType.DELETE_AUTO_SERVICE_ITEM_ERROR
        ],
        query: id
    })
};

export const saveItem = (item) => (dispatch) => {
    if (item === null) {
        return
    }
    let api = updateItem;
    if (!item.id) {
        api = createItem;
    }
    dispatch(toggleBackdropLoader(true))

    dispatch({
        api,
        types: [
            ActionType.SAVE_AUTO_SERVICE_ITEM_START,
            ActionType.SAVE_AUTO_SERVICE_ITEM_SUCCESS,
            ActionType.SAVE_AUTO_SERVICE_ITEM_ERROR
        ],
        query: item
    })
        .then(() => {
            dispatch(goBack());
            dispatch(toggleBackdropLoader(false))
        })
        .catch(() => {
            dispatch(toggleBackdropLoader(false))
        })
};
