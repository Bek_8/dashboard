import * as modification from "../actionTypes/ModificationActionTypes";
import {createItem, deleteItem, item, list, updateItem} from "../../api/ModificationApi";

export const listAction = (params:any = {
    start: 0,
    limit: 10,
    searchKey: null,
    sortField: null,
    sortOrder: null
}) => (dispatch) => {
    dispatch({
        api: list,
        types: [
            modification.LOAD_MODIFICATION_LIST_START,
            modification.LOAD_MODIFICATION_LIST_SUCCESS,
            modification.LOAD_MODIFICATION_LIST_ERROR
        ],
        query: params
    })
};

export const itemAction = (id) => (dispatch) => {
    if (!id) {
        return;
    }
    dispatch({
        api: item,
        types: [
            modification.LOAD_MODIFICATION_ITEM_START,
            modification.LOAD_MODIFICATION_ITEM_SUCCESS,
            modification.LOAD_MODIFICATION_ITEM_ERROR
        ],
        query: {
            id
        }
    });
};


export const clearItem = () => (dispatch) => {
    dispatch({
        type: modification.LOAD_MODIFICATION_ITEM_ERROR,
        payload: null
    });
};

export const deleteItemAction = (id, modelId) => (dispatch) => {
    dispatch({
        api: deleteItem,
        types: [
            modification.DELETE_MODIFICATION_ITEM_START,
            modification.DELETE_MODIFICATION_ITEM_SUCCESS,
            modification.DELETE_MODIFICATION_ITEM_ERROR
        ],
        query: {
            id
        }
    })
        .then(() => dispatch(listAction({modelId: modelId})))
};

export const saveItem = (item) => (dispatch) => {
    if (item === null) {
        return
    }
    let api = updateItem;
    if (!item.id) {
        api = createItem;
    }
    dispatch({
        api,
        types: [
            modification.SAVE_MODIFICATION_ITEM_START,
            modification.SAVE_MODIFICATION_ITEM_SUCCESS,
            modification.SAVE_MODIFICATION_ITEM_ERROR
        ],
        query: item
    })
        .then(() => {
            dispatch(listAction({modelId: item.modelId}))
        });
};
