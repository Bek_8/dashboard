import * as ActionTypes from "../actionTypes/ReferenceActionTypes";
import * as ReferenceApi from "../../api/ReferenceApi";
import {
    list, createItem,
    deleteItem, item, updateItem
} from "../../api/ReferenceApi";
import * as ActionType from "../actionTypes/ReferenceActionTypes";
import {goBack} from "react-router-redux";
import {toggleBackdropLoader} from "./MessageActions";


export const listAction = (query) => (dispatch) => {
    dispatch({
        api: list,
        types: [
            ActionTypes.LOAD_REFERENCE_LISTING_START,
            ActionTypes.LOAD_REFERENCE_LISTING_SUCCESS,
            ActionTypes.LOAD_REFERENCE_LISTING_ERROR
        ],
        query
    });
};

export const itemAction = (id) => (dispatch) => {
    if (!id) {
        return;
    }
    dispatch({
        api: item,
        types: [
            ActionType.LOAD_REFERENCE_ITEM_START,
            ActionType.LOAD_REFERENCE_ITEM_SUCCESS,
            ActionType.LOAD_REFERENCE_ITEM_ERROR
        ],
        query: id
    });
};


export const clearItem = () => (dispatch) => {
    dispatch({
        type: ActionType.LOAD_REFERENCE_ITEM_ERROR,
        payload: null
    });
};

export const deleteItemAction = (id) => (dispatch) => {
    return dispatch({
        api: deleteItem,
        types: [
            ActionType.DELETE_REFERENCE_ITEM_START,
            ActionType.DELETE_REFERENCE_ITEM_SUCCESS,
            ActionType.DELETE_REFERENCE_ITEM_ERROR
        ],
        query: id
    })
};

export const saveItem = (item) => (dispatch) => {
    if (item === null) {
        return
    }
    let api = updateItem;
    if (!item.id) {
        api = createItem;
    }
    dispatch(toggleBackdropLoader(true))

    dispatch({
        api,
        types: [
            ActionType.SAVE_REFERENCE_ITEM_START,
            ActionType.SAVE_REFERENCE_ITEM_SUCCESS,
            ActionType.SAVE_REFERENCE_ITEM_ERROR
        ],
        query: item
    })
        .then(() => {
            dispatch(goBack());
            dispatch(toggleBackdropLoader(false))
        })
        .catch(() => {
            dispatch(toggleBackdropLoader(false))
        })
};


export const selectCategoryTypes = (parentCode) => (dispatch) => {
    dispatch({
        api: ReferenceApi.getAllCategoryTypes,
        types: [
            ActionTypes.LOAD_REFERENCE_CATEGORY_TYPE_START,
            ActionTypes.LOAD_REFERENCE_CATEGORY_TYPE_SUCCESS,
            ActionTypes.LOAD_REFERENCE_CATEGORY_TYPE_ERROR
        ],
        query: {
            parentCode: parentCode || "_SERVICE_PROVIDER_CATEGORIES"
        }
    });
};

export const getReferenceChildChoose = (parentCode) => (dispatch) => {
    dispatch({
        api: ReferenceApi.getAllCategoryTypes,
        types: [
            ActionTypes.LOAD_REFERENCE_CHILD_CHOOSE_LIST_START,
            ActionTypes.LOAD_REFERENCE_CHILD_CHOOSE_LIST_SUCCESS,
            ActionTypes.LOAD_REFERENCE_CHILD_CHOOSE_LIST_ERROR
        ],
        query: {
            parentCode: parentCode
        }
    });
};

export const getSelectGenders = () => (dispatch) => {
    dispatch({
        api: ReferenceApi.getGenders,
        types: [
            ActionTypes.LOAD_REFERENCE_GENDERS_START,
            ActionTypes.LOAD_REFERENCE_GENDERS_SUCCESS,
            ActionTypes.LOAD_REFERENCE_GENDERS_ERROR
        ],
    });
};


export const getSelectStatuses = () => (dispatch) => {
    dispatch({
        api: ReferenceApi.getStatuses,
        types: [
            ActionTypes.LOAD_REFERENCE_STATUS_LIST_START,
            ActionTypes.LOAD_REFERENCE_STATUS_LIST_SUCCESS,
            ActionTypes.LOAD_REFERENCE_STATUS_LIST_ERROR
        ],
    });
};


export const getSelectRoles = () => (dispatch) => {
    dispatch({
        api: ReferenceApi.getRoles,
        types: [
            ActionTypes.LOAD_REFERENCE_USER_ROLES_START,
            ActionTypes.LOAD_REFERENCE_USER_ROLES_SUCCESS,
            ActionTypes.LOAD_REFERENCE_USER_ROLES_ERROR
        ],
    });
};


export const getSelectRegions = () => (dispatch) => {
    dispatch({
        api: ReferenceApi.getRegions,
        types: [
            ActionTypes.LOAD_REFERENCE_REGIONS_START,
            ActionTypes.LOAD_REFERENCE_REGIONS_SUCCESS,
            ActionTypes.LOAD_REFERENCE_REGIONS_ERROR
        ],
    });
};


export const getSelectDistricts = (regionId) => (dispatch) => {
    dispatch({
        api: ReferenceApi.getDistricts,
        types: [
            ActionTypes.LOAD_REFERENCE_DISTRICTS_START,
            ActionTypes.LOAD_REFERENCE_DISTRICTS_SUCCESS,
            ActionTypes.LOAD_REFERENCE_DISTRICTS_ERROR
        ],
        query: regionId
    });
};


export const getSelectWeekdays = () => (dispatch) => {
    dispatch({
        api: ReferenceApi.getWeekdays,
        types: [
            ActionTypes.LOAD_REFERENCE_WEEKDAYS_START,
            ActionTypes.LOAD_REFERENCE_WEEKDAYS_SUCCESS,
            ActionTypes.LOAD_REFERENCE_WEEKDAYS_ERROR
        ],
    });
};

