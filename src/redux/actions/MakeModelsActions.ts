import {push, goBack} from "react-router-redux";
import * as ActionType from "../actionTypes/MakeModelsActionTypes";
import {list, item, createItem, updateItem, deleteItem} from "../../api/ReferenceApi";
import {uploadFile} from "../../api/FileApi";

export const listAction = (params = {
    start: 0,
    limit: 10,
    searchKey: null,
    sortField: null,
    sortOrder: null
}) => (dispatch) => {
    dispatch({
        api: list,
        types: [
            ActionType.LOAD_MAKE_MODEL_LIST_START,
            ActionType.LOAD_MAKE_MODEL_LIST_SUCCESS,
            ActionType.LOAD_MAKE_MODEL_LIST_ERROR
        ],
        query: params
    })
};
// Aziz Aka @ Java, [13.12.19 11:15]
// Spravochnik #1: SP Categories (Parent Code: "_SERVICE_PROVIDER_CATEGORIES")
//
// Aziz Aka @ Java, [13.12.19 11:15]
// Spravochnik #2: Auto Service Categories (Parent Code: "MAKE_MODEL")
//
// Aziz Aka @ Java, [13.12.19 11:16]
// Spravochnik #3: Oil Station Categories: (Parent Code: "MAKE_MODEL")

export const itemAction = (id) => (dispatch) => {
    if (!id) {
        return;
    }
    dispatch({
        api: item,
        types: [
            ActionType.LOAD_MAKE_MODEL_ITEM_START,
            ActionType.LOAD_MAKE_MODEL_ITEM_SUCCESS,
            ActionType.LOAD_MAKE_MODEL_ITEM_ERROR
        ],
        query: id
    });
};


export const clearItem = () => (dispatch) => {
    dispatch({
        type: ActionType.LOAD_MAKE_MODEL_ITEM_ERROR,
        payload: null
    });
};

export const deleteItemAction = (id) => (dispatch) => {
    return dispatch({
        api: deleteItem,
        types: [
            ActionType.DELETE_MAKE_MODEL_ITEM_START,
            ActionType.DELETE_MAKE_MODEL_ITEM_SUCCESS,
            ActionType.DELETE_MAKE_MODEL_ITEM_ERROR
        ],
        query: id
    })
};

export const saveItem = ({image,...payload}) => async (dispatch) => {
    if (payload === null) {
        return
    }
    if(image) {
        const formData = new FormData();
        formData.append('uploadFile', image);
        const imageResponse :any=  await uploadFile(formData);
        console.log("image res --", imageResponse)
        payload.imageId = imageResponse?.data?.data.id;
    }
    let api = updateItem;
    if (!payload.id) {
        api = createItem;
    }
    dispatch({
        api,
        types: [
            ActionType.SAVE_MAKE_MODEL_ITEM_START,
            ActionType.SAVE_MAKE_MODEL_ITEM_SUCCESS,
            ActionType.SAVE_MAKE_MODEL_ITEM_ERROR
        ],
        query: payload
    })
        .then(() => {
            dispatch(goBack());
        });
};
