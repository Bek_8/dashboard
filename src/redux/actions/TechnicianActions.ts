import {goBack} from "react-router-redux";
import * as TechnicianType from "../actionTypes/TechnicianActionTypes";
import {createItem, deleteItem, item, list, updateItem} from "../../api/TechnicianApi";
import * as ServiceApi from "../../api/ServiceApi";
import * as DateUtils from "../../utils/DateUtils";
import {convertAdditionalCostsForPayload} from "../../utils/ProviderUtils";
import {toggleBackdropLoader} from "./MessageActions";
import {uploadFile} from "../../api/FileApi";

export const listAction = (params = {
    start: 0,
    limit: 10,
    searchKey: null,
    sortField: null,
    sortOrder: null
}) => (dispatch) => {
    dispatch({
        api: list,
        types: [
            TechnicianType.LOAD_TECHNICIAN_LIST_START,
            TechnicianType.LOAD_TECHNICIAN_LIST_SUCCESS,
            TechnicianType.LOAD_TECHNICIAN_LIST_ERROR
        ],
        query: params
    })
};

export const itemAction = (id) => (dispatch) => {
    if (!id) {
        return;
    }
    dispatch({
        api: item,
        types: [
            TechnicianType.LOAD_TECHNICIAN_ITEM_START,
            TechnicianType.LOAD_TECHNICIAN_ITEM_SUCCESS,
            TechnicianType.LOAD_TECHNICIAN_ITEM_ERROR
        ],
        query: id
    });
};


export const clearItem = () => (dispatch) => {
    dispatch({
        type: TechnicianType.LOAD_TECHNICIAN_ITEM_ERROR,
        payload: null
    });
};

export const deleteItemAction = (id) => (dispatch) => {
    return dispatch({
        api: deleteItem,
        types: [
            TechnicianType.DELETE_TECHNICIAN_ITEM_START,
            TechnicianType.DELETE_TECHNICIAN_ITEM_SUCCESS,
            TechnicianType.DELETE_TECHNICIAN_ITEM_ERROR
        ],
        query: id
    })
};

export const saveItem = ({image,hasCommission, commission,...payload}) => async (dispatch) => {
    if (payload === null) {
        return
    }
    let api = updateItem;
    if (!payload.id) {
        api = createItem;
    }
    dispatch(toggleBackdropLoader(true))
    if (image) {
        const formData = new FormData();
        formData.append('uploadFile', image);
        const imageResponse: any = await uploadFile(formData);
        console.log("image res --", imageResponse)
        payload.imageId = imageResponse?.data?.data.id;
    }
        payload.comissionId = commission?.id || null;
    dispatch({
        api,
        types: [
            TechnicianType.SAVE_TECHNICIAN_ITEM_START,
            TechnicianType.SAVE_TECHNICIAN_ITEM_SUCCESS,
            TechnicianType.SAVE_TECHNICIAN_ITEM_ERROR
        ],
        query: convertAdditionalCostsForPayload(DateUtils.convertTimeSlotsPayload(payload))
    })
        .then(() => {
            dispatch(goBack());
            dispatch(toggleBackdropLoader(false))
        })
        .catch(() => {
            dispatch(toggleBackdropLoader(false))
        })
};


export const getServicesByCategory = (parentCategoryId, providerId) =>
    (dispatch) => {
        dispatch({
            api: ServiceApi.list,
            types: [
                TechnicianType.GET_SERVICES_BY_CATEGORY_START,
                TechnicianType.GET_SERVICES_BY_CATEGORY_SUCCESS,
                TechnicianType.GET_SERVICES_BY_CATEGORY_ERROR
            ],
            query: {
                parentCategoryId,
                providerId,
                limit: 50
            }
        })
    };
