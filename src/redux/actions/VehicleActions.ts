import {push} from "react-router-redux";
import * as vehicle from "../actionTypes/VehicleActionTypes";
import {list, item, createItem, updateItem, deleteItem} from "../../api/VehicleApi";

export const listAction = (params = {
    start: 0,
    limit: 10,
    searchKey: null,
    sortField: null,
    sortOrder: null
}) => (dispatch) => {
    dispatch({
        api: list,
        types: [
            vehicle.LOAD_VEHICLE_LIST_START,
            vehicle.LOAD_VEHICLE_LIST_SUCCESS,
            vehicle.LOAD_VEHICLE_LIST_ERROR
        ],
        query: params
    })
};

export const itemAction = (id) => (dispatch) => {
    if (!id) {
        return;
    }
    dispatch({
        api: item,
        types: [
            vehicle.LOAD_VEHICLE_ITEM_START,
            vehicle.LOAD_VEHICLE_ITEM_SUCCESS,
            vehicle.LOAD_VEHICLE_ITEM_ERROR
        ],
        query: {
            id
        }
    });
};


export const clearItem = () => (dispatch) => {
    dispatch({
        type: vehicle.LOAD_VEHICLE_ITEM_ERROR,
        payload: null
    });
};

export const deleteItemAction = (id) => (dispatch) => {
    dispatch({
        api: deleteItem,
        types: [
            vehicle.DELETE_VEHICLE_ITEM_START,
            vehicle.DELETE_VEHICLE_ITEM_SUCCESS,
            vehicle.DELETE_VEHICLE_ITEM_ERROR
        ],
        query: {
            id
        }
    })
        .then(() => dispatch(listAction()))
};

export const saveItem = (item) => (dispatch) => {
    if (item === null) {
        return
    }
    let api = updateItem;
    if (!item.id) {
        api = createItem;
    }
    dispatch({
        api,
        types: [
            vehicle.SAVE_VEHICLE_ITEM_START,
            vehicle.SAVE_VEHICLE_ITEM_SUCCESS,
            vehicle.SAVE_VEHICLE_ITEM_ERROR
        ],
        query: item
    })
        .then(() => {
            dispatch(push(`/vehicle/list`));
            dispatch(listAction())
        });
};
