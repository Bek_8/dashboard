import {push, goBack} from "react-router-redux";
import * as ActionType from "../actionTypes/OilStationActionTypes";
import {list, item, createItem, updateItem, deleteItem} from "../../api/ReferenceApi";

export const listAction = (params = {
    start: 0,
    limit: 10,
    searchKey: null,
    sortField: null,
    sortOrder: null
}) => (dispatch) => {
    dispatch({
        api: list,
        types: [
            ActionType.LOAD_OIL_STATION_LIST_START,
            ActionType.LOAD_OIL_STATION_LIST_SUCCESS,
            ActionType.LOAD_OIL_STATION_LIST_ERROR
        ],
        query: params
    })
};
// Aziz Aka @ Java, [13.12.19 11:15]
// Spravochnik #1: SP Categories (Parent Code: "_SERVICE_PROVIDER_CATEGORIES")
//
// Aziz Aka @ Java, [13.12.19 11:15]
// Spravochnik #2: Auto Service Categories (Parent Code: "OIL_STATION")
//
// Aziz Aka @ Java, [13.12.19 11:16]
// Spravochnik #3: Oil Station Categories: (Parent Code: "OIL_STATION")

export const itemAction = (id) => (dispatch) => {
    if (!id) {
        return;
    }
    dispatch({
        api: item,
        types: [
            ActionType.LOAD_OIL_STATION_ITEM_START,
            ActionType.LOAD_OIL_STATION_ITEM_SUCCESS,
            ActionType.LOAD_OIL_STATION_ITEM_ERROR
        ],
        query: id
    });
};


export const clearItem = () => (dispatch) => {
    dispatch({
        type: ActionType.LOAD_OIL_STATION_ITEM_ERROR,
        payload: null
    });
};

export const deleteItemAction = (id) => (dispatch) => {
    return dispatch({
        api: deleteItem,
        types: [
            ActionType.DELETE_OIL_STATION_ITEM_START,
            ActionType.DELETE_OIL_STATION_ITEM_SUCCESS,
            ActionType.DELETE_OIL_STATION_ITEM_ERROR
        ],
        query: id
    })
};

export const saveItem = (item) => (dispatch) => {
    if (item === null) {
        return
    }
    let api = updateItem;
    if (!item.id) {
        api = createItem;
    }
    dispatch({
        api,
        types: [
            ActionType.SAVE_OIL_STATION_ITEM_START,
            ActionType.SAVE_OIL_STATION_ITEM_SUCCESS,
            ActionType.SAVE_OIL_STATION_ITEM_ERROR
        ],
        query: item
    })
        .then(() => {
            dispatch(goBack());
        });
};
