import { goBack} from "react-router-redux";
import * as CommissionActionTypes from "../actionTypes/CommissionActionTypes";
import {list, item, createItem, updateItem, deleteItem} from "../../api/CommissionApi";
import {toggleBackdropLoader} from "./MessageActions";
// import {getCommissionItemPayload} from "../utils/ProviderUtils";

export const getCommissionList = (params?: { searchKey: string }) => (dispatch) => {
    dispatch({
        api: list,
        types: [
            CommissionActionTypes.LOAD_COMMISSION_LIST_START,
            CommissionActionTypes.LOAD_COMMISSION_LIST_SUCCESS,
            CommissionActionTypes.LOAD_COMMISSION_LIST_ERROR
        ],
        query: params
    })
};

export const getCommissionItemAction = (id) =>
    (dispatch) => {
        if (!id) {
            return;
        }
        dispatch({
            api: item,
            types: [
                CommissionActionTypes.LOAD_COMMISSION_ITEM_START,
                CommissionActionTypes.LOAD_COMMISSION_ITEM_SUCCESS,
                CommissionActionTypes.LOAD_COMMISSION_ITEM_ERROR
            ],
            query:id
        });
    };


export const clearItem = () => (dispatch) => {
    dispatch({
        type: CommissionActionTypes.LOAD_COMMISSION_ITEM_ERROR,
        payload: null
    });
};

export const deleteItemAction = (id) => (dispatch) => {
    dispatch({
        api: deleteItem,
        types: [
            CommissionActionTypes.DELETE_COMMISSION_ITEM_START,
            CommissionActionTypes.DELETE_COMMISSION_ITEM_SUCCESS,
            CommissionActionTypes.DELETE_COMMISSION_ITEM_ERROR
        ],
        query: {
            id
        }
    })
};

export const saveItem = (item) => (dispatch) => {
    if (item === null) {
        return
    }
    let api = updateItem;
    if (!item.id) {
        api = createItem;
    }
    dispatch(toggleBackdropLoader(true))

    dispatch({
        api,
        types: [
            CommissionActionTypes.SAVE_COMMISSION_ITEM_START,
            CommissionActionTypes.SAVE_COMMISSION_ITEM_SUCCESS,
            CommissionActionTypes.SAVE_COMMISSION_ITEM_ERROR
        ],
        query: item
    })
        .then(() => {
            dispatch(goBack());
            dispatch(toggleBackdropLoader(false))
        })
        .catch(() => {
            dispatch(toggleBackdropLoader(false))
        })
};
