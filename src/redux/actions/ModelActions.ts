import {push} from "react-router-redux";
import * as model from "../actionTypes/ModelActionTypes";
import {list, item, createItem, updateItem, deleteItem} from "../../api/ModelApi";

export const listAction = (params = {
    start: 0,
    limit: 10,
    searchKey: null,
    sortField: null,
    sortOrder: null
}) => (dispatch) => {
    dispatch({
        api: list,
        types: [
            model.LOAD_MODEL_LIST_START,
            model.LOAD_MODEL_LIST_SUCCESS,
            model.LOAD_MODEL_LIST_ERROR
        ],
        query: params
    })
};

export const itemAction = (id) => (dispatch) => {
    if (!id) {
        return;
    }
    dispatch({
        api: item,
        types: [
            model.LOAD_MODEL_ITEM_START,
            model.LOAD_MODEL_ITEM_SUCCESS,
            model.LOAD_MODEL_ITEM_ERROR
        ],
        query: {
            id
        }
    });
};


export const clearItem = () => (dispatch) => {
    dispatch({
        type: model.LOAD_MODEL_ITEM_ERROR,
        payload: null
    });
};

export const deleteItemAction = (id) => (dispatch) => {
    dispatch({
        api: deleteItem,
        types: [
            model.DELETE_MODEL_ITEM_START,
            model.DELETE_MODEL_ITEM_SUCCESS,
            model.DELETE_MODEL_ITEM_ERROR
        ],
        query: {
            id
        }
    })
        .then(() => dispatch(listAction()))
};

export const saveItem = (item) => (dispatch) => {
    if (item === null) {
        return
    }
    let api = updateItem;
    if (!item.id) {
        api = createItem;
    }
    dispatch({
        api,
        types: [
            model.SAVE_MODEL_ITEM_START,
            model.SAVE_MODEL_ITEM_SUCCESS,
            model.SAVE_MODEL_ITEM_ERROR
        ],
        query: item
    })
        .then(() => {
            dispatch(push(`/model/list`));
            dispatch(listAction())
        });
};
