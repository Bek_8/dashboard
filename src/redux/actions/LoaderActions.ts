// import Nprogress from "nprogress";

import * as loaderActionTypes from "../actionTypes/LoaderActionTypes";

export const start = () => (dispatch, getState) => {
  const {
    loader: {
      count
    }
  } = getState();

  if (count === 0) {
    // console.log("#################################Start loader#################################")
    // Nprogress.start();
  }


  dispatch({
    type: loaderActionTypes.LOADER_START,
  })
};

export const stop = () => (dispatch, getState) => {
  const {
    loader: {
      count
    }
  } = getState();

  if (count === 1) {
    // console.log('#################################Stop loader#################################');
      // Nprogress.done();
  }

  dispatch({
    type: loaderActionTypes.LOADER_STOP,
  })
};