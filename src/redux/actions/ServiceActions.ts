import {push, goBack} from "react-router-redux";
import * as service from "../actionTypes/ServiceActionTypes";
import  {list, item, createItem, updateItem, deleteItem}from "../../api/ServiceApi";
import  * as ServiceApi from "../../api/ServiceApi";
import {convertServicePayload} from "../../utils/ProviderUtils";
import {toggleBackdropLoader} from "./MessageActions";

export const listAction = (params = {
    start: 0,
    limit: 10,
    searchKey: null,
    sortField: null,
    sortOrder: null
}) => (dispatch) => {
    dispatch({
        api: list,
        types: [
            service.LOAD_SERVICE_LIST_START,
            service.LOAD_SERVICE_LIST_SUCCESS,
            service.LOAD_SERVICE_LIST_ERROR
        ],
        query: params
    })
};

export const itemAction = (id) => (dispatch) => {
    if (!id) {
        return;
    }
    dispatch({
        api: item,
        types: [
            service.LOAD_SERVICE_ITEM_START,
            service.LOAD_SERVICE_ITEM_SUCCESS,
            service.LOAD_SERVICE_ITEM_ERROR
        ],
        query: id
    });
};


export const clearItem = () => (dispatch) => {
    dispatch({
        type: service.LOAD_SERVICE_ITEM_ERROR,
        payload: null
    });
};

export const deleteItemAction = (id) => (dispatch) => {
    return dispatch({
        api: deleteItem,
        types: [
            service.DELETE_SERVICE_ITEM_START,
            service.DELETE_SERVICE_ITEM_SUCCESS,
            service.DELETE_SERVICE_ITEM_ERROR
        ],
        query: id
    })
};

export const saveItem = (item) => (dispatch) => {
    if (item === null) {
        return
    }
    let api = updateItem;
    if (!item.id) {
        api = createItem;
    }
    dispatch(toggleBackdropLoader(true))

    dispatch({
        api,
        types: [
            service.SAVE_SERVICE_ITEM_START,
            service.SAVE_SERVICE_ITEM_SUCCESS,
            service.SAVE_SERVICE_ITEM_ERROR
        ],
        query: {...convertServicePayload(item), price: Number(item.price)}
    })
        .then(() => {
            dispatch(goBack());
            dispatch(toggleBackdropLoader(false))
        })
        .catch(()=>{
            dispatch(toggleBackdropLoader(false))
        })
};

export const getSubListAction = (params = {
    start: 0,
    limit: 10,
    searchKey: null,
    sortField: null,
    sortOrder: null
}) => (dispatch) => {
    dispatch({
        api: ServiceApi.getSubList,
        types: [
            service.LOAD_SERVICE_SUB_LIST_START,
            service.LOAD_SERVICE_SUB_LIST_SUCCESS,
            service.LOAD_SERVICE_SUB_LIST_ERROR
        ],
        query: params
    })
};

export const getSubItemAction = (id) => (dispatch) => {
    if (!id) {
        return;
    }
    dispatch({
        api: ServiceApi.getSubItem,
        types: [
            service.LOAD_SERVICE_SUB_ITEM_START,
            service.LOAD_SERVICE_SUB_ITEM_SUCCESS,
            service.LOAD_SERVICE_SUB_ITEM_ERROR
        ],
        query: id
    });
};


export const clearSubItem = () => (dispatch) => {
    dispatch({
        type: service.LOAD_SERVICE_SUB_ITEM_ERROR,
        payload: null
    });
};

export const deleteSubItemAction = (id) => (dispatch) => {
    return dispatch({
        api: ServiceApi.deleteSubItem,
        types: [
            service.DELETE_SERVICE_SUB_ITEM_START,
            service.DELETE_SERVICE_SUB_ITEM_SUCCESS,
            service.DELETE_SERVICE_SUB_ITEM_ERROR
        ],
        query: id
    })
};

export const saveSubItem = (item) => (dispatch) => {
    if (item === null) {
        return
    }
    let api = ServiceApi.updateSubItem;
    if (!item.id) {
        api = ServiceApi.createSubItem;
    }
    dispatch(toggleBackdropLoader(true))

    dispatch({
        api,
        types: [
            service.SAVE_SERVICE_SUB_ITEM_START,
            service.SAVE_SERVICE_SUB_ITEM_SUCCESS,
            service.SAVE_SERVICE_SUB_ITEM_ERROR
        ],
        query: {...item, price: Number(item.price)}
    })
        .then(() => {
            // dispatch(goBack());
            dispatch(toggleBackdropLoader(false))
        })
        .catch(()=>{
            dispatch(toggleBackdropLoader(false))
        })
};
