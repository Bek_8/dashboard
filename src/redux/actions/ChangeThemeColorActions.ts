import * as  changeColor from "../actionTypes/ChangeThemeColorActionTypes";
import {CHANGE_BACKGROUND_COLOR} from "../actionTypes/ChangeThemeColorActionTypes";

export const changeThemeColor = (color:string) => (dispatch) => {
    dispatch({
        type: changeColor.CHANGE_BACKGROUND_COLOR,
        color
    })
};