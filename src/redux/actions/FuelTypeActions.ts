import {push} from "react-router-redux";
import * as fuelType from "../actionTypes/FuelTypeActionTypes";
import {list, item, createItem, updateItem, deleteItem} from "../../api/FuelTypeApi";

export const listAction = (params = {
    start: 0,
    limit: 10,
    searchKey: null,
    sortField: null,
    sortOrder: null
}) => (dispatch) => {
    dispatch({
        api: list,
        types: [
            fuelType.LOAD_FUEL_TYPE_LIST_START,
            fuelType.LOAD_FUEL_TYPE_LIST_SUCCESS,
            fuelType.LOAD_FUEL_TYPE_LIST_ERROR
        ],
        query: params
    })
};

export const itemAction = (id) => (dispatch) => {
    if (!id) {
        return;
    }
    dispatch({
        api: item,
        types: [
            fuelType.LOAD_FUEL_TYPE_ITEM_START,
            fuelType.LOAD_FUEL_TYPE_ITEM_SUCCESS,
            fuelType.LOAD_FUEL_TYPE_ITEM_ERROR
        ],
        query: {
            id
        }
    });
};


export const clearItem = () => (dispatch) => {
    dispatch({
        type: fuelType.LOAD_FUEL_TYPE_ITEM_ERROR,
        payload: null
    });
};

export const deleteItemAction = (id) => (dispatch) => {
    dispatch({
        api: deleteItem,
        types: [
            fuelType.DELETE_FUEL_TYPE_ITEM_START,
            fuelType.DELETE_FUEL_TYPE_ITEM_SUCCESS,
            fuelType.DELETE_FUEL_TYPE_ITEM_ERROR
        ],
        query: {
            id
        }
    })
        .then(() => dispatch(listAction()))
};

export const saveItem = (item) => (dispatch) => {
    if (item === null) {
        return
    }
    let api = updateItem;
    if (!item.id) {
        api = createItem;
    }
    dispatch({
        api,
        types: [
            fuelType.SAVE_FUEL_TYPE_ITEM_START,
            fuelType.SAVE_FUEL_TYPE_ITEM_SUCCESS,
            fuelType.SAVE_FUEL_TYPE_ITEM_ERROR
        ],
        query: item
    })
        .then(() => {
            dispatch(push(`/fuel-type`));
            dispatch(listAction())
        });
};
