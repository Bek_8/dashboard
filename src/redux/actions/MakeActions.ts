import {push} from "react-router-redux";
import * as make from "../actionTypes/MakeActionTypes";
import {list, item, createItem, updateItem, deleteItem} from "../../api/MakeApi";

export const listAction = (params = {
    start: 0,
    limit: 10,
    searchKey: null,
    sortField: null,
    sortOrder: null
}) => (dispatch) => {
    dispatch({
        api: list,
        types: [
            make.LOAD_MAKE_LIST_START,
            make.LOAD_MAKE_LIST_SUCCESS,
            make.LOAD_MAKE_LIST_ERROR
        ],
        query: params
    })
};

export const itemAction = (id) => (dispatch) => {
    if (!id) {
        return;
    }
    dispatch({
        api: item,
        types: [
            make.LOAD_MAKE_ITEM_START,
            make.LOAD_MAKE_ITEM_SUCCESS,
            make.LOAD_MAKE_ITEM_ERROR
        ],
        query: {
            id
        }
    });
};


export const clearItem = () => (dispatch) => {
    dispatch({
        type: make.LOAD_MAKE_ITEM_ERROR,
        payload: null
    });
};

export const deleteItemAction = (id) => (dispatch) => {
    dispatch({
        api: deleteItem,
        types: [
            make.DELETE_MAKE_ITEM_START,
            make.DELETE_MAKE_ITEM_SUCCESS,
            make.DELETE_MAKE_ITEM_ERROR
        ],
        query: {
            id
        }
    })
        .then(() => dispatch(listAction()))
};

export const saveItem = (item) => (dispatch) => {
    if (item === null) {
        return
    }
    let api = updateItem;
    if (!item.id) {
        api = createItem;
    }
    dispatch({
        api,
        types: [
            make.SAVE_MAKE_ITEM_START,
            make.SAVE_MAKE_ITEM_SUCCESS,
            make.SAVE_MAKE_ITEM_ERROR
        ],
        query: item
    })
        .then(() => {
            dispatch(push(`/make`));
            dispatch(listAction())
        });
};
