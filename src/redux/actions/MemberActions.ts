import {push} from "react-router-redux";
import * as member from "../actionTypes/MemberActionTypes";
import {list, item, createItem, updateItem, deleteItem} from "../../api/MemberApi";

export const listAction = (params = {
    start: 0,
    limit: 10,
    searchKey: null,
    sortField: null,
    sortOrder: null
}) => (dispatch) => {
    dispatch({
        api: list,
        types: [
            member.LOAD_MEMBER_LIST_START,
            member.LOAD_MEMBER_LIST_SUCCESS,
            member.LOAD_MEMBER_LIST_ERROR
        ],
        query: params
    })
};

export const itemAction = (id) => (dispatch) => {
    if (!id || id === null) {
        return;
    }
    dispatch({
        api: item,
        types: [
            member.LOAD_MEMBER_ITEM_START,
            member.LOAD_MEMBER_ITEM_SUCCESS,
            member.LOAD_MEMBER_ITEM_ERROR
        ],
        query: {
            id
        }
    });
};



export const clearItem = () => (dispatch) => {
    dispatch({
        type: member.LOAD_MEASUREMENT_ITEM_ERROR,
        payload: null
    });
};

export const deleteItemAction = (id) => (dispatch) => {
    dispatch({
        api: deleteItem,
        types: [
            member.DELETE_MEMBER_ITEM_START,
            member.DELETE_MEMBER_ITEM_SUCCESS,
            member.DELETE_MEMBER_ITEM_ERROR
        ],
        query: {
            id
        }
    })
        .then(() => dispatch(listAction()))
};

export const saveItem = (item) => (dispatch) => {
    if (item === null) {
        return
    }
    let api = updateItem;
    if (!item.id) {
        api = createItem;
    }
    dispatch({
        api,
        types: [
            member.SAVE_MEMBER_ITEM_START,
            member.SAVE_MEMBER_ITEM_SUCCESS,
            member.SAVE_MEMBER_ITEM_ERROR
        ],
        query: item
    })
        .then(() => {
            dispatch(push(`/member/list`));
            dispatch(listAction())
        });
};
