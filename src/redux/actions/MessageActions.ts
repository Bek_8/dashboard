// import Nprogress from "nprogress";

import * as MessageActionType from "../actionTypes/MessageActionTypes";

export const setDeleteConfirmationItem = (item) =>
    (dispatch, getState) => {
        dispatch({
            type: MessageActionType.SET_DELETE_CONFIRMATION_ITEM,
            item
        })
    };
export const toggleSnackbar = (message, messageVariant = 'error') =>
    (dispatch, getState) => {
        dispatch({
            type: MessageActionType.TOGGLE_SNACKBAR,
            message, messageVariant
        })
    };
export const toggleBackdropLoader = (visible) =>
    (dispatch, getState) => {
        dispatch({
            type: MessageActionType.TOGGLE_BACKDROP_LOADER,
            visible
        })
    };