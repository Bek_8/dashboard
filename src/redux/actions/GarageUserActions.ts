import {goBack, push} from "react-router-redux";
import * as garageUser from "../actionTypes/GarageUserActionTypes";
import {list, item, createItem, updateItem, deleteItem} from "../../api/GarageUserApi";

export const listAction = (params:any = {
    start: 0,
    limit: 10,
    searchKey: null,
    sortField: null,
    sortOrder: null
}) => (dispatch) => {
    dispatch({
        api: list,
        types: [
            garageUser.LOAD_GARAGE_USER_LIST_START,
            garageUser.LOAD_GARAGE_USER_LIST_SUCCESS,
            garageUser.LOAD_GARAGE_USER_LIST_ERROR
        ],
        query: params
    })
};

export const itemAction = (id) => (dispatch) => {
    if (!id) {
        return;
    }
    dispatch({
        api: item,
        types: [
            garageUser.LOAD_GARAGE_USER_ITEM_START,
            garageUser.LOAD_GARAGE_USER_ITEM_SUCCESS,
            garageUser.LOAD_GARAGE_USER_ITEM_ERROR
        ],
        query: {
            id
        }
    });
};


export const clearItem = () => (dispatch) => {
    dispatch({
        type: garageUser.LOAD_GARAGE_USER_ITEM_ERROR,
        payload: null
    });
};

export const deleteItemAction = (id) => (dispatch) => {
    dispatch({
        api: deleteItem,
        types: [
            garageUser.DELETE_GARAGE_USER_ITEM_START,
            garageUser.DELETE_GARAGE_USER_ITEM_SUCCESS,
            garageUser.DELETE_GARAGE_USER_ITEM_ERROR
        ],
        query: {
            id
        }
    })
        .then(() => dispatch(listAction()))
};

export const saveItem = (item) => (dispatch) => {
    if (item === null) {
        return
    }
    let api = updateItem;
    if (!item.id) {
        api = createItem;
    }
    dispatch({
        api,
        types: [
            garageUser.SAVE_GARAGE_USER_ITEM_START,
            garageUser.SAVE_GARAGE_USER_ITEM_SUCCESS,
            garageUser.SAVE_GARAGE_USER_ITEM_ERROR
        ],
        query: item
    })
        .then(() => {
            dispatch(goBack());
            dispatch(listAction())
        });
};
