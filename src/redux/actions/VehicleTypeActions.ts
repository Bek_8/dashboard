import {push} from "react-router-redux";
import * as vehicleType from "../actionTypes/VehicleTypeActionTypes";
import {list, item, createItem, updateItem, deleteItem} from "../../api/VehicleTypeApi";

export const listAction = (params = {
    start: 0,
    limit: 10,
    searchKey: null,
    sortField: null,
    sortOrder: null
}) => (dispatch) => {
    dispatch({
        api: list,
        types: [
            vehicleType.LOAD_VEHICLE_TYPE_LIST_START,
            vehicleType.LOAD_VEHICLE_TYPE_LIST_SUCCESS,
            vehicleType.LOAD_VEHICLE_TYPE_LIST_ERROR
        ],
        query: params
    })
};

export const itemAction = (id) => (dispatch) => {
    if (!id) {
        return;
    }
    dispatch({
        api: item,
        types: [
            vehicleType.LOAD_VEHICLE_TYPE_ITEM_START,
            vehicleType.LOAD_VEHICLE_TYPE_ITEM_SUCCESS,
            vehicleType.LOAD_VEHICLE_TYPE_ITEM_ERROR
        ],
        query: {
            id
        }
    });
};


export const clearItem = () => (dispatch) => {
    dispatch({
        type: vehicleType.LOAD_VEHICLE_TYPE_ITEM_ERROR,
        payload: null
    });
};

export const deleteItemAction = (id) => (dispatch) => {
    dispatch({
        api: deleteItem,
        types: [
            vehicleType.DELETE_VEHICLE_TYPE_ITEM_START,
            vehicleType.DELETE_VEHICLE_TYPE_ITEM_SUCCESS,
            vehicleType.DELETE_VEHICLE_TYPE_ITEM_ERROR
        ],
        query: {
            id
        }
    })
        .then(() => dispatch(listAction()))
};

export const saveItem = (item) => (dispatch) => {
    if (item === null) {
        return
    }
    let api = updateItem;
    if (!item.id) {
        api = createItem;
    }
    dispatch({
        api,
        types: [
            vehicleType.SAVE_VEHICLE_TYPE_ITEM_START,
            vehicleType.SAVE_VEHICLE_TYPE_ITEM_SUCCESS,
            vehicleType.SAVE_VEHICLE_TYPE_ITEM_ERROR
        ],
        query: item
    })
        .then(() => {
            dispatch(push(`/vehicle-type/list`));
            dispatch(listAction())
        });
};
