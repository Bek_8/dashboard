import {goBack, push} from "react-router-redux";
import * as ActionTypes from "../actionTypes/ProviderActionTypes";
import {createItem, deleteItem, item, list, updateItem} from "../../api/ProviderApi";
import * as DateUtils from "../../utils/DateUtils";
import {DEFAULT_FILTER} from "../../constants/Constants";
import {toggleBackdropLoader} from "./MessageActions";
import {uploadFile} from "../../api/FileApi";

export const getProviderList = (params = DEFAULT_FILTER) => (dispatch) => {
    dispatch({
        api: list,
        types: [
            ActionTypes.LOAD_PROVIDER_LIST_START,
            ActionTypes.LOAD_PROVIDER_LIST_SUCCESS,
            ActionTypes.LOAD_PROVIDER_LIST_ERROR
        ],
        query: params
    })
};

export const getProviderItemAction = (id) =>
    (dispatch) => {
        if (!id) {
            return;
        }
        dispatch({
            api: item,
            types: [
                ActionTypes.LOAD_PROVIDER_ITEM_START,
                ActionTypes.LOAD_PROVIDER_ITEM_SUCCESS,
                ActionTypes.LOAD_PROVIDER_ITEM_ERROR
            ],
            query: id
        });
    };


export const clearItem = () => (dispatch) => {
    dispatch({
        type: ActionTypes.LOAD_PROVIDER_ITEM_ERROR,
        payload: null
    });
};

export const deleteItemAction = (id) => (dispatch) => {
    return dispatch({
        api: deleteItem,
        types: [
            ActionTypes.DELETE_PROVIDER_ITEM_START,
            ActionTypes.DELETE_PROVIDER_ITEM_SUCCESS,
            ActionTypes.DELETE_PROVIDER_ITEM_ERROR
        ],
        query: id
    })
};

export const saveItem = ({image,comission, owner, ...payload}) =>async (dispatch) => {
    if (payload === null) {
        return
    }
    let api = updateItem;
    if (!payload.id) {
        api = createItem;
    }
    dispatch(toggleBackdropLoader(true))
    if(image) {
        const formData = new FormData();
        formData.append('uploadFile', image);
       const imageResponse :any=  await uploadFile(formData);
       console.log("image res --", imageResponse)
        payload.imageId = imageResponse?.data?.data.id;
    }
    if(owner){
        payload.ownerId =  owner?.id
    }
    if(comission){
        payload.comissionId =  comission?.id
    }
    return dispatch({
        api,
        types: [
            ActionTypes.SAVE_PROVIDER_ITEM_START,
            ActionTypes.SAVE_PROVIDER_ITEM_SUCCESS,
            ActionTypes.SAVE_PROVIDER_ITEM_ERROR
        ],
        query: DateUtils.convertTimeSlotsPayload(payload)
    })
        .then(async () => {

            dispatch(goBack());
            dispatch(toggleBackdropLoader(false))
        })
        .catch(() => {
            dispatch(toggleBackdropLoader(false))
        })
};
