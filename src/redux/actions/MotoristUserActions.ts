import {push} from "react-router-redux";
import * as motorist from "../actionTypes/MotoristActionTypes";
import {list, item, createItem, updateItem, deleteItem, getAllGenderTypes} from "../../api/MotoristUserApi";
import {getAllStatues} from "../../api/MotoristUserApi";

export const listAction = (params = {
    start: 0,
    limit: 10,
    searchKey: null,
    sortField: null,
    sortOrder: null
}) => (dispatch) => {
    dispatch({
        api: list,
        types: [
            motorist.LOAD_MOTORIST_LIST_START,
            motorist.LOAD_MOTORIST_LIST_SUCCESS,
            motorist.LOAD_MOTORIST_LIST_ERROR
        ],
        query: params
    })
};

export const itemAction = (id) => (dispatch) => {
    dispatch(selectStatuses());
    dispatch(selectGenderTypes());
    if (!id) {
        return;
    }
    dispatch({
        api: item,
        types: [
            motorist.LOAD_MOTORIST_ITEM_START,
            motorist.LOAD_MOTORIST_ITEM_SUCCESS,
            motorist.LOAD_MOTORIST_ITEM_ERROR
        ],
        query: {
            id
        }
    });
};


export const clearItem = () => (dispatch) => {
    dispatch({
        type: motorist.LOAD_MOTORIST_ITEM_ERROR,
        payload: null
    });
};

export const deleteItemAction = (id) => (dispatch) => {
    dispatch({
        api: deleteItem,
        types: [
            motorist.DELETE_MOTORIST_ITEM_START,
            motorist.DELETE_MOTORIST_ITEM_SUCCESS,
            motorist.DELETE_MOTORIST_ITEM_ERROR
        ],
        query: {
            id
        }
    })
        .then(() => dispatch(listAction()))
};

export const saveItem = (item) => (dispatch) => {
    if (item === null) {
        return
    }
    let api = updateItem;
    if (!item.id) {
        api = createItem;
    }
    dispatch({
        api,
        types: [
            motorist.SAVE_MOTORIST_ITEM_START,
            motorist.SAVE_MOTORIST_ITEM_SUCCESS,
            motorist.SAVE_MOTORIST_ITEM_ERROR
        ],
        query: item
    })
        .then(() => {
            dispatch(push(`/motorist/list`));
            dispatch(listAction())
        });
};


export const selectStatuses = () => (dispatch) => {
    dispatch({
        api: getAllStatues,
        types: [
            motorist.LOAD_MOTORIST_STATUSES_START,
            motorist.LOAD_MOTORIST_STATUSES_SUCCESS,
            motorist.LOAD_MOTORIST_STATUSES_ERROR
        ],
        query: {}
    });
};

export const selectGenderTypes = () => (dispatch) => {
    dispatch({
        api: getAllGenderTypes,
        types: [
            motorist.LOAD_MOTORIST_GENDER_TYPES_START,
            motorist.LOAD_MOTORIST_GENDER_TYPES_SUCCESS,
            motorist.LOAD_MOTORIST_GENDER_TYPES_ERROR
        ],
        query: {}
    });
};
