import {createHashHistory} from "history";
import {createStore,  applyMiddleware} from "redux";
import { routerMiddleware} from "react-router-redux";
import thunkMiddleware from "redux-thunk";
import logger from "redux-logger";;
import apiMiddleware from "./middleware/apiMiddleware";
import { persistStore, persistReducer } from 'redux-persist'
import storage from 'redux-persist/lib/storage'
import rootReducer from "./reducers/rootReducers";
import SettingsTheme from "../widgets/SettingsTheme";

export const DEV = process.env.NODE_ENV === 'development';

const persistConfig = {
    key: 'root',
    storage,
    whitelist: ['auth']
}
const persistedReducer = persistReducer(persistConfig, rootReducer)



const history = createHashHistory();
const routeMiddleware = routerMiddleware(history);

const middleWares = [
    thunkMiddleware,
    routeMiddleware,
    apiMiddleware,
    DEV && logger,
].filter(Boolean);

export const configReduxStore = ()=>{
    const store = createStore(
        persistedReducer,
        applyMiddleware(
            ...middleWares
        )
    );
    let persistor = persistStore(store)

    return {store, persistor}
}
