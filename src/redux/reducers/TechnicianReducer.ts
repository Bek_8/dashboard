import {createReducer} from "../../utils/storeUtils";
import fp from "lodash/fp"
import * as TechnicianActionTypes from "../actionTypes/TechnicianActionTypes";
import {ListingObj} from "../../types/ApiOjbects";
import * as ApiUtils from "../../utils/ApiUtils";
import * as DateUtils from "../../utils/DateUtils";
import {convertAdditionalCostsForResponse, getProviderItemResponse, getTimeSlotList} from "../../utils/ProviderUtils";


const getCategoryServices = fp.get("payload.data.list")
const initTimeSlotsData = () => ({
    timeslots: getTimeSlotList(),
    additionalCosts:[{}]
})
const initialState = {
    list: [],
    count: 0,
    item: initTimeSlotsData(),
    serviceListByCategory: []
};

const reducers = {
    [TechnicianActionTypes.LOAD_TECHNICIAN_LIST_SUCCESS](state, action) {
        const {list, count}: ListingObj<any> = ApiUtils.getListingObjFromResponse(action);

        state.list = list;
        state.count = count;
    },

    [TechnicianActionTypes.LOAD_TECHNICIAN_LIST_ERROR](state) {
        state.list = [];
        state.count = 0;
    },

    [TechnicianActionTypes.LOAD_TECHNICIAN_ITEM_SUCCESS](state, action: any = {}) {
        const response = action?.payload?.data;
        if (action.payload && action.payload.data) {
            const details = DateUtils.convertTimeSlotsResponse(action.payload.data);
            state.item  = {    additionalCosts:[{}], ...convertAdditionalCostsForResponse(details)}
            if(response?.comissionId){
                state.item.commission = {id: response?.comissionId, name: response?.comissionName};
            }
        }
    },

    [TechnicianActionTypes.GET_SERVICES_BY_CATEGORY_START](state) {
        state.serviceListByCategory = [];
    },
    [TechnicianActionTypes.GET_SERVICES_BY_CATEGORY_SUCCESS](state, action: any = {}) {
        if (action.payload && action.payload.data) {
            state.serviceListByCategory = getCategoryServices(action) || [];
        }
    },

    [TechnicianActionTypes.LOAD_TECHNICIAN_ITEM_ERROR](state) {
        state.item = initTimeSlotsData();
    },

};

export default createReducer(initialState, reducers);
