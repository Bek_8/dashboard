import * as MessageActionType from '../actionTypes/MessageActionTypes';
import {createReducer} from '../../utils/storeUtils';

const initState = {
    deletionItem: null,
    snackbar: {},
    backdrop: false,
};

const reducers = {
    [MessageActionType.SET_DELETE_CONFIRMATION_ITEM](state, {item}) {
        state.deletionItem = item;
    },
    [MessageActionType.TOGGLE_SNACKBAR](state, {message, messageVariant}) {
        state.snackbar = {message, messageVariant, show: Boolean(message)};
    },
    [MessageActionType.TOGGLE_BACKDROP_LOADER](state, {visible}) {
        state.backdrop = visible
    },
};

export default createReducer(initState, reducers);
