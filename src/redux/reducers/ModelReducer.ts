import {createReducer} from "../../utils/storeUtils";

import * as modelActionTypes from "../actionTypes/ModelActionTypes";
import {ListingObj} from "../../types/ApiOjbects";
import * as ApiUtils from "../../utils/ApiUtils";

const initialState = {
    list: [],
    count: 0,
    item: {}
};

const reducers = {
    [modelActionTypes.LOAD_MODEL_LIST_SUCCESS](state, action) {
        const {list, count}: ListingObj<any> = ApiUtils.getListingObjFromResponse(action);

        state.list = list;
        state.count = count;
    },

    [modelActionTypes.LOAD_MODEL_LIST_ERROR](state) {
        state.list = [];
        state.count = 0;
    },

    [modelActionTypes.LOAD_MODEL_ITEM_SUCCESS](state, action:any = {}) {
        if (action.payload && action.payload.data) {
            state.item = action.payload.data;
        }
    },

    [modelActionTypes.LOAD_MODEL_ITEM_ERROR](state) {
        state.item = {};
    },

};

export default createReducer(initialState, reducers);
