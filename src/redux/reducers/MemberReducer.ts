import {createReducer} from "../../utils/storeUtils";

import * as memberActionTypes from "../actionTypes/MemberActionTypes";
import {ListingObj} from "../../types/ApiOjbects";
import * as ApiUtils from "../../utils/ApiUtils";

const initialState = {
    list: [],
    count: 0,
    item: {}
};

const reducers = {
    [memberActionTypes.LOAD_MEMBER_LIST_SUCCESS](state, action) {
        const {list, count}: ListingObj<any> = ApiUtils.getListingObjFromResponse(action);

        state.list = list;
        state.count = count;
    },

    [memberActionTypes.LOAD_MEMBER_LIST_ERROR](state) {
        state.list = [];
        state.count = 0;
    },

    [memberActionTypes.LOAD_MEMBER_ITEM_SUCCESS](state, action:any = {}) {
        if (action.payload && action.payload.data) {
            state.item = action.payload.data;
        }
    },

    [memberActionTypes.LOAD_MEASUREMENT_ITEM_ERROR](state) {
        state.item = {};
    },

};

export default createReducer(initialState, reducers);
