import {createReducer} from "../../utils/storeUtils";

import * as makeActionTypes from "../actionTypes/MakeActionTypes";
import {ListingObj} from "../../types/ApiOjbects";
import * as ApiUtils from "../../utils/ApiUtils";

const initialState = {
    list: [],
    count: 0,
    item: {}
};

const reducers = {
    [makeActionTypes.LOAD_MAKE_LIST_SUCCESS](state, action) {
        const {list, count}: ListingObj<any> = ApiUtils.getListingObjFromResponse(action);

        state.list = list;
        state.count = count;
    },

    [makeActionTypes.LOAD_MAKE_LIST_ERROR](state) {
        state.list = [];
        state.count = 0;
    },

    [makeActionTypes.LOAD_MAKE_ITEM_SUCCESS](state, action:any = {}) {
        if (action.payload && action.payload.data) {
            state.item = action.payload.data;
        }
    },

    [makeActionTypes.LOAD_MAKE_ITEM_ERROR](state) {
        state.item = {};
    },

};

export default createReducer(initialState, reducers);
