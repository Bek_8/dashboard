import {createReducer} from "../../utils/storeUtils";

import * as fuelTypeActionTypes from "../actionTypes/FuelTypeActionTypes";
import {ListingObj} from "../../types/ApiOjbects";
import * as ApiUtils from "../../utils/ApiUtils";

const initialState = {
    list: [],
    count: 0,
    item: {}
};

const reducers = {
    [fuelTypeActionTypes.LOAD_FUEL_TYPE_LIST_SUCCESS](state, action) {
        const {list, count}: ListingObj<any> = ApiUtils.getListingObjFromResponse(action);

        state.list = list;
        state.count =count;
    },

    [fuelTypeActionTypes.LOAD_FUEL_TYPE_LIST_ERROR](state) {
        state.list = [];
        state.count = 0;
    },

    [fuelTypeActionTypes.LOAD_FUEL_TYPE_ITEM_SUCCESS](state, action:any = {}) {
        if (action.payload && action.payload.data) {
            state.item = action.payload.data;
        }
    },

    [fuelTypeActionTypes.LOAD_FUEL_TYPE_ITEM_ERROR](state) {
        state.item = {};
    },

};

export default createReducer(initialState, reducers);
