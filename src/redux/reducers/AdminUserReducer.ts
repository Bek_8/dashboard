import {createReducer} from "../../utils/storeUtils";

import * as adminUserActionTypes from "../actionTypes/AdminUserActionTypes";
import {ListingObj} from "../../types/ApiOjbects";
import * as ApiUtils from "../../utils/ApiUtils";

const initialState = {
    list: [],
    count: 0,
    item: {}
};

const reducers = {
    [adminUserActionTypes.LOAD_ADMIN_USER_LIST_SUCCESS](state, action) {
        const {list, count}: ListingObj<any> = ApiUtils.getListingObjFromResponse(action);

        state.list = list;
        state.count = count;
    },

    [adminUserActionTypes.LOAD_ADMIN_USER_LIST_ERROR](state) {
        state.list = [];
        state.count = 0;
    },

    [adminUserActionTypes.LOAD_ADMIN_USER_ITEM_SUCCESS](state, action:any = {}) {
        if (action.payload && action.payload.data) {
            state.item = action.payload.data;
        }
    },

    [adminUserActionTypes.LOAD_ADMIN_USER_ITEM_ERROR](state) {
        state.item = {};
    },

};

export default createReducer(initialState, reducers);
