import {combineReducers} from "redux";
import {routerReducer} from "react-router-redux";
import adminUser from "./AdminUserReducer";
import garageUser from "./GarageUserReducer";
import motorist from "./MotoristReducer";
import auth from "./AuthReducer";
import loader from "./LoaderReducer";
import message from "./MessageReducer";
import reference from "./ReferenceReducer";
import autoService from "./AutoServiceReducer";
import oilStation from "./OilStationReducer";
import makeModels from "./MakeModelsReducer";
import provider from "./ProviderReducer";
import deposit from "./DepositReducer";
import technician from "./TechnicianReducer";
import fuelType from "./FuelTypeReducer";
import make from "./MakeReducer";
import model from "./ModelReducer";
import modification from "./ModificationReducer";
import service from "./ServiceReducer";
import vehicle from "./VehicleReducer";
import vehicleType from "./VehicleTypeReducer";
import commission from "./CommissionReducer";
import theme from "../reducers/ChangeThemeColorReducer";

import {reducer as formReducer} from "redux-form";


export default combineReducers({
    form: formReducer,
    router: routerReducer,
    adminUser,
    garageUser,
    motorist,
    auth,
    loader,
    message,

    reference,
    autoService,
    oilStation,
    makeModels,

    provider,
    deposit,
    technician,

    fuelType,
    make,
    model,
    modification,
    service,
    vehicle,
    vehicleType,
    theme,
    commission
})
