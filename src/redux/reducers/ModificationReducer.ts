import {createReducer} from "../../utils/storeUtils";

import * as modificationActionTypes from "../actionTypes/ModificationActionTypes";
import {ListingObj} from "../../types/ApiOjbects";
import * as ApiUtils from "../../utils/ApiUtils";

const initialState = {
    list: [],
    count: 0,
    item: {}
};

const reducers = {
    [modificationActionTypes.LOAD_MODIFICATION_LIST_SUCCESS](state, action) {
        const {list, count}: ListingObj<any> = ApiUtils.getListingObjFromResponse(action);

        state.list = list;
        state.count = count;
    },

    [modificationActionTypes.LOAD_MODIFICATION_LIST_ERROR](state) {
        state.list = [];
        state.count = 0;
    },

    [modificationActionTypes.LOAD_MODIFICATION_ITEM_SUCCESS](state, action: any = {}) {
        if (action.payload && action.payload.data) {
            state.item = action.payload.data;
        }
    },

    [modificationActionTypes.LOAD_MODIFICATION_ITEM_ERROR](state) {
        state.item = {};
    },
};

export default createReducer(initialState, reducers);
