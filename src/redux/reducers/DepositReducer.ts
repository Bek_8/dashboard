import {createReducer} from "../../utils/storeUtils";

import * as DepositActionTypes from "../actionTypes/DepositActionTypes";
import {ListingObj} from "../../types/ApiOjbects";
import * as ApiUtils from "../../utils/ApiUtils";

const initialState = {
    list: [],
    count: 0,
    item: null
};

const reducers = {
    [DepositActionTypes.LOAD_DEPOSIT_LIST_SUCCESS](state, action) {
        const {list, count}: ListingObj<any> = ApiUtils.getListingObjFromResponse(action);

        state.list = list;
        state.count = count;
    },

    [DepositActionTypes.LOAD_DEPOSIT_LIST_ERROR](state) {
        state.list = [];
        state.count = 0;
    },

    [DepositActionTypes.LOAD_DEPOSIT_ITEM_START](state) {
        state.item = null;
    },

    [DepositActionTypes.LOAD_DEPOSIT_ITEM_SUCCESS](state, action: any = {}) {
        if (action.payload && action.payload.data) {
            state.item = action.payload.data
        }
    },

    [DepositActionTypes.LOAD_DEPOSIT_ITEM_ERROR](state) {
        state.item = null;
    },

    [DepositActionTypes.LOAD_DEPOSIT_ITEM_BY_PROVIDER_ID_START](state) {
        state.depositInfo = null;
    },

    [DepositActionTypes.LOAD_DEPOSIT_ITEM_BY_PROVIDER_ID_SUCCESS](state, action: any = {}) {
        if (action.payload && action.payload.data) {
            // action.payload.data.timeslots
            state.depositInfo = action.payload.data;
        }
    },

    [DepositActionTypes.LOAD_DEPOSIT_ITEM_BY_PROVIDER_ID_ERROR](state) {
    },

};

export default createReducer(initialState, reducers);
