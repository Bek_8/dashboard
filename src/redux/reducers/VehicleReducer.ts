import {createReducer} from "../../utils/storeUtils";

import * as vehicleActionTypes from "../actionTypes/VehicleActionTypes";
import {ListingObj} from "../../types/ApiOjbects";
import * as ApiUtils from "../../utils/ApiUtils";

const initialState = {
    list: [],
    count: 0,
    item: {}
};

const reducers = {
    [vehicleActionTypes.LOAD_VEHICLE_LIST_SUCCESS](state, action) {
        const {list, count}: ListingObj<any> = ApiUtils.getListingObjFromResponse(action);

        state.list = list;
        state.count = count;
    },

    [vehicleActionTypes.LOAD_VEHICLE_LIST_ERROR](state) {
        state.list = [];
        state.count = 0;
    },

    [vehicleActionTypes.LOAD_VEHICLE_ITEM_SUCCESS](state, action:any = {}) {
        if (action.payload && action.payload.data) {
            state.item = action.payload.data;
        }
    },

    [vehicleActionTypes.LOAD_VEHICLE_ITEM_ERROR](state) {
        state.item = {};
    },

};

export default createReducer(initialState, reducers);
