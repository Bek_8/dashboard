import {createReducer} from "../../utils/storeUtils";

import * as changeThemeColor from "../actionTypes/ChangeThemeColorActionTypes";


const intialState = {
    color: 'blue'
}

const  reducers = {
    [changeThemeColor.CHANGE_BACKGROUND_COLOR](state, action) {
        state.color = action.color;
    },
};



export default createReducer(intialState, reducers);