import {createReducer} from "../../utils/storeUtils";

import * as serviceActionTypes from "../actionTypes/ServiceActionTypes";
import {ListingObj} from "../../types/ApiOjbects";
import * as ApiUtils from "../../utils/ApiUtils";
import {convertServiceResponse} from "../../utils/ProviderUtils";

const initialState = {
    list: [],
    count: 0,
    item: {},
    subList: [],
    subItem: {},
    subCount: 0
};

const reducers = {
    [serviceActionTypes.LOAD_SERVICE_LIST_SUCCESS](state, action) {
        const {list, count}: ListingObj<any> = ApiUtils.getListingObjFromResponse(action);

        state.list = list;
        state.count = count;
    },
    [serviceActionTypes.LOAD_SERVICE_LIST_ERROR](state) {
        state.list = [];
        state.count = 0;
    },
    [serviceActionTypes.LOAD_SERVICE_ITEM_SUCCESS](state, action:any = {}) {
        if (action.payload && action.payload.data) {
            state.item = convertServiceResponse(action.payload.data);
        }
    },
    [serviceActionTypes.LOAD_SERVICE_ITEM_ERROR](state) {
        state.item = {};
    },

    [serviceActionTypes.LOAD_SERVICE_SUB_LIST_SUCCESS](state, action) {
        // const {list, count}: ListingObj<any> = ApiUtils.getListingObjFromResponse(action);

        state.subList = action.payload.data || [];
        state.subCount = state.subList.length;
    },
    [serviceActionTypes.LOAD_SERVICE_SUB_LIST_ERROR](state) {
        state.subList = [];
        state.subCount = 0;
    },
    [serviceActionTypes.LOAD_SERVICE_SUB_ITEM_SUCCESS](state, action:any = {}) {
        if (action.payload && action.payload.data) {
            state.subItem = convertServiceResponse(action.payload.data);
        }
    },
    [serviceActionTypes.LOAD_SERVICE_SUB_ITEM_ERROR](state) {
        state.subItem = {};
    },
};

export default createReducer(initialState, reducers);
