import {createReducer} from "../../utils/storeUtils";

import * as ProviderActionTypes from "../actionTypes/ProviderActionTypes";
import {ListingObj} from "../../types/ApiOjbects";
import * as ApiUtils from "../../utils/ApiUtils";
import {getProviderItemResponse, getTimeSlotList} from "../../utils/ProviderUtils";

const initProviderData = () => ({
    timeslots: getTimeSlotList()
})
const initialState = {
    list: [],
    count: 0,
    loader: 0,
    item: initProviderData(),
};

const reducers = {
    [ProviderActionTypes.LOAD_PROVIDER_LIST_START](state) {
        state.list = [];
        state.count = 0;
        state.loader = true;
    },
    [ProviderActionTypes.LOAD_PROVIDER_LIST_SUCCESS](state, action) {
        const {list, count}: ListingObj<any> = ApiUtils.getListingObjFromResponse(action);

        state.list = list;
        state.count = count;
        state.loader = false;
        state.item = initProviderData();
    },

    [ProviderActionTypes.LOAD_PROVIDER_LIST_ERROR](state) {
        state.list = [];
        state.count = 0;
        state.loader = false;
    },

    [ProviderActionTypes.LOAD_PROVIDER_ITEM_START](state) {
        state.item = null;
    },

    [ProviderActionTypes.LOAD_PROVIDER_ITEM_SUCCESS](state, action: any = {}) {
        if (action.payload && action.payload.data) {
            // action.payload.data.timeslots
            state.item = getProviderItemResponse(action.payload.data);
        }
    },

    [ProviderActionTypes.LOAD_PROVIDER_ITEM_ERROR](state) {
        state.item = initProviderData();
    },

};

export default createReducer(initialState, reducers);
