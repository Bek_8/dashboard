import {createReducer} from "../../utils/storeUtils";

import * as ActionTypes from "../actionTypes/AutoServiceActionTypes";
import {ListingObj} from "../../types/ApiOjbects";
import * as ApiUtils from "../../utils/ApiUtils";

const initialState = {
    list: [],
    count: 0,
    item: {}
};

const reducers = {
    [ActionTypes.LOAD_AUTO_SERVICE_LIST_START](state) {
        state.list = [];
        state.count = 0;
        state.loader = true;
    },
    [ActionTypes.LOAD_AUTO_SERVICE_LIST_SUCCESS](state, action) {
        const {list, count}: ListingObj<any> = ApiUtils.getListingObjFromResponse(action);

        state.list = list;
        state.count = count;
        state.loader = false;
    },

    [ActionTypes.LOAD_AUTO_SERVICE_LIST_ERROR](state) {
        state.list = [];
        state.count = 0;
        state.loader = false;
    },

    [ActionTypes.LOAD_AUTO_SERVICE_ITEM_SUCCESS](state, action:any = {}) {
        if (action.payload && action.payload.data) {
            state.item = action.payload.data;
        }
    },

    [ActionTypes.LOAD_AUTO_SERVICE_ITEM_ERROR](state) {
        state.item = {};
    },

};

export default createReducer(initialState, reducers);
