import {createReducer} from "../../utils/storeUtils";
import * as ApiUtils from "../../utils/ApiUtils";

import * as garageUserActionTypes from "../actionTypes/GarageUserActionTypes";
import {ListingObj} from "../../types/ApiOjbects";

const initialState = {
    list: [],
    count: 1,
    item: {}
};

const reducers = {
    [garageUserActionTypes.LOAD_GARAGE_USER_LIST_SUCCESS](state, action) {
        const {list, count}: ListingObj<any> = ApiUtils.getListingObjFromResponse(action);

        state.list = list;
        state.count = count;
    },

    [garageUserActionTypes.LOAD_GARAGE_USER_LIST_ERROR](state) {
        state.list = [];
        state.count = 0;
    },

    [garageUserActionTypes.LOAD_GARAGE_USER_ITEM_SUCCESS](state, action: any = {}) {
        if (action.payload && action.payload.data) {
            state.item = action.payload.data;
        }
    },

    [garageUserActionTypes.LOAD_GARAGE_USER_ITEM_ERROR](state) {
        state.item = {};
    },

};

export default createReducer(initialState, reducers);
