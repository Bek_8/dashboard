import {createReducer} from "../../utils/storeUtils";

import * as CommissionActionTypes from "../actionTypes/CommissionActionTypes";
import {ListingObj} from "../../types/ApiOjbects";
import * as ApiUtils     from "../../utils/ApiUtils";

const initialState = {
    list: [],
    count: 0,
    loader: false,
    item: null
};

const reducers = {
    [CommissionActionTypes.LOAD_COMMISSION_LIST_START](state, action) {
        state.list = [];
        state.count = 0;
        state.loader = true;
    },
    [CommissionActionTypes.LOAD_COMMISSION_LIST_SUCCESS](state, action) {
        const {list, count}: ListingObj<any> = ApiUtils.getListingObjFromResponse(action);

        state.list = list;
        state.count = count;
        state.loader = false;
    },

    [CommissionActionTypes.LOAD_COMMISSION_LIST_ERROR](state) {
        state.list = [];
        state.count = 0;
        state.loader = false;
    },

    [CommissionActionTypes.LOAD_COMMISSION_ITEM_START](state) {
        state.item = null;
    },

    [CommissionActionTypes.LOAD_COMMISSION_ITEM_SUCCESS](state, action: any = {}) {
        if (action.payload && action.payload.data) {
            state.item = action.payload.data
        }
    },

    [CommissionActionTypes.LOAD_COMMISSION_ITEM_ERROR](state) {
        state.item = null;
    },

};

export default createReducer(initialState, reducers);
