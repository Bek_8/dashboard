import {createReducer} from "../../utils/storeUtils";

import * as vehicleTypeActionTypes from "../actionTypes/VehicleTypeActionTypes";
import {ListingObj} from "../../types/ApiOjbects";
import * as ApiUtils from "../../utils/ApiUtils";

const initialState = {
    list: [],
    count: 0,
    item: {}
};

const reducers = {
    [vehicleTypeActionTypes.LOAD_VEHICLE_TYPE_LIST_SUCCESS](state, action) {
        const {list, count}: ListingObj<any> = ApiUtils.getListingObjFromResponse(action);

        state.list = list;
        state.count = count;
    },

    [vehicleTypeActionTypes.LOAD_VEHICLE_TYPE_LIST_ERROR](state) {
        state.list = [];
        state.count = 0;
    },

    [vehicleTypeActionTypes.LOAD_VEHICLE_TYPE_ITEM_SUCCESS](state, action: any = {}) {
        if (action.payload && action.payload.data) {
            state.item = action.payload.data;
        }
    },

    [vehicleTypeActionTypes.LOAD_VEHICLE_TYPE_ITEM_ERROR](state) {
        state.item = {};
    },

};

export default createReducer(initialState, reducers);
