import * as ActionType from '../actionTypes/ReferenceActionTypes';
import {createReducer} from '../../utils/storeUtils';
import {ListingObj} from "../../types/ApiOjbects";
import * as ApiUtils from "../../utils/ApiUtils";

const initState = {
    genders: [],
    statuses: [],
    userRoles: [],
    categories: [],
    regions: [],
    cities: [],
    weekends: [],
    list: [],
    item: null,
    count: 0,
    loader: false,
};

const reducers = {
    [ActionType.LOAD_REFERENCE_GENDERS_SUCCESS](state, {payload}) {
        state.genders = payload.data;
    },
    [ActionType.LOAD_REFERENCE_STATUS_LIST_SUCCESS](state, {payload}) {
        state.statuses = payload.data;
    },
    [ActionType.LOAD_REFERENCE_CATEGORY_TYPE_SUCCESS](state, {payload}) {
        state.categories = payload.data;
    },
    [ActionType.LOAD_REFERENCE_USER_ROLES_SUCCESS](state, {payload}) {
        state.userRoles = payload.data;
    },
    [ActionType.LOAD_REFERENCE_REGIONS_SUCCESS](state, {payload}) {
        state.regions = payload.data;
    },
    [ActionType.LOAD_REFERENCE_DISTRICTS_SUCCESS](state, {payload}) {
        state.cities = payload.data;
    },
    [ActionType.LOAD_REFERENCE_WEEKDAYS_SUCCESS](state, {payload}) {
        state.weekends = payload.data;
    },
    [ActionType.LOAD_REFERENCE_LISTING_START](state, action) {
        state.loader = true;
        state.list = [];
        state.count = 0;
    },
    [ActionType.LOAD_REFERENCE_LISTING_SUCCESS](state, action) {
        const {list, count}: ListingObj<any> = ApiUtils.getListingObjFromResponse(action);

        state.list = list;
        state.count = count;
        state.loader = false;
    },

    [ActionType.LOAD_REFERENCE_LISTING_ERROR](state) {
        state.list = [];
        state.count = 0;
        state.loader = false;
    },

    [ActionType.LOAD_REFERENCE_ITEM_SUCCESS](state, action: any = {}) {
        if (action.payload && action.payload.data) {
            state.item = action.payload.data;
        }
    },

    [ActionType.LOAD_REFERENCE_ITEM_ERROR](state) {
        state.item = {};
    },
};

export default createReducer(initState, reducers);
