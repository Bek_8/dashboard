import * as authActionTypes from "../actionTypes/AuthActionTypes";
import {getInitialState, createReducer} from "../../utils/storeUtils";

const initState = {
    token: null,
    tokenType: null,
    user: null
};

const reducers = {
    [authActionTypes.AUTH_GET_USER_TOKEN_SUCCESS](state, action) {
        state.token = action.payload.data.accessToken;
        state.tokenType = action.payload.data.tokenType;
        // state.user = action.payload;
    },
    [authActionTypes.AUTH_LOGOUT](state) {
        state.token = null;
        state.tokenType = null;
        state.user = null;
        localStorage.removeItem("token");
        localStorage.removeItem("tokenType");

    },
    [authActionTypes.AUTH_RESTORE](state) {
        const userToken = localStorage.getItem("token");
        const tokenType = localStorage.getItem("tokenType");

        if (userToken && tokenType) {
            state.token = userToken;
            state.tokenType = tokenType;
        }
    },
    [authActionTypes.GET_USER_INFO_SUCCESS](state, action) {
        state.user= action.payload
    },
};

export default createReducer(getInitialState("auth", initState), reducers);
