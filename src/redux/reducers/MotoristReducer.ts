import {createReducer} from "../../utils/storeUtils";

import * as motoristActionTypes from "../actionTypes/MotoristActionTypes";
import {ListingObj} from "../../types/ApiOjbects";
import * as ApiUtils from "../../utils/ApiUtils";

const initialState = {
    list: [],
    count: 0,
    item: {},
    statuses: [],
    genderTypes: []
};

const reducers = {
    [motoristActionTypes.LOAD_MOTORIST_LIST_SUCCESS](state, action) {
        const {list, count}: ListingObj<any> = ApiUtils.getListingObjFromResponse(action);

        state.list = list;
        state.count = count;
    },

    [motoristActionTypes.LOAD_MOTORIST_LIST_ERROR](state) {
        state.list = [];
        state.count = 0;
    },

    [motoristActionTypes.LOAD_MOTORIST_ITEM_SUCCESS](state, action: any = {}) {
        if (action.payload && action.payload.data) {
            state.item = action.payload.data;
        }
    },

    [motoristActionTypes.LOAD_MOTORIST_ITEM_ERROR](state) {
        state.item = {};
    },

    [motoristActionTypes.LOAD_MOTORIST_GENDER_TYPES_SUCCESS](state, action) {
        state.genderTypes = action.payload.data || []
    },
    [motoristActionTypes.LOAD_MOTORIST_STATUSES_SUCCESS](state, action) {
        state.statuses = action.payload.data || []
    },

};

export default createReducer(initialState, reducers);
