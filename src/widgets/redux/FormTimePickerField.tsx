import React from "react";
import fp from "lodash/fp"
import {Field} from "redux-form";
import {KeyboardTimePicker, MuiPickersUtilsProvider} from "@material-ui/pickers";
import DateFnsUtils from "@date-io/date-fns";

// FormDatePickerComponent.propTypes = {
//     meta: PropTypes.object,
//     input: PropTypes.object,
// };

function FormTimePickerComponent({input, meta, ...custom}) {

    function handleDateChange(nextValue) {
        return input.onChange(nextValue);
    }

    const value = input.value === '' ? null : input.value;

    return (
        <MuiPickersUtilsProvider utils={DateFnsUtils}>
            <KeyboardTimePicker
                {...input}
                value={value}
                onBlur={fp.noop}
                onFocus={fp.noop}
                ampm={false}
                margin={custom.margin}
                label={custom.label}
                format={custom.dateFormat}
                error={meta.touched && meta.invalid}
                helperText={meta.touched && meta.error}
                style={{width: '100%'}}
                onChange={(value:any) => handleDateChange(value)}
                KeyboardButtonProps={{
                    'aria-label': 'change date',
                }}
            />
        </MuiPickersUtilsProvider>
    )
}

// FormDatePickerField.propTypes = {
//     name: PropTypes.string.isRequired,
//     label: PropTypes.string,
//     dateFormat: PropTypes.string,
//     margin: PropTypes.string
// };

export default function FormTimePickerField(props:any) {
    return <Field {...props} component={FormTimePickerComponent}/>
};