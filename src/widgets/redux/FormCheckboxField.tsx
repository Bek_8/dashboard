import React from "react";
import {Field} from "redux-form";
import TextField from "@material-ui/core/TextField";
import {Checkbox, FormControlLabel} from "@material-ui/core";

function FormCheckboxComponent({input, label, meta, InputProps, ...rest}: any) {
    console.log("FormCheckboxComponent === ", input, label, meta, InputProps, rest)
    return (
        <FormControlLabel
            control={
                <Checkbox
                    {...input}
                    checked={input.value||false}
                    {...InputProps}
                    error={meta.touched && meta.invalid}
                    helperText={meta.touched && meta.error}
                />
            }
            label={label}
        />

    )
}
interface IProps {
    label:string;
    name:string;
    fullWidth?: boolean;
    onChange?: (e:any, checked:boolean)=>void;
    InputProps?: any;
}

export default function FormCheckboxField(props: IProps) {
    return (<Field {...props} component={FormCheckboxComponent}/>);
}