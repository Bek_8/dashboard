import React from "react";
import {Field} from "redux-form";
import fp from "lodash/fp";
import {CircularProgress, FormControl, TextField} from "@material-ui/core";
import {Autocomplete} from "@material-ui/lab";

const NOT_SET_OPTION = {};

interface IComponentProps {
    input?: any,
    meta?: any,
    loading?: boolean,
    fullWidth?: boolean,
    disabled?: boolean,
    variant?: 'standard' | 'outlined' | 'filled',
    margin?: 'none' | 'dense' | 'normal',
    label?: string,
    validate?: Function,
    formatInput?: Function,
    formatOption?: (option: any) => any,
    formatValue?: (option: any) => any,
    compareOptions?: (option: any, value: any) => boolean,
    onTextChange?: (textValue: string) => void,
    onSelected?: (v: any)=>void;
    options: any[],
}

AutoCompleteComponent.defaultProps = {
    formatOption: fp.identity,
    compareOptions: fp.isEqualWith,
    variant: "outlined",
    margin: "normal"
};

export function AutoCompleteComponent(
    {
        input, meta,
        options = [],
        label,
        disabled,
        fullWidth=true,
        margin,
        formatInput,
        formatOption,
        compareOptions,
        formatValue,
        loading,
        onTextChange,
        onSelected,
        ...custom
    }: IComponentProps) {
    const parsedChangeHandler = (e: any, newValue: any) => {
        // console.log('autocomplte onChange =', newValue, e);
        input.onChange(newValue);
        if(onSelected )onSelected(newValue);
    }
    const onTestChangeHandler = (e: any, value: any, reason: 'input' | 'reset')=>{
        // console.log('> TextChange =====', input, value, reason);
        if( reason == 'input' && input.value != value) {
            onTextChange(value)

        }
    }
    // const blurHandler = (e, v)=>{
    //     console.log('blurHandler --', e,e.target,e.target?.value);
    //     onBlur(e)
    // }
    console.log("---AutoComplete  -", input, custom, options)
    const {onBlur, onFocus,onChange, ...restInput} = input
    return (
        <FormControl
            style={{width: "100%"}}
            variant={custom.variant}
            margin={margin}>
            <Autocomplete
                value={restInput.value}
                id="combo-box-demo"
                size="medium"
                options={options}
                disabled={disabled}
                getOptionSelected={compareOptions}
                getOptionLabel={formatOption}
                onInputChange={onTestChangeHandler}
                onChange={parsedChangeHandler}
fullWidth={true}
                renderInput={params => (<>
                        <TextField
                            {...params}
                            autoComplete='oneto'
                            fullWidth={fullWidth}
                            error={meta.touched && meta.invalid}
                            helperText={meta.touched && meta.error}
                            variant="outlined"
                            placeholder={label}
                            InputProps={{
                                ...params.InputProps,
                                autoComplete: "sdfds",
                                endAdornment: (
                                    <React.Fragment>
                                        {loading ? <CircularProgress color="inherit" size={20}/> : null}
                                        {params.InputProps.endAdornment}
                                    </React.Fragment>
                                ),
                            }}
                        />
                    </>

                )}
            />
        </FormControl>
    )
}

interface IProps {
    name: string,
    variant?: 'standard' | 'outlined' | 'filled',
    margin?: 'none' | 'dense' | 'normal',
    required?: boolean,
    InputProps?: any,
    fullWidth?: boolean,
    label?: React.ReactNode | string,
    validate?: Function,
    formatInput?: Function,
    formatOption?: Function,
    formatValue?: Function,
    compareOptions?: Function,
    options: any[]
}

export default function FormAutoCompleteField(props: IProps) {
    return <Field {...props} component={AutoCompleteComponent}/>
}
