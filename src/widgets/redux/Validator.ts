import moment from "moment";

export const isValidDate = (value:any) => {
    return value && moment(value).isValid() ?
        undefined :
        "Неверный формат";
};

export const isEmpty = (value:any) => !value ? "Заполните обязательное поле" : undefined;

export const requiredSelect = (value:any) => {
    if (!value || value === '' || value == null) {
        return 'Заполните обязательное поле'
    }
    return undefined;
};
