import React from "react";
import * as PropTypes from "prop-types";
import fp from "lodash/fp"
import {Field} from "redux-form";
import {KeyboardDatePicker, MuiPickersUtilsProvider} from "@material-ui/pickers";
import DateFnsUtils from "@date-io/date-fns";

// FormDatePickerComponent.propTypes = {
//     meta: PropTypes.object,
//     input: PropTypes.object,
// };

function FormDatePickerComponent({input, meta, ...custom}) {

    function handleDateChange(nextValue) {
        return input.onChange(nextValue);
    }

    const value = input.value === '' ? null : input.value;

    return (
        <MuiPickersUtilsProvider utils={DateFnsUtils}>
            <KeyboardDatePicker
                {...input}
                value={value}
                onBlur={fp.noop}
                onFocus={fp.noop}
                margin={custom.margin}
                label={custom.label}
                format={custom.dateFormat}
                error={meta.touched && meta.invalid}
                helperText={meta.touched && meta.error}
                style={{width: '100%'}}
                onChange={(value:any) => handleDateChange(value)}
                KeyboardButtonProps={{
                    'aria-label': 'change date',
                }}
            />
        </MuiPickersUtilsProvider>
    )
}

// FormDatePickerField.propTypes = {
//     name: PropTypes.string.isRequired,
//     label: PropTypes.string,
//     dateFormat: PropTypes.string,
//     margin: PropTypes.string
// };
FormDatePickerField.defaultProps = {
    dateFormat: "dd/MM/yyyy",
    margin: "normal",
};

export default function FormDatePickerField(props:any) {
    return <Field {...props} component={FormDatePickerComponent}/>
};