import React from "react"
import { Form, Field, reduxForm } from "redux-form";
import IconButton from "@material-ui/core/IconButton";
import {PhotoCamera} from "@material-ui/icons";
// import Grid from "@material-ui/core/Grid";

interface IProps {
    imagePath?: string,
    onChange: (e:any)=>void
}
export  function ImageInput({imagePath, onChange}:IProps) {
    // const getBase64 = (file) => {
    //     return new Promise((resolve, reject) => {
    //         const reader = new FileReader();
    //         reader.readAsDataURL(file);
    //         reader.onload = () => resolve(reader.result);
    //         reader.onerror = error => reject(error);
    //     });
    // }
    //
    // const onFileChange = async (e) => {
    //     const {input} = props
    //     const targetFile = e.target.files[0]
    //     if (targetFile) {
    //         const val = await getBase64(targetFile)
    //         input.onChange(val)
    //     } else {
    //         input.onChange(null)
    //     }
    // }
    const id = Date.now()
    return (
        <>
            <input accept="image/*"
                   onChange={onChange}
                   style={{display:'none'}} id={`file-id-${id}`} type="file" name="file" />
            <label htmlFor={`file-id-${id}`}>
                <IconButton color="primary"  component="span">
                    <PhotoCamera />
                </IconButton>
                {imagePath &&<img src={imagePath} style={{maxWidth:200, maxHeight: 200 }}/>}
            </label>
        </>
    )
}
export default function FileInputField (props){
    return (
        <Field {...props} component={ImageInput}  type="file" />
    )
}
