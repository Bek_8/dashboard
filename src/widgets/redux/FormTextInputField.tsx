import React from "react";
import {Field} from "redux-form";
// import * as PropTypes from "prop-types";
import TextField from "@material-ui/core/TextField";

// FormTextInputComponent.propTypes = {
//     meta: PropTypes.object,
//     input: PropTypes.object,
//     InputProps: PropTypes.object,
//     multiline: PropTypes.bool,
//     rowsMax: PropTypes.string,
//     rows: PropTypes.string,
//     variant: PropTypes.string
// };
function FormTextInputComponent({input, meta, InputProps, ...custom}: any) {

    return (
        <TextField
            {...input}
            {...InputProps}
            margin={custom.margin}
            rows={custom.rows}
            rowsMax={custom.rowsMax}
            multiline={custom.multiline}
            placeholder={custom.placeholder}
            error={meta.touched && meta.invalid}
            helperText={meta.touched && meta.error}
        />
    )
}

// FormTextInputField.propTypes = {
//     name: PropTypes.string.isRequired,
//     label: PropTypes.string,
//     margin: PropTypes.string
// };
//
FormTextInputField.defaultProps = {
    margin: "normal",
};

export default function FormTextInputField(props: any) {
    return (<Field {...props} component={FormTextInputComponent}/>);
}