import React from "react";
import * as PropTypes from "prop-types";
import cx from "classnames";
import Toolbar from "@material-ui/core/Toolbar";
import makeStyles from "@material-ui/core/styles/makeStyles";
import Button from "@material-ui/core/Button";
import TextField from "@material-ui/core/TextField";
import FormControl from "@material-ui/core/FormControl";
import InputAdornment from "@material-ui/core/InputAdornment";
import IconButton from "@material-ui/core/IconButton";
import SearchIcon from "@material-ui/icons/Search";
import FlashOnIcon from '@material-ui/icons/FlashOn';
import Tooltip from "@material-ui/core/Tooltip";
import Fab from "@material-ui/core/Fab";
import AddToPhotosIcon from '@material-ui/icons/Add';
import AutorenewIcon from '@material-ui/icons/Autorenew';
import SpeedDial from '@material-ui/lab/SpeedDial';
import SpeedDialIcon from '@material-ui/lab/SpeedDialIcon';
import SpeedDialAction from '@material-ui/lab/SpeedDialAction';
import FileCopyIcon from '@material-ui/icons/FileCopyOutlined';
import SaveIcon from '@material-ui/icons/Save';
import PrintIcon from '@material-ui/icons/Print';
import ShareIcon from '@material-ui/icons/Share';
import FavoriteIcon from '@material-ui/icons/Favorite';

const useStyles = makeStyles(theme => ({
    toolbar: {
        paddingLeft: "0",
        paddingRight: "0"
    },
    paper: {
        padding: theme.spacing(2),
        textAlign: 'center',
        color: theme.palette.text.secondary,
    },
    textField: {
        paddingTop: "0",
        paddingBottom: "0",
        marginTop: 0,
        marginBottom: 0,
    },
    formControl: {
        marginRight: theme.spacing(1)
    },
    button: {
        padding: "7px 15px",
    },
    addButton: {
        padding: "7px 15px",

        "&:hover svg": {
            transform: 'scale(1.1)',
            transition: '0.3s',
        }
    },
    flashIcon: {
        "&:hover svg": {
            transform: 'scale(1.2)',
            transition: '0.3s',
        }
    },
    extendedIcon: {
        marginRight: theme.spacing(1)
    },
    addIcon: {
        weight: '600',
        marginRight: "10px",

    },
    autoRunIcon: {
        fontSize: '20pt',
        "&:hover svg": {
            transform: 'rotate(360deg)',
            transition: '1s',
        }
    },
    exampleWrapper: {
        maxHeight: 56,

        position: 'fixed',
        right: '40px',
        bottom: '40px',
        marginTop: theme.spacing(3),
        height: 380,
        "&:hover svg": {
            transform: 'scale(1.2)',
            transition: '0.3s',
        }

    },
    addFab: {
        color: '#FFFFFF',
    },


}));


interface IProps {
    resetListToDefault: (e: any) => void,
    updateList: (e: any) => void,
    searchList: (e: any) => void,
    addNewHandler?: React.ReactElement,
    filterKey?: string,
    searchChange?: (e: any) => void,
    changeSearchChange?: (e: any) => void,
}

const actions = [
    {icon: <FileCopyIcon/>, name: 'Copy'},
    {icon: <SaveIcon/>, name: 'Save'},
    {icon: <PrintIcon/>, name: 'Print'},
    {icon: <FavoriteIcon/>, name: 'Like'},
];
export default function ListPanelHeaderToolbar(props: IProps) {
    const classes = useStyles(props);

    return (
        <Toolbar className={classes.toolbar}>
            {props.addNewHandler &&
            <FormControl className={classes.formControl}>
                <Tooltip title="Добавить" aria-label="Добавить" placement="top"
                         className={cx(classes.button, classes.addButton)}>
                    {props.addNewHandler}
                </Tooltip>

            </FormControl>}
            <FormControl className={classes.formControl}>
                <Tooltip title="Поиск" aria-label="Поиск" placement="top">
                    <TextField
                        id="standard-textarea"
                        margin="dense"
                        label="Поиск"
                        className={classes.textField}
                        variant="outlined"
                        value={props.filterKey}
                        onKeyUp={props.searchChange}
                        onChange={props.changeSearchChange}
                        InputProps={{
                            endAdornment: (
                                <InputAdornment position="end">
                                    <IconButton
                                        onClick={() => props.searchList(props.filterKey)}
                                        aria-label="toggle password visibility">
                                        <SearchIcon/>
                                    </IconButton>
                                </InputAdornment>
                            )
                        }}/>
                </Tooltip>
            </FormControl>
            <FormControl className={classes.formControl}>
                <Tooltip title="Обновить" aria-label="Обновить" placement="top">
                    <Button
                        variant="outlined"

                        color="primary"
                        onClick={props.updateList}
                        className={cx(classes.button, classes.autoRunIcon)}
                    >
                        <AutorenewIcon/>
                    </Button>
                </Tooltip>
            </FormControl>
            <FormControl className={classes.formControl}>
                <Tooltip title="Сбросить фильтр" aria-label="Сбросить фильтр" placement="top">
                    <Button
                        variant="outlined"
                        className={cx(classes.button, classes.flashIcon)}
                        color="secondary"
                        onClick={props.resetListToDefault}>
                        <FlashOnIcon/>
                    </Button>
                </Tooltip>
            </FormControl>
        </Toolbar>

    )
}