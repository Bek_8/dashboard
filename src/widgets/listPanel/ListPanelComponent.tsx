import React, {Component} from "react";
import Immutable, {List} from "immutable";
import _ from "lodash";
import ListHeaderComponent from "./ListHeaderComponent";
import ListFooterComponent from "./ListFooterComponent";
import Table from "@material-ui/core/Table";
import TableBody from "@material-ui/core/TableBody";
import TableRow from "@material-ui/core/TableRow";
import TableCell from "@material-ui/core/TableCell";
import ListPanelHeaderToolbar from "./ListPanelHeaderToolbar";
import CircularProgress from "@material-ui/core/CircularProgress";
import Loader from "../../components/common-core/Loader";
import { useSelector } from "react-redux";

export default class ListPanelComponent extends Component<any, any> {
    constructor(props: any) {
        super(props);
        this.state = {
            list: List(),
            count: 0,
            limit: 10,
            activePage: 0,
            filterKey: "",
            sortField: null,
            order: null,
            key: null,
        }
    }

    componentWillReceiveProps(nextProps) {
        const {list, count} = nextProps;

        this.getListData(list, count);
    }

    shouldComponentUpdate(nextProps, nextState, nextContext) {
        const {list: l1} = nextProps;
        const {list: l2} = this.props;
        return !_.eq(l1, l2) || !_.eq(this.state.filterKey, nextState.filterKey);
    }

    componentWillUpdate(nextProps, nextState, nextContext) {
        const {list: l1, count: c1} = nextProps;
        const {list: l2, count: c2} = this.props;
        return !_.eq(l1, l2) && !_.eq(c1, c2);
    }


    componentDidMount() {
        const {limit, filterKey, sortField, order, key, activePage} = this.state;
        const {requestProvider, list, count} = this.props;

        if (_.isFunction(requestProvider)) {
            requestProvider(limit, activePage * limit, filterKey, sortField, order, key);
        }
        this.getListData(list, count);
    }

    getListData(list, count) {
        this.setState({
            list: Immutable.fromJS(list),
            count,
        });
    };

    changeSortColumn(sortField, order) {
        this.setState({
            sortField,
            order
        });
        const {limit, filterKey, activePage, key} = this.state;
        const {requestProvider} = this.props;

        if (_.isFunction(requestProvider)) {
            requestProvider(limit, activePage * limit, filterKey, sortField, order, key);
        }
    };

    renderColumns(columnsConfig, list) {
        const {loader} = this.props;
        const rows = [];

        if (loader && loader.count > 0) {
            return (
                <TableRow>
                    <TableCell
                        colSpan={columnsConfig.size}
                    >
                        <Loader size={60} thickness={5}/>
                    </TableCell>
                </TableRow>
            );
        }

        list.forEach((item, index) => {
            rows.push(
                <TableRow
                    hover
                    key={index}
                    tabIndex={-1}>
                    {this.renderRow(item, columnsConfig)}
                </TableRow>
            );
        });
        return rows;
    }

    renderRow(value, columnsConfig) {
        let rows = [];

        columnsConfig.forEach((item, index) => {
            const getValue = item.get('value');

            rows.push(
                <TableCell
                    key={index}
                    style={{padding: "8px 40px 8px 16px"}}
                    align={item.get("align") || "left"}>
                    {getValue(value)}
                </TableCell>
            );
        });

        return rows;
    };


    onChangeRowsPerPage = (event, {key = 10}) => {
        this.setState({
            limit: key
        });
        const {start, filterKey, sortField, order} = this.state;
        const {requestProvider} = this.props;

        if (_.isFunction(requestProvider)) {
            requestProvider(key, start, filterKey, sortField, order, this.state.key);
        }
    };

    onChangePage = (event, page) => {
        const {limit, sortField, filterKey, order, key} = this.state;
        const {requestProvider} = this.props;


        this.setState({
            activePage: page
        });
        if (_.isFunction(requestProvider)) {
            requestProvider(limit, page * limit, filterKey, sortField, order, key);
        }
    };

    updateList(event) {
        if (event) {
            event.preventDefault();
        }
        const {limit, filterKey, sortField, order, key, activePage} = this.state;
        const {requestProvider} = this.props;

        if (_.isFunction(requestProvider)) {
            requestProvider(limit, activePage * limit, filterKey, sortField, order, key);
        }
    };

    searchList(value) {
        const searchValue = value;

        this.setState({
            filterKey: searchValue
        });
        const {limit, sortField, order, key, activePage} = this.state;
        const {requestProvider} = this.props;

        if (_.isFunction(requestProvider)) {
            requestProvider(limit, activePage * limit, searchValue, sortField, order, key);
        }
    };

    resetListToDefault(event) {
        event.preventDefault();

        this.setState({
            limit: 10,
            filterKey: "",
            sortField: null,
            order: 0,
            key: null,
            activePage: 0
        });
        const {requestProvider} = this.props;

        if (_.isFunction(requestProvider)) {
            requestProvider(10, 0, null, null, 0, null);
        }
    };

    searchChange(event) {
        const eventKey = event.which;
        const value = event.target.value;

        if (eventKey === 13) {
            this.searchList(value);
        }
    };

    onSearchChange(event) {
        const value = event.target.value;

        this.setState({
            filterKey: value
        });
    };

    render() {
        const {list, count, limit, activePage, filterKey}: Readonly<any> = this.state;
        const {columnsConfig, addNewHandler}: Readonly<any> = this.props;
        const tableBody = this.renderColumns(columnsConfig, list);

        return (
            <React.Fragment>
                <ListPanelHeaderToolbar
                    filterKey={filterKey}
                    searchChange={(event) => this.searchChange(event)}
                    changeSearchChange={(event) => this.onSearchChange(event)}
                    addNewHandler={addNewHandler}
                    updateList={(event) => this.updateList(event)}
                    searchList={(value) => this.searchList(value)}
                    resetListToDefault={(event) => this.resetListToDefault(event)}
                />
                <Table>
                    <ListHeaderComponent
                        columnsConfig={columnsConfig}
                        changeSortColumn={(columnCode, sortDirection) => this.changeSortColumn(columnCode, sortDirection)}
                    />
                    <TableBody>
                        {tableBody}
                    </TableBody>
                </Table>
                <ListFooterComponent
                    activePage={activePage}
                    count={count}
                    limit={+limit}
                    onChangePage={this.onChangePage}
                    onChangeRowsPerPage={this.onChangeRowsPerPage}
                />
            </React.Fragment>
        );
    }
}