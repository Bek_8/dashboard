import React from "react";
import * as PropTypes from "prop-types";
import TablePagination from "@material-ui/core/TablePagination";

ListFooterComponent.propTypes = {
    limit: PropTypes.number,
    count: PropTypes.number,
    activePage: PropTypes.number,
    onChangePage: PropTypes.func,
    onChangeRowsPerPage: PropTypes.func
};
export default function ListFooterComponent(props:any) {
    const {limit, count, activePage, onChangePage, onChangeRowsPerPage} = props;

    return (
        <TablePagination
            labelRowsPerPage="Строк на страницу"
            rowsPerPageOptions={[10, 25, 50, 100, 500]}
            component="div"
            count={count}
            rowsPerPage={limit}
            page={activePage}
            backIconButtonProps={{
                "aria-label": "Предыдущая страница",
            }}
            nextIconButtonProps={{
                "aria-label": "Следующая страница",
            }}
            onChangePage={onChangePage}
            onChangeRowsPerPage={onChangeRowsPerPage}
        />
    );
}