import React from "react";
import _ from "lodash";
import TableRow from "@material-ui/core/TableRow";
import * as PropTypes from "prop-types";
import TableCell from "@material-ui/core/TableCell";
import TableSortLabel from "@material-ui/core/TableSortLabel";
import TableHead from "@material-ui/core/TableHead";
import {List} from "immutable";
import withStyles from "@material-ui/core/styles/withStyles";
import { useSelector } from "react-redux";

const StyledTableCell = withStyles(theme => ({
    head: {
        backgroundColor: theme.palette.primary.main,
        color: theme.palette.common.white,
        fontSize: "0.8rem"
    },
}))(TableCell);

// ListHeaderComponent.propTypes = {
//     sortField: PropTypes.string,
//     columnsConfig: PropTypes.instanceOf(List),
//     changeSortColumn: PropTypes.func
// };
export default function ListHeaderComponent(props:any) {
    const {columnsConfig} = props;
    const [sortColumn, setSortColumn] = React.useState(null);
    const [sortDirection, setSortDirection] = React.useState("asc");

    function onSortColumnChange(newColumnCode) {
        let newSortDirection;
        if (_.eq(sortColumn, newColumnCode)) {
            if (sortDirection === "asc") {
                newSortDirection = "desc";
            } else {
                newSortDirection = "asc";
            }
        } else {
            newSortDirection = "asc";
        }
        setSortDirection(newSortDirection);
        setSortColumn(newColumnCode);

        const {changeSortColumn} = props;

        if (_.isFunction(changeSortColumn)) {
            changeSortColumn(newColumnCode, sortDirection);
        }
    }

    function renderHeader() {
        const rows = [];

        if (!columnsConfig || columnsConfig.size <= 0) {
            return rows;
        }


        columnsConfig.forEach((item, index) => {
            let cellValue;

            if (item.get("sortable")) {
                cellValue = (
                    <StyledTableCell
                        key={index}
                        align={item.get("align") || "left"}
                        sortDirection={(_.eq(item.get("columnCode"), sortColumn) ? sortDirection : false) as any}>
                        <TableSortLabel
                            key={index}
                            direction={sortDirection as any}
                            active={_.eq(item.get("columnCode"), sortColumn)}
                            onClick={() => onSortColumnChange(item.get("columnCode"))}>
                            <span dangerouslySetInnerHTML={{__html: item.get("columnName")}}/>
                        </TableSortLabel>
                    </StyledTableCell>
                );
            } else {
                cellValue = (
                    <StyledTableCell
                        key={index}
                        align={item.get("align") || "left"}>
                        <span dangerouslySetInnerHTML={{__html: item.get("columnName")}}/>
                    </StyledTableCell>
                );
            }

            rows.push(
                cellValue
            );
        });

        return rows;
    }

    const tableHeader = renderHeader();
 //   const color = useSelector((state:any) => state.theme.color);

    return (
        <TableHead >
            <TableRow>
                {tableHeader}
            </TableRow>
        </TableHead>
    )
}