import React from "react";
import Card from "@material-ui/core/Card";
import {makeStyles} from "@material-ui/core";
import CardHeader from "@material-ui/core/CardHeader";
import CardContent from "@material-ui/core/CardContent";

const useStyles = makeStyles(theme => ({
    card: {
        width: "100%",
        minHeight: "70vh"
    }
}));

interface IProps {
    title?: string,
    children?: React.ReactNode,
}

export default function CardComponent(props: IProps) {
    const classes = useStyles(props);
    const {title} = props;

    return (
        <Card className={classes.card}>
            {title &&
            <CardHeader
                title={title}
            />}
            <CardContent>
                {props.children}
            </CardContent>
        </Card>
    )
}