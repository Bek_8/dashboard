import React, {ChangeEvent} from 'react';
import { createStyles, makeStyles, Theme } from '@material-ui/core/styles';
import InputLabel from '@material-ui/core/InputLabel';
import MenuItem from '@material-ui/core/MenuItem';
import FormControl from '@material-ui/core/FormControl';
import Select from '@material-ui/core/Select';
import Grid from "@material-ui/core/Grid";

const useStyles = makeStyles((theme: Theme) =>
    createStyles({
        formControl: {
            margin: theme.spacing(1),
            minWidth: 120,
        },
        selectEmpty: {
            marginTop: theme.spacing(2),
        },
        labelName:{
            '&:label':{
                fontSize:'20px',
            }
        }
    }),
);

interface IProps {
    changeTheme:(color:string) => void
}
export default function SettingsTheme(props) {
    const classes = useStyles(props);

    return(
      <Grid container spacing={1} >
          <Grid item xs={6} md={12}>
            <FormControl className={classes.formControl}  >
                <InputLabel id="demo-simple-select-label" className={classes.labelName}>Цвет темы </InputLabel>
                <Select
                    id="demo-simple-select"
                    onChange = {(event:ChangeEvent<any>) =>
                        props.changeTheme(event.target.value)}
                >
                    <MenuItem value='green'>Зеленая тема</MenuItem>
                    <MenuItem value='blue'>Голубая тема</MenuItem>
                    <MenuItem value='black'>Чёрная тема</MenuItem>

                </Select>
            </FormControl>
          </Grid>
          <Grid item xs={6} md={12}>
            <FormControl className={classes.formControl}  >
                <InputLabel id="demo-simple-select-label" className={classes.labelName}>Языки</InputLabel>
                <Select
                    id="demo-simple-select"

                >
                    <MenuItem value={10}>Руский</MenuItem>
                    <MenuItem value={20}>English</MenuItem>
                </Select>
            </FormControl>
          </Grid>
      </Grid>
    );
}