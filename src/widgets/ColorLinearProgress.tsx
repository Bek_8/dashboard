import React from "react";
import {withStyles} from "@material-ui/core";
import LinearProgress from "@material-ui/core/LinearProgress";
import makeStyles from "@material-ui/core/styles/makeStyles";

const ColorLinearProgress = withStyles({
    colorPrimary: {
        backgroundColor: '#b2dfdb',
    },
    barColorPrimary: {
        backgroundColor: '#00695c',
    },
})(LinearProgress);

const useStyles = makeStyles(theme => ({
    margin: {
        margin: theme.spacing(1),
    },
}));

export default function (props:any) {
    const {show} = props;
    const classes = useStyles(props);

    if (show) {
        return (
            <ColorLinearProgress
                className={classes.margin}
                {...props}
            />
        );
    }
    return (
        <span className="hide"/>
    )

}