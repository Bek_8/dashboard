import React from 'react';
import * as PropTypes from "prop-types";
import Dialog from '@material-ui/core/Dialog';
import DialogActions from '@material-ui/core/DialogActions';
import DialogContent from '@material-ui/core/DialogContent';
import DialogContentText from '@material-ui/core/DialogContentText';
import DialogTitle from '@material-ui/core/DialogTitle';
import useMediaQuery from '@material-ui/core/useMediaQuery';
import {useTheme} from '@material-ui/core/styles';

AutoBioDialog.propTypes = {
    show: PropTypes.bool.isRequired,
    title: PropTypes.any,
    body: PropTypes.any,
    actions: PropTypes.any,
    handleClose: PropTypes.func
};
export default function AutoBioDialog(props:any) {
    const {handleClose, show, title, body, actions} = props;

    const theme = useTheme();
    const fullScreen = useMediaQuery(theme.breakpoints.down('sm'));

    return (
        <Dialog
            open={show}
            onClose={handleClose}
            fullScreen={fullScreen}
            aria-labelledby="responsive-dialog-title">

            {title &&
            <DialogTitle>
                {title}
            </DialogTitle>}

            {body &&
            <DialogContent>
                <DialogContentText>
                    {body}
                </DialogContentText>
            </DialogContent>}

            {actions &&
            <DialogActions>
                {actions}
            </DialogActions>}
        </Dialog>
    );
}