// import {} from "react-router";
import history from "../routes/HistoryApi"
import qs from "qs"
import * as H from "history";
import fp from "lodash/fp";


/**
 * @param {Object} query
 */
export const addQuery = (query) => {
    const location = Object.assign({}, history.location);
    // if (!location.query) {
    //     location.query = {};
    // }
    // Object.assign(location.search, query);
    // location.search = "view=1";
    location.hash = query;
    // or simple replace location.query if you want to completely change params

    history.push(location);
};

/**
 * @param {...String} queryNames
 */
export const removeQuery = (...queryNames) => {
    const location: any = Object.assign({}, history.location);
    queryNames.forEach(q => delete location.query[q]);
    history.push(location);
};

export const getLocationQuery = (location) =>
    qs.parse(location.search, {ignoreQueryPrefix: true})

export const updateQuery = (location: H.Location, query: any = {}) => {
    return `${location.pathname}${qs.stringify(query, {addQueryPrefix: true})}${location.hash}`
}
export const navigateTo = (history: H.History, to, replace) =>
    replace ? history.replace(to) : history.push(to);


