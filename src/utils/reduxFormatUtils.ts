//validate
export const required = value =>
    !value ? 'Заполните обязательное поле' : undefined;

export const requiredSelect = (value:any) => {
    if (!value || value === '' || value == null) {
        return 'Заполните обязательное поле'
    }
    return undefined;
};

export const maxLengthFactory = (max, text) => value =>
    value && value.length > max ? text || `Must be ${max} characters or less` : undefined;

export const minLengthFactory = min => value =>
    value && value.length < min ? `Must be ${min} characters or more` : undefined;

export const number = value =>
    value && isNaN(Number(value)) ? 'Must be a number' : undefined;

export const autoCompleteID = value =>
    value && isNaN(Number(value)) ? 'Please, select from the list' : undefined;

export const minValueFactory = min => value =>
    value && value < min ? `Must be at least ${min}` : undefined;

export const email = value =>
    value && !/^[A-Z0-9._%+-]+@[A-Z0-9.-]+\.[A-Z]{2,4}$/i.test(value)
        ? 'Неправильная эл. почта'
        : undefined;
export  const login = value =>
    value && !(value.length > 3);

export const alphaNumeric = value =>
    value && /[^a-zA-Z0-9 ]/i.test(value)
        ? 'Only alphanumeric characters'
        : undefined;

export const phoneNumber = value =>
    value && !/^[0-9]{8,15}$/.test(value)
        ? 'Неправильный телефон'
        : undefined;

export const notRepeat = value =>
    value && /(.)\1\1\1+/g.test(value)
        ? 'May not contain 4 or more consecutive repeating characters'
        : undefined;


export const sequential = value =>
    value && /(?:abcd|bcde|cdef|defg|efgh|fghi|ghij|hijk|ijkl|jklm|klmn|lmno|mnop|nopq|opqr|pqrs|qrst|rstu|stuv|tuvw|uvwx|vwxy|wxyz|0123|1234|2345|3456|4567|5678|6789|7890)/i.test(value)
        ? 'May not contain 4 or more consecutive sequential characters'
        : undefined;

export const matchValue = (matchValueKey, text) => (value, allValues) => value !== allValues[matchValueKey]
    ? text || "Values not match"
    : undefined;

export const oneDigits = (value:any) => value && !/^(?=.*[0-9])/.test(value) ? "Must have one or more digits" : undefined;

export const oneUppercase = (value:any) => value && !/^(?=.*[A-Z])/.test(value) ? "Must have one or more uppercase" : undefined;

export const oneLowercase = (value:any) => value && !/^(?=.*[a-z])/.test(value) ? "Must have one or more lowercase" : undefined;

export const oneSpecialCharacters = (value:any) => value && !/^(?=.*[!@#$%^&*])/.test(value) ? "Must have one or more special characters !@#$%^&*" : undefined;

// normalize
export const normalizePhone = (value:any) => (value && value.replace(/[^\d]/g, '')) || "";

// format
export const formatPhone = (value:any) => {
    if (!value) {
        return value
    }

    if (value.length <= 3) {
        return `${value}`
    }

    if (value.length <= 7) {
        return `(${value.slice(0, 3)}) ${value.slice(3)}`
    }

    return `(${value.slice(0, 3)}) ${value.slice(3, 6)}-${value.slice(6, 10)}`;
};

export const formatPrice = (value:any) => {
    const formatter = new Intl.NumberFormat('en-US', {
        minimumFractionDigits: 0,
    });

    return (value && formatter.format(value || 0)) || "";
};

//filterOption
export const filterUpperCase = (inputValue, option) => {
    if (option.props.children.toUpperCase) {
        return option.props.children.toUpperCase().indexOf(inputValue.toUpperCase()) !== -1;

    } else if (option.props.text && option.props.text.toUpperCase) {
        return option.props.text.toUpperCase().indexOf(inputValue.toUpperCase()) !== -1;

    } else {
        return false;
    }
};
