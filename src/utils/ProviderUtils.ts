import * as DateUtils from "./DateUtils";
import fp from "lodash/fp"
import {format, parse} from "date-fns";
import qs from "qs";

export interface TimeSlotObj {
    weekday: string,
    startTime: Date | string,
    endTime: Date | string,
    selected?: boolean
    id?: number
}

const getStartEndRange = (hourMinute: string) => {
    const [hour, minute] = hourMinute.split(":")
    return DateUtils.makeHourTime(Number(hour), Number(minute))
}
export const getTimeSlotList = (): TimeSlotObj[] => {
    const [startTime, endTime] = [DateUtils.makeHourTime(9), DateUtils.makeHourTime(20)]

    return [
        {weekday: "MONDAY", startTime, endTime},
        {weekday: "TUESDAY", startTime, endTime},
        {weekday: "WEDNESDAY", startTime, endTime},
        {weekday: "THURSDAY", startTime, endTime},
        {weekday: "FRIDAY", startTime, endTime},
        {weekday: "SATURDAY", startTime, endTime},
        {weekday: "SUNDAY", startTime, endTime},
    ]
}

export const getProviderItemPayload = (data) => {
    return {
        ...data,
        timeslots: data.timeslots
            .filter(
                fp.flow(
                    fp.get("selected"),
                    Boolean
                )
            )
            .map(
                fp.flow(
                    fp.omit(["selected"]),
                    fp.update("startTime", d => format(d, "HH:mm")),
                    fp.update("endTime", d => format(d, "HH:mm"))
                )
            )

    }
}
export const getProviderItemResponse = (data) => {
    const rest:any = {};
    if(data.comissionId){
        rest.comission = {id: data.comissionId, name: data.comissionName};
    }
    if(data.ownerId){
        rest.owner = {id: data.ownerId, name: data.ownerName};
    }
    return {
        ...data,
        ...rest,
        timeslots: getTimeSlotList()
            .map((wItem: TimeSlotObj) => {
                if (data.timeslots) {
                    const found = data.timeslots.find(fp.flow(fp.get("weekday"), fp.eq(wItem.weekday)));
                    if (found) {
                        return {
                            ...found,
                            startTime: getStartEndRange(found.startTime),
                            endTime: getStartEndRange(found.endTime),
                            selected: true
                        }
                    }
                }
                return wItem;
            })

    }
}
const getId = fp.get('id')
const getCode = fp.get('code')
// const getId = fp.get('id')
export const convertServicePayload = (payload) => {
    const {category, parentCategory, ...rest} = payload
    return {
        parentCategory: getCode(parentCategory),
        categoryId: getId(category),
        ...rest
    }
}

export const convertServiceResponse = (response) => {
    const {categoryId, parentCategory, categoryName, ...rest} = response
    return {
        parentCategory: {code: parentCategory},
        category: {id: categoryId, nameRu: categoryName},
        ...rest
    }
}

export const getProviderTab = fp.flow(
    fp.get("hash"),
    hash => hash || "#details",
    str => str.substring(1)
);

export const convertAdditionalCostsForPayload = (data: any = {}) => {
    const costs = Boolean(data) ? data.additionalCosts : [{}]
    const additionalCosts = costs.reduce((acc, item) => ({[item.distance]: item.percentage}), {});
    return {...data, phone: data.phone.replace('+', ''), additionalCosts}
}
export const convertAdditionalCostsForResponse = (response: any = {}) => {
    const costs = Boolean(response && response.additionalCosts) ? response.additionalCosts : {}

    const additionalCosts = Object.keys(costs).map(key => ({distance: key, percentage: costs[key]}));

    return {...response, additionalCosts}
}
