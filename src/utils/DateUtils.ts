import {isDate, isValid, parse,format, setHours, setMinutes} from "date-fns";
import fp from "lodash/fp";
import {getTimeSlotList, TimeSlotObj} from "./ProviderUtils";

export const isValidDate = fp.overEvery([isDate, isValid]);

export const safeParseDate = fp.flow(
    v => (fp.isNumber(v)
    || fp.isString(v) ?
        parse(v, "dd/MM/yyyy", new Date()) :
        v),

    v => (isValidDate(v) ? v : null),
);

export const makeHourTime = (hour: number, minute: number=0) => {
    return setMinutes(setHours(new Date(), hour), minute)
}

const getStartEndRange = (hourMinute: string) => {
    const [hour, minute] = hourMinute.split(":")
    return makeHourTime(Number(hour), Number(minute))
}

export const convertTimeSlotsPayload = (data) => {
    return {
        ...data,
        timeslots: data.timeslots
            .filter(
                fp.flow(
                    fp.get("selected"),
                    Boolean
                )
            )
            .map(
                fp.flow(
                    fp.omit(["selected"]),
                    fp.update("startTime", d => format(d, "HH:mm")),
                    fp.update("endTime", d => format(d, "HH:mm"))
                )
            )

    }
}

export const convertTimeSlotsResponse = (data) => {
    return {
        ...data,
        timeslots: getTimeSlotList()
            .map((wItem: TimeSlotObj) => {
                if (data.timeslots) {
                    const found = data.timeslots.find(fp.flow(fp.get("weekday"), fp.eq(wItem.weekday)));
                    if (found) {
                        return {
                            ...found,
                            startTime: getStartEndRange(found.startTime),
                            endTime: getStartEndRange(found.endTime),
                            selected: true
                        }
                    }
                }
                return wItem;
            })

    }
}