import {createHashHistory} from "history";
import {createStore, combineReducers, applyMiddleware} from "redux";
import {routerReducer, routerMiddleware} from "react-router-redux";
import thunkMiddleware from "redux-thunk";
import logger from "redux-logger";
import {reducer as formReducer} from "redux-form";
import apiMiddleware from "../redux/middleware/apiMiddleware";
import adminUser from "../redux/reducers/AdminUserReducer";

import garageUser from "../redux/reducers/GarageUserReducer";
import motorist from "../redux/reducers/MotoristReducer";


import auth from "../redux/reducers/AuthReducer";
import loader from "../redux/reducers/LoaderReducer";
import message from "../redux/reducers/MessageReducer";
//Spravochniki
import reference from "../redux/reducers/ReferenceReducer";
import autoService from "../redux/reducers/AutoServiceReducer";
import oilStation from "../redux/reducers/OilStationReducer";
import makeModels from "../redux/reducers/MakeModelsReducer";

import provider from "../redux/reducers/ProviderReducer";
import deposit from "../redux/reducers/DepositReducer";
import technician from "../redux/reducers/TechnicianReducer";

import fuelType from "../redux/reducers/FuelTypeReducer";
import make from "../redux/reducers/MakeReducer";
import model from "../redux/reducers/ModelReducer";
import modification from "../redux/reducers/ModificationReducer";
import service from "../redux/reducers/ServiceReducer";
import vehicle from "../redux/reducers/VehicleReducer";
import vehicleType from "../redux/reducers/VehicleTypeReducer";
import theme from "../redux/reducers/ChangeThemeColorReducer";


export const DEV = process.env.NODE_ENV === 'development';


const history = createHashHistory();
const routeMiddleware = routerMiddleware(history);

const middleWares = [
    thunkMiddleware,
    routeMiddleware,
    apiMiddleware,
    DEV && logger,
].filter(Boolean);

export const store = createStore(
    combineReducers({
        form: formReducer,
        router: routerReducer,
        adminUser,
        garageUser,
        motorist,
        auth,
        loader,
        message,

        reference,
        autoService,
        oilStation,
        makeModels,

        provider,
        deposit,
        technician,

        fuelType,
        make,
        model,
        modification,
        service,
        vehicle,
        vehicleType,
        theme,
    }),
    applyMiddleware(
        ...middleWares
    )
);
