import axios from "axios/index";
import {HttpConfig} from "../constants/Constants";

export default class HttpClient {
    static headers: any = {
        "Content-Type": "application/json"
    };

    /**@private*/
    static doRequest() {
        const userToken = localStorage.getItem("token");
        const tokenType = localStorage.getItem("tokenType");

        if (userToken) {
            HttpClient.headers = {
                ...HttpClient.headers,
                'Accept-Language': 'ru',
                Authorization: `Bearer ${userToken}`
            };
        }
        return axios.create({
            headers: {
                ...HttpClient.headers
            },
        });
    }

    static doGet(url, params = {}) {
        return HttpClient.doRequest().get(makeUrl(url), {params});
    }

    static doGetBase(url, params = {}) {
        return HttpClient.doRequest().get(url, {params});
    }

    static doDeleteBase(url, params = {}) {
        return HttpClient.doRequest().delete(url, {params});
    }

    // static doPost(url, data) {
    //     return HttpClient.doRequest().post(url, data);
    // }
    //
    static doPost(url, data, up = null) {
        return HttpClient.doRequest().post(makeUrl(url), data, up);
    }

    static doPut(url, data) {
        return HttpClient.doRequest().put(makeUrl(url), data);
    }

    static doDelete(url, params = {}) {
        return HttpClient.doRequest().delete(makeUrl(url), {params});
    }


}

function makeUrl(url) {
    return url.includes('http') ? url : `${HttpConfig.API_PATH}${url}`
}

export function makeBaseUrl(url) {
    return url.includes('http') ? url : `${HttpConfig.BASE_URL}${url}`
}
