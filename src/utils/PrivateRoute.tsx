import React from "react";
import {connect} from "react-redux";
import {Redirect, Route} from "react-router-dom";

const PrivateRoute = ({component: Component, ...rest}) => {
    console.log("PrivateRoute --- ", rest)
    return (
        <Route {...rest} render={(props: any) => (
            rest != null && rest.auth != null && rest.auth.token != null
                ? <Component {...props} />
                : <Redirect to={{
                    pathname: "/login",
                    state: {from: props.location}
                }}/>
        )}/>
    );
}
export default connect(({privateRoute, auth}:any) => ({privateRoute, auth}))(PrivateRoute);
