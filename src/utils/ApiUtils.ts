import {get} from "lodash";
import {ListingObj} from "../types/ApiOjbects";


export function getListingObjFromResponse<T>(action): ListingObj<T> {
    return {
        list: get(action, "payload.data.list", []),
        count: get(action, "payload.data.count", 0)
    }
}