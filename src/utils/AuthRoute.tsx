import React from "react";
import {connect} from "react-redux";
import {Redirect, Route} from "react-router-dom";

const AuthRoute = ({component: Component, ...rest}) => (
    <Route {...rest} render={(props:any) => (
        rest != null && rest.auth != null && rest.auth.token != null
            ?  <Redirect to={{
                pathname: "/login",
                state: {from: props.location}
            }}/>:<Component {...props} />
    )}/>
);
export default connect(({privateRoute, auth}:any) => ({privateRoute, auth}))(AuthRoute);
