import fp from "lodash/fp";

export const convertCodeToTitle = fp.flow(
    fp.toLower,
    fp.startCase
)