import React from 'react';
import ReactDOM from 'react-dom';
import App from "./App";
import {SnackbarProvider, VariantType, useSnackbar} from 'notistack';

ReactDOM.render(
    <SnackbarProvider>
        <App/>
    </SnackbarProvider>,
    document.getElementById('root'));
