import React from 'react';
import {Provider} from "react-redux";
import {configReduxStore} from "./redux/configReduxStore";
import RootLayout from "./components/layout-core/RootLayout";
import {HashRouter} from "react-router-dom";
import {Routes} from "./routes/Routes";
import {PersistGate} from 'redux-persist/integration/react';

App.propTypes = {};
const {store, persistor} = configReduxStore();

function App() {
    return (
            <Provider store={store}>
                <PersistGate loading={null} persistor={persistor}>
                    <RootLayout>
                        <HashRouter>
                            <Routes/>
                        </HashRouter>
                    </RootLayout>
                </PersistGate>
            </Provider>
    )
}

export default App;
