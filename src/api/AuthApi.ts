import HttpClient from "../utils/HttpClient";
import { HttpConfig } from "../constants/Constants";

export const loginUser = (query = { username: null, password: null }) => {
  return HttpClient.doPost(HttpConfig.BASE_URL.concat("/api/login"), query);
};
export const getUser = (query) => {
  return HttpClient.doGet(HttpConfig.BASE_URL.concat("/api/account"), query);
};

// export const getUserToken = (data) => {
//     return http().post("sessions", data)
// };
