import HttpClient from "../utils/HttpClient";

export const list = (query = {start: 0, limit: 10, searchKey: null, sortField: null, sortOrder: null}) => {
    return HttpClient.doGet("/fuel-type/list", query);
};

export const item = (query = {id: null}) => {
    return HttpClient.doGet("/fuel-type/item", query);
};

export const deleteItem = (query = {id: null}) => {
    return HttpClient.doDelete("/fuel-type/item", query);
};

export const createItem = (query) => {
    return HttpClient.doPost("/fuel-type/item", query);
};

export const updateItem = (query) => {
    return HttpClient.doPut("/fuel-type/item", query);
};