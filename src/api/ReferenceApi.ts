import HttpClient from "../utils/HttpClient";

export const list = (query = {start: 0, limit: 10, searchKey: null, sortField: null, sortOrder: null}) => {
    return HttpClient.doGet("/reference/list", query);
};

export const item = (id) => {
    return HttpClient.doGet("/reference/item/" + id);
};

export const deleteItem = (id) => {
    return HttpClient.doDelete("/reference/item/" + id);
};

export const createItem = (query) => {
    return HttpClient.doPost("/reference/item", query);
};

export const updateItem = (query) => {
    return HttpClient.doPut("/reference/item", query);
};
export const getAllCategoryTypes = (query) => {
    return HttpClient.doGet("/reference/choose", query);
};
export const getGenders = (query) => {
    return HttpClient.doGet("/genders", query);
};
export const getStatuses = (query) => {
    return HttpClient.doGet("/member-statuses", query);
};

export const getRoles = (query) => {
    return HttpClient.doGet("/member-roles", query);
};

export const getRegions = (query) => {
    return HttpClient.doGet("/regions", query);
};
export const getDistricts = (regionId) => {
    return HttpClient.doGet(`/districts/${regionId}`);
};
export const getWeekdays = (query) => {
    return HttpClient.doGet("/weekdays", query);
};
