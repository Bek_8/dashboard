import HttpClient from "../utils/HttpClient";

export const list = (query = {start: 0, limit: 10, searchKey: null, sortField: null, sortOrder: null}) => {
    return HttpClient.doGet("/vehicle/list", query);
};

export const item = (query = {id: null}) => {
    return HttpClient.doGet("/vehicle/item", query);
};

export const deleteItem = (query = {id: null}) => {
    return HttpClient.doDelete("/vehicle/item", query);
};

export const createItem = (query) => {
    return HttpClient.doPost("/vehicle/item", query);
};

export const updateItem = (query) => {
    return HttpClient.doPut("/vehicle/item", query);
};