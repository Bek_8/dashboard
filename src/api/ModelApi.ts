import HttpClient from "../utils/HttpClient";

export const list = (query = {makeId: null,start: 0, limit: 10, searchKey: null, sortField: null, sortOrder: null}) => {
    return HttpClient.doGet("/model/list", query);
};

export const item = (query = {id: null}) => {
    return HttpClient.doGet("/model/item", query);
};

export const deleteItem = (query = {id: null}) => {
    return HttpClient.doDelete("/model/item", query);
};

export const createItem = (query) => {
    return HttpClient.doPost("/model/item", query);
};

export const updateItem = (query) => {
    return HttpClient.doPut("/model/item", query);
};