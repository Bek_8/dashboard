import HttpClient from "../utils/HttpClient";

const CURRENT_ROOT = 'technician'

export const list = (query = {start: 0, limit: 10, searchKey: null, sortField: null, sortOrder: null}) => {
    return HttpClient.doGet(`/${CURRENT_ROOT}/list`, query);
};

export const item = (id) => {
    return HttpClient.doGet(`/${CURRENT_ROOT}/item/${id}`);
};

export const deleteItem = (id) => {
    return HttpClient.doDelete(`/${CURRENT_ROOT}/item/${id}`);
};

export const createItem = (query) => {
    return HttpClient.doPost(`/${CURRENT_ROOT}/item`, query);
};

export const updateItem = (query) => {
    return HttpClient.doPut(`/${CURRENT_ROOT}/item`, query);
};