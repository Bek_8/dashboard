import HttpClient from "../utils/HttpClient";

export const list = (query = {start: 0, limit: 10, searchKey: null, sortField: null, sortOrder: null}) => {
    return HttpClient.doGet("/member/list", query);
};

export const item = (query = {id: null}) => {
    return HttpClient.doGet("/member/item", query);
};

export const deleteItem = (query = {id: null}) => {
    return HttpClient.doDelete("/member/item", query);
};

export const createItem = (query) => {
    return HttpClient.doPost("/member/item", query);
};

export const updateItem = (query) => {
    return HttpClient.doPut("/member/item", query);
};