import HttpClient from "../utils/HttpClient";

export const list = (query = {start: 0, limit: 10, searchKey: null, sortField: null, sortOrder: null}) => {
    return HttpClient.doGet("/provider/list", query);
};

export const item = (id) => {
    return HttpClient.doGet("/provider/item/"+id);
};

export const deleteItem = (id ) => {
    return HttpClient.doDelete("/provider/item/"+id);
};

export const createItem = (query) => {
    return HttpClient.doPost("/provider/item", query);
};

export const updateItem = (query) => {
    return HttpClient.doPut("/provider/item", query);
};
export const getAllCategoryTypes = (query) => {
    return HttpClient.doGet("/reference/choose", query);
};
