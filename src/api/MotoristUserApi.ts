import HttpClient from "../utils/HttpClient";

export const list = (query = {start: 0, limit: 10, searchKey: null, sortField: null, sortOrder: null}) => {
    return HttpClient.doGet("/motorist/list", query);
};

export const item = (query = {id: null}) => {
    return HttpClient.doGet("/motorist/item", query);
};

export const deleteItem = (query = {id: null}) => {
    return HttpClient.doDelete("/motorist/item", query);
};

export const createItem = (query) => {
    return HttpClient.doPost("/motorist/item", query);
};

export const updateItem = (query) => {
    return HttpClient.doPut("/motorist/item", query);
};

export const getAllStatues = (query) => {
    return HttpClient.doGet("/motorist/select/status", query);
};

export const getAllGenderTypes = (query) => {
    return HttpClient.doGet("/motorist/select/gender-types", query);
};
