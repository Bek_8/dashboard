import HttpClient, {makeBaseUrl} from "../utils/HttpClient";

export const list = (query = {start: 0, limit: 10, searchKey: null, sortField: null, sortOrder: null}) => {
    return HttpClient.doGet("/garage-user/list", query);
};

export const item = (query = {id: null}) => {
    return HttpClient.doGetBase(makeBaseUrl("/api/mobile/v1/member/item/"+query.id));
};

export const deleteItem = (query = {id: null}) => {
    return HttpClient.doDeleteBase(makeBaseUrl("/api/mobile/v1/member/item/"+query.id));
};
//
// export const deleteItem = (query = {id: null}) => {
//     return HttpClient.doDelete("/garage-user/item", query);
// };

export const createItem = (query) => {
    return HttpClient.doPost("/garage-user/item", query);
};

export const updateItem = (query) => {
    return HttpClient.doPut("/garage-user/item", query);
};
