import HttpClient from "../utils/HttpClient";



export const uploadFile = (query) => {
    return HttpClient.doPost("/file/item", query);
};
export const uploadFileList = (query) => {
    return HttpClient.doPost("/file/list", query);
};

export const deleteItem = (query = {id: null}) => {
    return HttpClient.doDelete("/file/item", query);
};
