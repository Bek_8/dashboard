import HttpClient from "../utils/HttpClient";

export const list = (query = {start: 0, limit: 10, searchKey: null, sortField: null, sortOrder: null}) => {
    return HttpClient.doGet("/commission/list", query);
};
export const item = (id) => {
    return HttpClient.doGet("/commission/item/"+id);
};
//
// export const getItemByProviderId = (providerId) => {
//     return HttpClient.doGet("commission/info/"+providerId);
// };

export const deleteItem = (query = {id: null}) => {
    return HttpClient.doDelete("/commission/item", query);
};

export const createItem = (query) => {
    return HttpClient.doPost("/commission/item", query);
};

export const updateItem = (query) => {
    return HttpClient.doPut("/commission/item", query);
};
