import HttpClient from "../utils/HttpClient";

export const list = (query = {start: 0, limit: 10, searchKey: null, sortField: null, sortOrder: null}) => {
    return HttpClient.doGet("/service/list", query);
};

export const item = (id) => {
    return HttpClient.doGet(`/service/item/${id}`);
};

export const deleteItem = (id) => {
    return HttpClient.doDelete(`/service/item/${id}`);
};

export const createItem = (query) => {
    return HttpClient.doPost("/service/item", query);
};

export const updateItem = (query) => {
    return HttpClient.doPut("/service/item", query);
};
export const getSubList = (query = {start: 0, limit: 10, searchKey: null, sortField: null, sortOrder: null}) => {
    return HttpClient.doGet("/service-item", query);
};

export const getSubItem = (id) => {
    return HttpClient.doGet(`/service-item/${id}`);
};

export const deleteSubItem = (id) => {
    return HttpClient.doDelete(`/service-item/${id}`);
};

export const createSubItem = (query) => {
    return HttpClient.doPost("/service-item", query);
};

export const updateSubItem = (query) => {
    return HttpClient.doPut("/service-item", query);
};
