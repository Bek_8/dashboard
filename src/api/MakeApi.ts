import HttpClient from "../utils/HttpClient";

export const list = (query = {start: 0, limit: 10, searchKey: null, sortField: null, sortOrder: null}) => {
    return HttpClient.doGet("/make/list", query);
};

export const item = (query = {id: null}) => {
    return HttpClient.doGet("/make/item", query);
};

export const deleteItem = (query = {id: null}) => {
    return HttpClient.doDelete("/make/item", query);
};

export const createItem = (query) => {
    return HttpClient.doPost("/make/item", query);
};

export const updateItem = (query) => {
    return HttpClient.doPut("/make/item", query);
};