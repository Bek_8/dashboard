import HttpClient from "../utils/HttpClient";

export const list = (query = {modelId: null, start: 0, limit: 10, searchKey: null, sortField: null, sortOrder: null}) => {
    return HttpClient.doGet("/modification/list", query);
};

export const item = (query = {id: null}) => {
    return HttpClient.doGet("/modification/item", query);
};

export const deleteItem = (query = {id: null}) => {
    return HttpClient.doDelete("/modification/item", query);
};

export const createItem = (query) => {
    return HttpClient.doPost("/modification/item", query);
};

export const updateItem = (query) => {
    return HttpClient.doPut("/modification/item", query);
};