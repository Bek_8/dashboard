import HttpClient from "../utils/HttpClient";

export const list = (query = { start: 0, limit: 10, searchKey: null, sortField: null, sortOrder: null }) => {
    return HttpClient.doGet("/deposit/list", query);
};

export const item = (id) => {
    return HttpClient.doGet("/deposit/item/" + id);
};

export const getItemByProviderId = (providerId) => {
    return HttpClient.doGet("/deposit/info/" + providerId);
};

export const deleteItem = (query = { id: null }) => {
    return HttpClient.doDelete("/deposit/item", query);
};

export const createItem = (query) => {
    return HttpClient.doPost("/deposit/item", query);
};

export const updateItem = (query) => {
    return HttpClient.doPut("/deposit/item", query);
};