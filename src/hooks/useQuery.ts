import {useLocation} from "react-router";
import H from "history";
import {getLocationQuery} from "../utils/UrlUtils";

export default function useQuery(): any {
    const location: H.Location = useLocation();
    return getLocationQuery(location.search) || {};
}