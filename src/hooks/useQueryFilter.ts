import H from "history";
import {getLocationQuery} from "../utils/UrlUtils";
import {DEFAULT_FILTER} from "../constants/Constants";
import {useMemo} from "react";
import {useLocation} from "react-router";

export default function useQueryFilter(extraQuery: any = {}): any {
    const location: H.Location = useLocation();

    return useMemo(() => ({
        ...DEFAULT_FILTER,
        ...extraQuery,
        ...getLocationQuery(location),
    }), [location.search]);
}