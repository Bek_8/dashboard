import React from 'react';
import CircularProgress from "@material-ui/core/CircularProgress";

interface IProps {
    size: number;
    thickness: number;
    variant?: 'determinate' | 'indeterminate' | 'static';
}

export default function Loader(props: IProps) {
    const {size = 48, thickness = 3} = props;
    return (
        <CircularProgress
            {...props}
            size={size}
            thickness={thickness}
            style={{
                margin: "15px auto",
                display: "block"
            }}
        />
    );
}