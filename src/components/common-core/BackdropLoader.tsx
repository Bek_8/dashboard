import React from 'react';
import Backdrop from '@material-ui/core/Backdrop';
import {toggleBackdropLoader} from '../../redux/actions/MessageActions';
import CircularProgress from '@material-ui/core/CircularProgress';
import {makeStyles, createStyles, Theme} from '@material-ui/core/styles';
import {createSelector} from "reselect";
import {useDispatch, useSelector} from "react-redux";

const useStyles = makeStyles((theme: Theme) =>
    createStyles({
        backdrop: {
            zIndex: theme.zIndex.drawer + 1,
            color: '#fff',
        },
    }),
);

export default function BackdropLoader(props) {
    const dispatch = useDispatch()
    const classes = useStyles(props);
    const [open, setOpen] = React.useState(false);

    const loading = useSelector(createSelector(
        (state: any) => state.message,
        (stateSection: any) => stateSection.backdrop
    ))
    return (<Backdrop
            className={classes.backdrop}
            open={loading}
            onClick={() => {
                dispatch(toggleBackdropLoader(false))
            }}
        >
            <CircularProgress color="inherit"/>
        </Backdrop>
    );
}
