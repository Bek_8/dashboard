import React from 'react';
import {IconButton, Snackbar} from "@material-ui/core";
import {useDispatch, useSelector} from "react-redux";
import {toggleSnackbar} from "../../redux/actions/MessageActions";
import CloseIcon from "@material-ui/icons/Close"
import SnackbarContentWrapper from "../common-core/SnackbarContentWrapper";

interface IProps {

}

const SnackbarComponent = (props: IProps) => {

    const dispatch = useDispatch()

    const snackbar = useSelector((state: any) => state.message.snackbar)

    const handleClose = () => {
        dispatch(toggleSnackbar(''))
    }

    return (
        <Snackbar
            open={snackbar.show}
            anchorOrigin={{vertical: 'bottom', horizontal: "right"}}
            onClose={() => handleClose()}
            ContentProps={{
                'aria-describedby': 'message-id',
            }}
            message={<span id="message-id">{snackbar.message}</span>}
        >
            <SnackbarContentWrapper
                variant={snackbar.messageVariant}
                message={snackbar.message}
                onClose={handleClose}
            />
        </Snackbar>)
}

export default SnackbarComponent;
