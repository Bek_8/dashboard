import React from 'react';
import AppBar from "@material-ui/core/AppBar/AppBar";
import Tabs from "@material-ui/core/Tabs";
import Tab from "@material-ui/core/Tab";
import {Link} from "react-router-dom";
import {Location} from "history";

type Props = {
    selectedTab: string,
    location: Location,
    tabs: any[]
};

export default function ReferenceEditItemTabs(props: Props) {
    return (<Tabs
        value={props.selectedTab}
        indicatorColor="primary"
        textColor="primary"
        variant="scrollable"
        scrollButtons="auto">
        {props.tabs.map(tabItem => (
            <Tab
                disabled={tabItem.disabled}
                label={tabItem.name}
                key={tabItem.id}
                component={Link}
                to={`${props.location.pathname}#${tabItem.value}`}
                value={tabItem.value}
            />
        ))}

    </Tabs>);
};