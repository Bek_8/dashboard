import React, {Fragment, useEffect} from "react";
import {useDispatch, useSelector} from "react-redux";
import * as action from "../../redux/actions/MakeModelsActions";

import fp from "lodash/fp"
import IconButton from "@material-ui/core/IconButton";
import EditIcon from "@material-ui/icons/Edit";
import {Link, useHistory, useLocation, useParams} from "react-router-dom";
import {updateQuery} from "../../utils/UrlUtils";
import DataTable, {DataCol} from "../../components/table-core/DataTable";
import useQueryFilter from "../../hooks/useQueryFilter";
import {Card} from "@material-ui/core";
import {setDeleteConfirmationItem} from "../../redux/actions/MessageActions";
import DeleteIcon from "@material-ui/icons/Delete";

// import CardComponent from "../../widgets/CardComponent";
import {createSelector} from "reselect";
import * as H from "history";

interface IProps {
    parentItem: any
}

const getListingReducers = createSelector(
    (state: any) => state.makeModels,
    ({list, count, loader}) => ({list, count, loader})
);
const getParentCodeAndId = fp.flow(
    fp.over([
        fp.get("parentItem.code"),
        fp.get("parentItem.id")
    ])
);

export default function MakeChildrenTab(props: IProps) {
    // const params = useParams()
    const dispatch = useDispatch()
    const location: H.Location = useLocation();
    const [parentCode, parentId] = getParentCodeAndId(props);
    const filter = useQueryFilter({parentCode})
    const history: H.History = useHistory()
    const {list, count, loader} = useSelector(getListingReducers)


    useEffect(() => {
        dispatch(action.listAction(filter))
    }, [filter])

    return (
        <Card>
            <DataTable
                listing={{list, count}}
                filter={filter}
                loader={loader}
                addLink={`/reference/make/${parentId}/child/add?parentCode=${parentCode}`}
                onDeleteItem={(itemId) => dispatch(action.deleteItemAction(itemId))}
                getList={(filter) => dispatch(action.listAction(filter))}
                onFilterChange={query => history.push(updateQuery(location, query))}
            >
                <DataCol label={"ID"}
                         sortable={true}
                         fieldKey={"id"}
                         cellRenderer={row => row.get("id")}/>

                <DataCol label={"Название"}
                         sortable={true}
                         fieldKey={"nameRu"}
                         cellRenderer={row => row.get("nameRu")}
                />


                <DataCol label={"Действия"}
                         cellRenderer={row => (
                             <Fragment>
                                 <IconButton aria-label="Редактировать"
                                             component={Link}
                                             to={`/reference/make/${parentId}/child/${row.get("id")}?parentCode=${parentCode}`}
                                             size="small">
                                     <EditIcon/>
                                 </IconButton>
                                 <IconButton aria-label="Удалить"
                                             onClick={() => dispatch(setDeleteConfirmationItem(row))}
                                             size="small">
                                     <DeleteIcon/>
                                 </IconButton>
                             </Fragment>
                         )}/>


            </DataTable>
        </Card>
    )
}
