import React, {useEffect} from "react";
import fp from "lodash/fp";
import {FormControl, InputLabel, MenuItem, OutlinedInput, Select} from "@material-ui/core";

const NOT_SET_OPTION = {};

interface IComponentProps {
    input?: React.InputHTMLAttributes<any>,
    meta?: any,
    variant?: 'standard' | 'outlined' | 'filled',
    margin?: 'none' | 'dense' | 'normal',
    label?: React.ReactNode,
    validate?: Function,
    formatInput?: Function,
    formatOption?: Function,
    formatValue?: Function,
    compareOptions?: Function,
    options: any[]
}

FormCategorySelectComponent.defaultProps = {
    formatOption: fp.identity,
    compareOptions: fp.isEqualWith,
    variant: "outlined",
    margin: "normal"
};

export default function FormCategorySelectComponent(
    {
        input, meta,
        label, margin, formatInput,
        formatOption, compareOptions,
        formatValue, options=[],
        ...custom
    }: IComponentProps) {
    const [labelWidth, setLabelWidth] = React.useState(0);
    const inputLabel = React.useRef(null);

    useEffect(() => {
        setLabelWidth(inputLabel.current.offsetWidth);
    }, []);

    let selectedOption = NOT_SET_OPTION;
    const children: any[] = [];
    options.forEach((option, index) => {
        const text = formatOption ? formatOption(option) : option;

        if (selectedOption === NOT_SET_OPTION && compareOptions(option, input.value)) {
            selectedOption = option;
        }

        children.push(
            <MenuItem
                key={index}
                value={option}
                label={formatInput ? formatInput(option) : text} component={null}>
                {text}
            </MenuItem>
        );
    });

    function onChange(newValue) {
        input.onChange(formatValue ? formatValue(newValue) : newValue);
    }

    if (selectedOption === NOT_SET_OPTION) {
        selectedOption = input.value;
    }

    return (
        <FormControl
            style={{width: "100%"}}
            variant={custom.variant}
            margin={margin}>
            {label && (<InputLabel ref={inputLabel} htmlFor="outlined-age-simple">
                {label}
            </InputLabel>)}
            <Select
                {...custom}
                {...input}
                color={"primary"}
                value={selectedOption}
                error={meta.touched && meta.error}
                onBlur={fp.flow(fp.noop, input.onBlur)}
                onFocus={fp.flow(fp.noop, input.onFocus)}
                onChange={event => onChange(event.target.value)}
                input={<OutlinedInput name="status" labelWidth={labelWidth}/>}
            >
                {children}
            </Select>
        </FormControl>
    )
}