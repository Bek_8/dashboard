import React, {useEffect} from "react";
import {selectCategoryTypes, listAction} from "../../redux/actions/ReferenceActions"
import {useDispatch, useSelector} from "react-redux";
import {Field} from "redux-form";

import {createSelector} from 'reselect'
import {AutoCompleteComponent} from "../../widgets/redux/FormAutoCompleteField";

const getCategoryReducers = createSelector(
    (state: any) => state.reference,
    (stateItem: any) => stateItem.list
);

interface IProps {
    name: string,
    parentCode?: string,
    variant?: 'standard' | 'outlined' | 'filled',
    margin?: 'none' | 'dense' | 'normal',
    label?: React.ReactNode | string,
    validate?: Function,
    required?: boolean,
    fullWidth?: boolean,
    formatInput?: Function,
    formatOption?: Function,
    formatValue?: Function,
    compareOptions?: (option: any, value: any) => boolean,
}

export default function FormServiceAutoCompleteField(props: IProps) {
    const {name, parentCode, ...rest} = props;
    const dispatch = useDispatch()
    const options = useSelector(getCategoryReducers)
    const filter = {parentCode}
    useEffect(() => {
        dispatch(listAction(filter))
    }, [parentCode]);

    return <Field name={name} {...rest}
                  onTextChange={(val: string) => dispatch(listAction({...filter, searchKey: val}))}
                  options={options}
                  component={AutoCompleteComponent}/>
}

