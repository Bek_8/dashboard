import React, {useEffect} from "react";
import {getCommissionList} from "../../redux/actions/CommissionActions"
import {useDispatch, useSelector} from "react-redux";
import {Field} from "redux-form";

import {createSelector} from 'reselect'
import {AutoCompleteComponent} from "../../widgets/redux/FormAutoCompleteField";

const getOptionsReducers = createSelector(
    (state: any) => state.commission,
    (stateItem: any) => stateItem.list
);

interface IProps {
    name: string,
    variant?: 'standard' | 'outlined' | 'filled',
    margin?: 'none' | 'dense' | 'normal',
    label?: React.ReactNode | string,
    validate?: Function,
    required?: boolean,
    fullWidth?: boolean,
    disabled?: boolean,
    formatInput?: Function,
    formatOption?: Function,
    formatValue?: Function,
    compareOptions?: (option: any, value: any) => boolean,
}

export default function FormCommissionAutoCompleteField(props: IProps) {
    const {name, ...rest} = props;
    const dispatch = useDispatch()
    const options = useSelector(getOptionsReducers)
    const filter = {}
    useEffect(() => {
        dispatch(getCommissionList())
    }, []);
    return <Field name={name} {...rest}
                  onTextChange={(val: string) => dispatch(getCommissionList({...filter, searchKey: val}))}
                  options={options}
                      component={AutoCompleteComponent}/>
}

