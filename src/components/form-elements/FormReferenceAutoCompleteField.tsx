import React, {useEffect, useState} from "react";
import { listAction} from "../../redux/actions/ReferenceActions"
import { list} from "../../api/ReferenceApi"
import {useDispatch, useSelector} from "react-redux";
import {Field} from "redux-form";

import {createSelector} from 'reselect'
import {AutoCompleteComponent} from "../../widgets/redux/FormAutoCompleteField";

const getCategoryReducers = createSelector(
    (state: any) => state.reference,
    (stateItem: any) => stateItem.list
);

interface IProps {
    name: string,
    parentCode?: string,
    variant?: 'standard' | 'outlined' | 'filled',
    margin?: 'none' | 'dense' | 'normal',
    label?: React.ReactNode | string,
    validate?: Function,
    required?: boolean,
    fullWidth?: boolean,
    formatInput?: Function,
    formatOption?: Function,
    formatValue?: Function,
    onSelected?: (v: any) => void;
    compareOptions?: (option: any, value: any) => boolean,
}

export default function FormReferenceAutoCompleteField({name, parentCode, ...rest} : IProps) {
    const dispatch = useDispatch()
    const [options, setOptions ] = useState([])
    // const options = useSelector(getCategoryReducers)
    const filter = {parentCode}
    useEffect(() => {

        getOptions(filter)
        // dispatch(listAction(filter))
    }, [parentCode]);
    const getOptions = async (payload)=>{
        const resp:any = await list(payload);
        setOptions(resp.data?.data.list)
    }
    return <Field name={name} {...rest}
                  onTextChange={(val: string) => getOptions({...filter, searchKey: val})}
                  options={options}
                  component={AutoCompleteComponent}/>
}

