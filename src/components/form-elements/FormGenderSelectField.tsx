import React, {useEffect} from "react";
import * as referenceAction from "../../redux/actions/ReferenceActions"
import {useDispatch, useSelector} from "react-redux";
import {Field} from "redux-form";
import {createSelector} from 'reselect'
import FormSelectComponent from "./FormSelectComponent";

const getOptionsyReducers = createSelector(
    (state: any) => state.reference,
    (reference: any) => reference.genders
);


interface IProps {
    name: string,
    variant?: 'standard' | 'outlined' | 'filled',
    margin?: 'none' | 'dense' | 'normal',
    label?: React.ReactNode | string,
    validate?: Function,
    required?: boolean,
    fullWidth?: boolean,
    formatInput?: Function,
    formatOption?: Function,
    formatValue?: Function,
    compareOptions?: Function,
}

export default function FormGenderSelectField(props: IProps) {
    const dispatch = useDispatch();
    const options = useSelector(getOptionsyReducers)

    useEffect(() => {
        if (!options || !options.length) {
            dispatch(referenceAction.getSelectGenders())
        }
    }, []);

    return <Field {...props} options={options} component={FormSelectComponent}/>
}

