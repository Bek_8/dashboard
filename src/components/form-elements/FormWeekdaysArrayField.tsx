import React, {useEffect} from 'react';
import {FieldArray} from "redux-form"
import TimeSlotListField from "../provider-core/TimeSlotListField";
import {useDispatch, useSelector} from "react-redux";
import * as referenceAction from "../../redux/actions/ReferenceActions";
import {createSelector} from "reselect";


interface IProps {
    name: string
}

const getOptionsReducers = createSelector(
    (state: any) => state.reference,
    (reference: any) => reference.weekdays
);


export default function FormWeekdaysArrayField(props: IProps) {
    const dispatch = useDispatch();
    const options = useSelector(getOptionsReducers) || []

    useEffect(() => {
        if (!options.length) {
            dispatch(referenceAction.getSelectWeekdays())
        }
    }, []);
    return options.length && <FieldArray
        name={props.name}
        options={options}
        component={TimeSlotListField}
    /> || null
}
