import React, {useEffect} from "react";
import * as referenceAction from "../../redux/actions/ReferenceActions"
import {useDispatch, useSelector} from "react-redux";
import {Field} from "redux-form";
import {createSelector} from 'reselect'
import FormSelectComponent from "./FormSelectComponent";

const getOptionsReducers = createSelector(
    (state: any) => state.reference,
    (reference: any) => reference.cities
);


interface IProps {
    name: string,
    regionId?: number | string,
    variant?: 'standard' | 'outlined' | 'filled',
    margin?: 'none' | 'dense' | 'normal',
    label?: React.ReactNode | string,
    validate?: Function,
    required?: boolean,
    fullWidth?: boolean,
    formatInput?: Function,
    formatOption?: Function,
    formatValue?: Function,
    compareOptions?: Function,
}

export default function FormDistrictSelectField(props: IProps) {
    const dispatch = useDispatch();
    const options = useSelector(getOptionsReducers);

    useEffect(() => {
        if (props.regionId)
            dispatch(referenceAction.getSelectDistricts(props.regionId))
    }, [props.regionId]);

    return <Field {...props} options={options} component={FormSelectComponent}/>
}

