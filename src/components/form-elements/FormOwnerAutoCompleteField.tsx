import React, {useEffect} from "react";
import {listAction} from "../../redux/actions/GarageUserActions"
import {useDispatch, useSelector} from "react-redux";
import {Field} from "redux-form";

import {createSelector} from 'reselect'
import {AutoCompleteComponent} from "../../widgets/redux/FormAutoCompleteField";

const getOptionsReducers = createSelector(
    (state: any) => state.garageUser,
    (stateItem: any) => stateItem.list
);

interface IProps {
    name: string,
    variant?: 'standard' | 'outlined' | 'filled',
    margin?: 'none' | 'dense' | 'normal',
    label?: React.ReactNode | string,
    validate?: Function,
    required?: boolean,
    fullWidth?: boolean,
    formatInput?: Function,
    formatOption?: Function,
    formatValue?: Function,
    compareOptions?: (option: any, value: any) => boolean,
}

export default function FormOwnerAutoCompleteField(props: IProps) {
    const {name, ...rest} = props;
    const dispatch = useDispatch()
    const options = useSelector(getOptionsReducers)
    const filter = {start:0, limit: 15}
    useEffect(() => {
        dispatch(listAction())
    }, []);
    return <Field name={name} {...rest}
                  onTextChange={(val: string) => dispatch(listAction({...filter, searchKey: val}))}
                  options={options}
                      component={AutoCompleteComponent}/>
}

