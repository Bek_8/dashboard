import React, {useEffect} from "react";
import {selectCategoryTypes} from "../../redux/actions/ReferenceActions"
import {useDispatch, useSelector} from "react-redux";
import {Field} from "redux-form";

import {createSelector} from 'reselect'
import FormCategorySelectComponent from "./FormSelectComponent";
import { getCommissionList } from "../../redux/actions/CommissionActions";

const getCategoryReducers = createSelector(
    (state: any) => state.reference,
    (stateItem: any) => stateItem.categories
);

interface IProps {
    name: string,
    parentCode?: string,
    variant?: 'standard' | 'outlined' | 'filled',
    margin?: 'none' | 'dense' | 'normal',
    label?: React.ReactNode | string,
    validate?: Function,
    required?: boolean,
    fullWidth?: boolean,
    formatInput?: Function,
    formatOption?: Function,
    formatValue?: Function,
    compareOptions?: Function,
}

export default function FormCategorySelectField(props: IProps) {
    const dispatch = useDispatch()
    const options = useSelector(getCategoryReducers)

    useEffect(() => {
        dispatch(getCommissionList())
    }, []);

    return <Field {...props} options={options} component={FormCategorySelectComponent}/>
}

