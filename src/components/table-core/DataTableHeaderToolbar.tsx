import React, {useState} from "react";
import cx from "classnames";
import Toolbar from "@material-ui/core/Toolbar";
import makeStyles from "@material-ui/core/styles/makeStyles";
import Button from "@material-ui/core/Button";
import TextField from "@material-ui/core/TextField";
import FormControl from "@material-ui/core/FormControl";
import InputAdornment from "@material-ui/core/InputAdornment";
import IconButton from "@material-ui/core/IconButton";
import SearchIcon from "@material-ui/icons/Search";
import FlashOnIcon from '@material-ui/icons/FlashOn';
import Tooltip from "@material-ui/core/Tooltip";
import Fab from "@material-ui/core/Fab";
import AddToPhotosIcon from '@material-ui/icons/Add';
import AutorenewIcon from '@material-ui/icons/Autorenew';
import FileCopyIcon from '@material-ui/icons/FileCopyOutlined';
import SaveIcon from '@material-ui/icons/Save';
import PrintIcon from '@material-ui/icons/Print';
import FavoriteIcon from '@material-ui/icons/Favorite';
import {Link} from "react-router-dom";
import AddIcon from "@material-ui/icons/Add";

const useStyles = makeStyles(theme => ({
    toolbar: {
        paddingLeft: "0",
        paddingRight: "0"
    },
    paper: {
        padding: theme.spacing(2),
        textAlign: 'center',
        color: theme.palette.text.secondary,
    },
    textField: {
        paddingTop: "0",
        paddingBottom: "0",
        marginTop: 0,
        marginBottom: 0,
    },
    formControl: {
        marginRight: theme.spacing(1)
    },
    button: {
        padding: "7px 15px",
    },
    addButton: {
        padding: "7px 15px",

        "&:hover svg": {
            transform: 'scale(1.1)',
            transition: '0.3s',
        }
    },
    flashIcon: {
        "&:hover svg": {
            transform: 'scale(1.2)',
            transition: '0.3s',
        }
    },
    extendedIcon: {
        marginRight: theme.spacing(1)
    },
    addIcon: {
        weight: '600',
        marginRight: "10px",

    },
    autoRunIcon: {
        fontSize: '20pt',
        "&:hover svg": {
            transform: 'rotate(360deg)',
            transition: '1s',
        }
    },
    exampleWrapper: {
        maxHeight: 56,

        position: 'fixed',
        right: 20,
        bottom: 20,
        marginTop: theme.spacing(3),
        height: 380,
        "&:hover svg": {
            transform: 'scale(1.2)',
            transition: '0.3s',
        }

    },
    addFab: {
        color: '#FFFFFF',
    },
}));


interface IProps {
    onRefreshClick?: (e: any) => void,
    onResetClick?: (e: any) => void,
    onSearch: (searchKey: string) => void,
    filter: any;
    title?: string;
    addLink?: string;
}

const actions = [
    {icon: <FileCopyIcon/>, name: 'Copy'},
    {icon: <SaveIcon/>, name: 'Save'},
    {icon: <PrintIcon/>, name: 'Print'},
    {icon: <FavoriteIcon/>, name: 'Like'},
];
export default function DataTableHeaderToolbar(props: IProps) {
    const classes = useStyles(props);
    const {filter} = props;
    const [searchKey, setSearchKey] = useState<string>(filter.searchKey || '')


    return (
        <Toolbar className={classes.toolbar}>
            {Boolean(props.addLink) && <FormControl className={classes.formControl}>
                <Tooltip title="Добавить" aria-label="Добавить" placement="top"
                         className={cx(classes.button, classes.addButton)}>
                    <Button
                        variant="outlined"
                        aria-label="Добавить"
                        component={Link}
                        to={props.addLink}
                    >
                        <AddIcon color="primary"/>
                        Добавить
                    </Button>
                </Tooltip>
            </FormControl>}
            {Boolean(props.onSearch) && <FormControl className={classes.formControl}>
                <Tooltip title="Поиск" aria-label="Поиск" placement="top">
                    <TextField
                        id="standard-textarea"
                        margin="dense"
                        label="Поиск"
                        className={classes.textField}
                        variant="outlined"
                        value={searchKey}
                        onChange={(event: any) => setSearchKey(event.target.value)}
                        onKeyPress={event => {
                            if (event.key === 'Enter') {
                                props.onSearch(searchKey)
                            }
                        }}
                        InputProps={{
                            endAdornment: (
                                <InputAdornment position="end">
                                    <IconButton
                                        onClick={() => props.onSearch(searchKey)}
                                        aria-label="toggle password visibility">
                                        <SearchIcon/>
                                    </IconButton>
                                </InputAdornment>
                            )
                        }}/>
                </Tooltip>
            </FormControl>}
            {Boolean(props.onRefreshClick) && <FormControl className={classes.formControl}>
                <Tooltip title="Обновить" aria-label="Обновить" placement="top">
                    <Button
                        variant="outlined"
                        color="primary"
                        onClick={props.onRefreshClick}
                        className={cx(classes.button, classes.autoRunIcon)}
                    >
                        <AutorenewIcon/>
                    </Button>
                </Tooltip>
            </FormControl>}
            {Boolean(props.onResetClick) && <FormControl className={classes.formControl}>
                <Tooltip title="Сбросить фильтр" aria-label="Сбросить фильтр" placement="top">
                    <Button
                        variant="outlined"
                        className={cx(classes.button, classes.flashIcon)}
                        color="secondary"
                        onClick={(e) => {
                            props.onResetClick(e);
                            setSearchKey('')
                        }}>
                        <FlashOnIcon/>
                    </Button>
                </Tooltip>
            </FormControl>}

            {Boolean(props.addLink) && <span className={classes.exampleWrapper}>
                <Fab
                    component={Link}
                    to={props.addLink}
                    size="medium"
                    aria-label="add"
                    className={classes.addFab}
                    color="primary"
                >
                    <AddToPhotosIcon/>
                </Fab>
           </span>}
        </Toolbar>

    )
}