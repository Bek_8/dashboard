import React, {Fragment, ReactElement, useState} from "react";
import CardComponent from "../../widgets/CardComponent";
import ListPanelComponent from "../../widgets/listPanel/ListPanelComponent";
import Immutable from "immutable";
import {useDispatch, useSelector} from "react-redux";
import * as providerActions from "../../redux/actions/ProviderActions";
import Button from "@material-ui/core/Button";
import {push} from "react-router-redux";

import SweetAlert from "react-bootstrap-sweetalert";
import IconButton from "@material-ui/core/IconButton";
import DeleteIcon from "@material-ui/icons/Delete";
import AddIcon from "@material-ui/icons/Add";
import EditIcon from "@material-ui/icons/Edit";
import DepositIcon from "@material-ui/icons/AccountBalanceWallet";
import ReportIcon from "@material-ui/icons/FileCopy";
import {ListingObj} from "../../types/ApiOjbects";
import {Link} from "react-router-dom";
import {setDeleteConfirmationItem} from "../../redux/actions/MessageActions";

interface IProps {
    getListAction: (any) => (any) => void,
    deleteItemAction: (any) => (any) => void,
    listing: ListingObj<any>
    columns: IColumn[]
    addButtonComponent?: React.ReactElement,
    loader: boolean
}

export interface IColumn {
    columnCode?: string,
    columnName?: string,
    sortable?: boolean,
    width?: string|number,
    align?: string,
    value: (any) => string | React.ReactNode | void
}

export default function ListingTable(props: IProps) {
    const dispatch = useDispatch()
    const deletionItem = useSelector((state: any) => state.message.deletionItem)

    const getRequestProvider = (limit, start, searchKey, sortField, sortOrder, key) => {
        dispatch(props.getListAction({start, limit, searchKey, sortField, sortOrder}));
    };

    function deleteItem(itemToDelete) {
        if (!itemToDelete) {
            return;
        }
        dispatch(setDeleteConfirmationItem(null));
        dispatch(props.deleteItemAction(itemToDelete.get("id")));
    }

    function renderSweetAlertBlock() {
        if (deletionItem) {
            return (
                <SweetAlert
                    showCancel
                    title="Внимание"
                    type="warning"
                    confirmBtnBsStyle="danger"
                    cancelBtnBsStyle="default"
                    cancelBtnCssClass="MuiButtonBase-root MuiButton-root MuiButton-contained MuiButton-containedSecondary"
                    confirmBtnCssClass="MuiButtonBase-root MuiButton-root jss1086 MuiButton-contained MuiButton-containedPrimary"
                    onConfirm={() => deleteItem(deletionItem)}
                    onCancel={() => dispatch(setDeleteConfirmationItem(null))}>
                    {`Вы действительно хотите удалить эту запись?`}
                </SweetAlert>
            );
        }
        return null;
    }

    return (
        <>
            {renderSweetAlertBlock()}
            <ListPanelComponent
                showDeleteModal={deletionItem != null}
                loader={props.loader}
                list={props.listing.list}
                count={props.listing.count}
                addNewHandler={props.addButtonComponent}
                requestProvider={getRequestProvider}
                columnsConfig={Immutable.fromJS(props.columns)}
            />
        </>
    )
}
