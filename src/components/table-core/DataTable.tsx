import React from 'react';
import {ListingFilter, ListingObj} from "../../types/ApiOjbects";
import {createStyles, makeStyles, Table, TableBody, TableHead, TableRow, Theme} from "@material-ui/core";
import TableSortLabel from "@material-ui/core/TableSortLabel";
import TableCell from "@material-ui/core/TableCell";
import Immutable from "immutable";
import withStyles from "@material-ui/core/styles/withStyles";
import TablePagination from "@material-ui/core/TablePagination";
import {DEFAULT_FILTER, PAGE_ROWS_LIMIT_LIST} from "../../constants/Constants";
import EnhancedTableToolbar from "./EnhancedTableToolbar";
import {filter} from "minimatch";
import SortOrder from "../../types/SortOrder";
import {setDeleteConfirmationItem} from "../../redux/actions/MessageActions";
import {useDispatch, useSelector} from "react-redux";
import SweetAlert from "react-bootstrap-sweetalert";
import DataTableHeaderToolbar from "./DataTableHeaderToolbar";
import Loader from "../common-core/Loader";
import fp from "lodash/fp"

interface IProps<T> {
    listing: ListingObj<T[]>,
    loader?: boolean,
    children: React.ReactChild[],
    title?: string,
    filter: ListingFilter,
    addLink?: string,
    onFilterChange?: (f: ListingFilter) => void,
    showToolbar?: boolean;
    showPagination?: boolean;
    onDeleteItem?: any,
    getList?: (f: any) => void,
}

const useStyles = makeStyles((theme: Theme) => createStyles({
    table: {
        minWidth: 750,
    },
}));
export default function DataTable<T>(props: IProps<T>) {
    const classes = useStyles(props);
    const dispatch = useDispatch();
    // const [perPage, setPerPage] = useState<number>()
    const {filter, title, onFilterChange, showToolbar=true, showPagination=true} = props;
    const limit = Number(filter.limit)
    const itemForDeletion = useSelector((state: any) => state.message.deletionItem)
    const currentPage = Math.floor(Number(props.filter.start) / limit);
    return (
        <>
            {showToolbar && <DataTableHeaderToolbar
                filter={filter}
                addLink={props.addLink}
                onRefreshClick={() => props.getList(filter)}
                onResetClick={() => onFilterChange(DEFAULT_FILTER)}
                onSearch={(searchKey) => onFilterChange({...filter, searchKey})}
            />}

            <Table className={classes.table}>
                <DataTableHead
                    {...props}
                    filter={filter}
                />
                <TableBody>
                    {Boolean(props.loader) && <TableRow>
                        <TableCell colSpan={React.Children.count(props.children)}>
                            <Loader size={32} thickness={4} variant="indeterminate"/>
                        </TableCell>
                    </TableRow>}
                    {Immutable.fromJS(props.listing.list)
                        .map((listItem: Immutable.Map<string, any>, index: number) =>
                            <DataTableBody
                                key={index}
                                {...props}
                                dataRow={listItem}/>
                        )}
                </TableBody>
            </Table>
            {showPagination && <TablePagination
                rowsPerPageOptions={PAGE_ROWS_LIMIT_LIST}
                component="div"
                count={props.listing.count || 0}
                rowsPerPage={limit}
                page={currentPage}
                onChangePage={(e: any, page: number) =>
                    props.onFilterChange({
                        ...filter,
                        start: limit * page
                    })}
                onChangeRowsPerPage={
                    (event: React.ChangeEvent<HTMLInputElement>) => {
                        const newLimit: number = Number(event.target.value)
                        props.onFilterChange({
                            ...filter,
                            start: 0,
                            limit: newLimit
                        })
                    }}
            />}
            {itemForDeletion && Boolean(props.onDeleteItem) && <SweetAlert
                showCancel
                title="Внимание"
                type="warning"
                confirmBtnBsStyle="danger"
                cancelBtnBsStyle="default"
                cancelBtnCssClass="MuiButtonBase-root MuiButton-root MuiButton-contained MuiButton-containedSecondary"
                confirmBtnCssClass="MuiButtonBase-root MuiButton-root jss1086 MuiButton-contained MuiButton-containedPrimary"
                onConfirm={() => {
                    Promise.resolve(props.onDeleteItem(itemForDeletion.get("id")))
                        .then(() => {
                            dispatch(setDeleteConfirmationItem(null))
                            props.getList(filter);
                        })
                }}
                onCancel={() => dispatch(setDeleteConfirmationItem(null))}>
                {`Вы действительно хотите удалить эту запись?`}
            </SweetAlert>}
        </>
    );
}


interface ITableHeadProps {
    filter: ListingFilter;
    children: React.ReactChild[];
    onFilterChange?: (f: any) => void
}

const StyledTableCell = withStyles(theme => ({
    head: {
        backgroundColor: theme.palette.primary.main,
        color: theme.palette.common.white,
        fontSize: "0.8rem"
    },
}))(TableCell);

function DataTableHead(props: ITableHeadProps) {

    return (
        <TableHead>
            <TableRow>
                {React.Children.map(props.children, (child: any, index: number) => (
                    <StyledTableCell
                        key={index}
                        style={{minWidth: child.props.width}}
                        align={child.props.align}
                        padding={child.props.padding || 'default'}
                    >
                        {child.props.sortable ?
                            <TableSortLabel
                                active={props.filter.sortField === child.props.fieldKey}
                                direction={fp.toLower(props.filter.sortType || SortOrder.ASC)}
                                onClick={() => props.onFilterChange({
                                    ...filter,
                                    sortField: child.props.fieldKey,
                                    sortType: props.filter.sortType === SortOrder.DESC ? SortOrder.ASC : SortOrder.DESC,
                                })}
                            >
                                {child.props.label}
                            </TableSortLabel> :
                            child.props.label
                        }
                    </StyledTableCell>
                ))}
            </TableRow>
        </TableHead>
    )
}

interface ITableBodyProps {
    children: React.ReactChild[],
    dataRow: any
}

function DataTableBody(props: ITableBodyProps) {
    return (
        <TableRow>
            {React.Children.map(props.children, (child: any, index: number) => (
                <TableCell
                    key={index}
                    align={child.align}
                    padding={child.props.padding || 'default'}
                >
                    {child.props.cellRenderer(props.dataRow)}
                </TableCell>
            ))}
        </TableRow>
    )
}

interface DataTableColProps {
    label: string | React.ReactElement,
    fieldKey?: string | null,
    searchKey?: string | null,
    sortable?: boolean,
    padding?: 'none' | 'default' | number,
    align?: "left" | "right" | "center",
    width?: number | string,
    cellRenderer: (row: Immutable.Map<string, any>) => React.ReactElement | string | null
}

export function DataCol(props: DataTableColProps) {
    return null;
}
