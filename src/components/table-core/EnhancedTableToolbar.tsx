import React, {useRef, useState} from "react"
import {createStyles, IconButton, makeStyles, TextField, Theme, Toolbar, Tooltip, Typography} from "@material-ui/core";
import clsx from "clsx"
import {lighten} from "@material-ui/core/styles";
import ResetIcon from '@material-ui/icons/NotInterested';
import RefreshIcon from '@material-ui/icons/LoopRounded';
import FilterListIcon from '@material-ui/icons/FilterList';
import AddIcon from '@material-ui/icons/Add';
import {Link} from "react-router-dom";
import Grid from "@material-ui/core/Grid";
import SearchIcon from '@material-ui/icons/Search';
import InputAdornment from "@material-ui/core/InputAdornment";
import  Box from '@material-ui/core/Box';


const useToolbarStyles = makeStyles((theme: Theme) =>
    createStyles({
        root: {
            paddingLeft: theme.spacing(2),
            paddingRight: theme.spacing(1),
        },
        highlight:
            theme.palette.type === 'light'
                ? {
                    color: theme.palette.secondary.main,
                    backgroundColor: lighten(theme.palette.secondary.light, 0.85),
                }
                : {
                    color: theme.palette.text.primary,
                    backgroundColor: theme.palette.secondary.dark,
                },
        title: {
            width: 100,
        },
        searchIcon:{
            marginLeft: 20,
            '&:hover': {
                backgroundColor: '#ffffff',
            },
        },
        iconButton: {
            '&:hover': {
                backgroundColor: '#ffffff',
            },
            '&:hover svg':{
                color: 'rgba(0,0,0,0.7)',
                transition: '0.3s',
            },
        },
        iconRefrash: {
            '&:hover': {
                backgroundColor: '#ffffff',
            },
            '&:hover svg': {
                transform: 'rotate(360deg)',
                color: 'rgba(0,0,0,0.7)',
                transition: '0.5s',
            },
        },
    }),
);


interface EnhancedTableToolbarProps {
    numSelected: number;
    filter: any;
    title?: string;
    addLink?: string;
    onFilterClick?: () => void;
    onRefreshClick?: () => void;
    onResetClick?: () => void;
    onSearch?: (searchKey: string) => void;
    searchList?: (e: any) => void;
    filterKey?: string;
}

const EnhancedTableToolbar = (props: EnhancedTableToolbarProps) => {
    const classes = useToolbarStyles(props);
    const ref = useRef(null)
    const {numSelected, title, filter} = props;
    const [searchKey, setSearchKey] = useState(filter.searchKey)
    return (
        <Toolbar
            className={clsx(classes.root, {
                [classes.highlight]: numSelected > 0,
            })}>
                <Typography className={classes.title} color="inherit" variant="subtitle1">
                    {title}
                </Typography>


                    <TextField

                        id="search"
                        placeholder="Поиск"
                        type="search"
                        fullWidth={true}
                        value={searchKey}
                        onChange={(e: React.ChangeEvent<HTMLInputElement>) => setSearchKey(e.target.value)}
                        onKeyPress={event => {
                            if (event.key === 'Enter') {
                                props.onSearch(searchKey)
                            }
                        }}
                        InputProps={{
                            disableUnderline: true,
                            inputRef: ref,
                            startAdornment: (
                                <InputAdornment position="start">
                                    <IconButton
                                        disableRipple={true}
                                        className={classes.searchIcon}
                                        onClick={() => {
                                            ref.current.focus()
                                        }}
                                        aria-label="toggle password visibility">
                                        <SearchIcon/>
                                    </IconButton>
                                </InputAdornment>
                            )
                        }}
                    />
               {props.onRefreshClick &&  <Tooltip title="Refresh">
                        <IconButton aria-label="refresh" className={classes.iconRefrash}
                                    onClick={props.onRefreshClick}>
                            <RefreshIcon className={classes.iconButton}/>
                        </IconButton>
                    </Tooltip>}
               {props.onResetClick &&  <Tooltip title="Reset">
                        <IconButton aria-label="reset"
                                    className={classes.iconButton}
                                    onClick={e => {
                                        props.onResetClick();
                                        setSearchKey('')
                                    }}>
                            <ResetIcon/>
                        </IconButton>
                    </Tooltip>}
                    {props.onFilterClick &&
                    <Tooltip title="Filter">
                        <IconButton aria-label="filter"
                                    className={classes.iconButton}
                                    onClick={props.onFilterClick}>
                            <FilterListIcon/>
                        </IconButton>
                    </Tooltip>}

                    {props.addLink &&
                    <Tooltip title="Filter">
                        <IconButton aria-label="filter"
                                    className={classes.iconButton}
                                    component={Link}
                                    to={props.addLink}>
                            <AddIcon/>
                        </IconButton>
                    </Tooltip>}

        </Toolbar>
    );
};
export default EnhancedTableToolbar