import React, {useState} from "react";
import CardComponent from "../../widgets/CardComponent";
import makeStyles from "@material-ui/core/styles/makeStyles";
import { reduxForm, FieldArray, formValueSelector, change } from "redux-form";
import Grid from "@material-ui/core/Grid";
import FormTextInputField from "../../widgets/redux/FormTextInputField";
import { isEmpty, isValidDate } from "../../widgets/redux/Validator";
import Button from "@material-ui/core/Button";
import FormDatePickerField from "../../widgets/redux/FormDatePickerField";
import FormSelectField from "../../widgets/redux/FormSelectField";
import FormCategorySelectField from "../form-elements/FormCategorySelectField";
import TimeSlotListField from "../provider-core/TimeSlotListField";
import fp from "lodash/fp";
import ExpandMoreIcon from "@material-ui/icons/ExpandMore";
import FormCheckboxField from "../../widgets/redux/FormCheckboxField";
import { STATUS_OPTIONS } from "../../constants/Constants";
import {
  Box,
  Checkbox,
  Divider,
  ExpansionPanel,
  ExpansionPanelDetails,
  ExpansionPanelSummary,
  FormControlLabel,
  Typography,
} from "@material-ui/core";
import {useDispatch, useSelector} from "react-redux";
import CategoryServicesMultiSelectField from "./CategoryServicesFormArray";
import { convertCodeToTitle } from "../../utils/FormatUtils";
import { createSelector } from "reselect";
import InputAdornment from "@material-ui/core/InputAdornment";
import IconButton from "@material-ui/core/IconButton";
import SearchIcon from "@material-ui/core/SvgIcon/SvgIcon";
import TechnicianExtraPaymentFieldsArray from "./TechnicianExtraPaymentFieldsArray";
import { required } from "../../utils/reduxFormatUtils";
import { useParams } from "react-router-dom";
import {ImageInput} from "../../widgets/redux/FileInputField";
import {Simulate} from "react-dom/test-utils";
import FormCommissionAutoCompleteField from "../form-elements/FormCommissionAutoCompleteField";

const useStyles = makeStyles((theme) => ({
  headerColor: {
    backgroundColor: theme.palette.primary.main,
    display: "flex",
    justifyContent: "space-between",
    alignItems: "center",
    padding: theme.spacing(0, 2),
  },
  form: {
    width: "100%", // Fix IE 11 issue.
    marginTop: theme.spacing(1),
  },
  submit: {
    margin: theme.spacing(0, 2, 0, 0),
  },
  percentTable: {
    width: "100%",
  },
}));

interface IProps {
  handleBack: () => void;
  pristine?: boolean;
  item?: any;
  submitting?: boolean;
  submitForm: (d: any) => void;
  handleSubmit?: any;
}
const selector = formValueSelector("TechnicianSaveForm");

function TechnicianSaveForm(props: IProps) {
  const sosAvailable = useSelector((state) => selector(state, "sosAvailable"));
  const categoryId = useSelector((state) => selector(state, "categoryId"));
  // const hasCommission = useSelector((state) => selector(state, "hasCommission"));
const dispatch = useDispatch();
  const [image, setImage]=useState(null)
  // const providerItem = useSelector(createSelector(
  //     (state: any) => state.provider,
  //     (provider: any) => provider.item
  // ));
  const { providerId } = useParams();
  const selectedServices =
    useSelector((state) => selector(state, "services")) || [];
  const classes = useStyles(props);
  const { pristine, submitting, handleSubmit, submitForm } = props;

  return (
    <CardComponent title={true ? `Редактирование` : "Новая марка"}>
      <form
        className={classes.form}
        noValidate
        onSubmit={handleSubmit(submitForm)}
      >
        <Grid container alignItems="flex-start" spacing={3}>
          <Grid item xs={12} md={3}>
            <FormTextInputField
              name="firstName"
              validate={required}
              fullWidth={true}
              InputProps={
                {
                  fullWidth: true,
                  variant: "outlined",
                  label: "Имя",
                } as any
              }
            />
          </Grid>
          <Grid item xs={12} md={3}>
            <FormTextInputField
              name="lastName"
              validate={required}
              fullWidth={true}
              InputProps={
                {
                  fullWidth: true,
                  variant: "outlined",
                  label: "Фамилия",
                } as any
              }
            />
          </Grid>
          <Grid item xs={12} md={3}>
            <FormTextInputField
              name="phone"
              fullWidth={true}
              validate={required}
              InputProps={
                {
                  fullWidth: true,
                  variant: "outlined",
                  label: "Phone number",
                } as any
              }
            />
          </Grid>
          <Grid item xs={12} md={3}>
            <FormSelectField
              options={STATUS_OPTIONS}
              name="status"
              label="Статус"
              validate={required}
              fullWidth={true}
              compareOptions={(option, value) => option.code === value}
              formatOption={(v) => v.name}
              formatValue={(v) => v.code}
            />
          </Grid>
        </Grid>
        <Box p={"30px 0"}>
          <Grid container alignItems="flex-start" spacing={10}>
            <Grid item xs={12} md={6}>
              <FormCategorySelectField
                name="categoryId"
                label="Категория"
                variant="outlined"
                validate={required}
                parentCode="AUTO_SERVICE"
                compareOptions={(option, value) => option.id === value}
                fullWidth={true}
                formatOption={(v) => v.name}
                formatValue={(v) => v.id}
              />
              <CategoryServicesMultiSelectField
                name="services"
                categoryId={categoryId}
                providerId={providerId}
                selectedServices={selectedServices}
              />

                <ImageInput imagePath={image ? URL.createObjectURL(image) : props.item?.imageUrl }
                            onChange={e=>{
                              console.log("file ", e.target, e.target.files, e)
                              const file = e.target.files[0]
                              setImage(file);
                              dispatch(change('TechnicianSaveForm', 'image', file))
                            }}
                />
                <Box style={{minWidth: '250px', float: 'right'}}>
              {/*<FormCheckboxField*/}
              {/*    name="hasCommission"*/}
              {/*    fullWidth={true}*/}
              {/*    label="Комиссия"*/}
              {/*    InputProps={*/}
              {/*      {*/}
              {/*        fullWidth: true,*/}
              {/*        variant: "outlined",*/}
              {/*      } as any*/}
              {/*    }*/}
              {/*/>*/}
                <FormCommissionAutoCompleteField
                    name="commission"
                    label="Commission"
                    variant="outlined"
                    compareOptions={(option, value) => {
                      console.log("Compare ---", option, value)
                      return option?.id === value.id}}
                    formatOption={(v) => {console.log('formatOption -- ',v); return v?.name||''}}
                />
                </Box>
            </Grid>
            <Grid item xs={12} md={6}>
              <FieldArray
                name="timeslots"
                variant="outlined"
                component={TimeSlotListField}
              />
            </Grid>
          </Grid>
        </Box>
        <Divider />
        <Grid container alignItems="flex-start" spacing={10}>
          <Grid item xs={12} sm={12}>
            <FormCheckboxField
              name="sosAvailable"
              fullWidth={true}
              label="Доступен для SOS высовов"
              InputProps={
                {
                  fullWidth: true,
                  variant: "outlined",
                } as any
              }
            />

            {sosAvailable && (
              <Grid container alignItems="flex-start" spacing={3}>
                <Grid item xs={12} md={6}>
                  <table className={classes.percentTable}>
                    <tr>
                      <th>&nbsp;</th>
                      <th>Percent</th>
                    </tr>
                    <tr>
                      <td>Urgent Percent</td>
                      <td>
                        <FormTextInputField
                          name="urgentPercent"
                          InputProps={
                            {
                              variant: "outlined",
                              label: "Urgent Percent",
                              type: "number",
                              InputProps: {
                                endAdornment: (
                                  <InputAdornment position="end">
                                    %
                                  </InputAdornment>
                                ),
                              },
                            } as any
                          }
                        />
                      </td>
                    </tr>
                    <tr>
                      <td>Day Percent</td>
                      <td>
                        <FormTextInputField
                          name="dayPercent"
                          InputProps={
                            {
                              variant: "outlined",
                              label: "Day Percent",
                              type: "number",
                              InputProps: {
                                endAdornment: (
                                  <InputAdornment position="end">
                                    %
                                  </InputAdornment>
                                ),
                              },
                            } as any
                          }
                        />
                      </td>
                    </tr>
                    <tr>
                      <td>Week Percent</td>
                      <td>
                        <FormTextInputField
                          name="weekPercent"
                          InputProps={
                            {
                              variant: "outlined",
                              label: "Week Percent",
                              type: "number",
                              InputProps: {
                                endAdornment: (
                                  <InputAdornment position="end">
                                    %
                                  </InputAdornment>
                                ),
                              },
                            } as any
                          }
                          endAdornment={
                            <InputAdornment position="end">%</InputAdornment>
                          }
                        />
                      </td>
                    </tr>
                    <tr>
                      <td>With selected</td>
                      <td>
                        <FormTextInputField
                          name="customPercent"
                          InputProps={
                            {
                              variant: "outlined",
                              label: "With selected",
                              type: "number",
                              InputProps: {
                                endAdornment: (
                                  <InputAdornment position="end">
                                    %
                                  </InputAdornment>
                                ),
                              },
                            } as any
                          }
                        />
                      </td>
                    </tr>
                  </table>
                </Grid>

                <Grid item xs={12} md={5}>
                  <FieldArray
                    name="additionalCosts"
                    component={TechnicianExtraPaymentFieldsArray}
                  />
                </Grid>
              </Grid>
            )}
          </Grid>
        </Grid>

        <Grid container alignItems="flex-start" spacing={3}>
          <Grid item>
            <Button
              disabled={pristine || submitting}
              className={classes.submit}
              type="submit"
              color="primary"
              variant="contained"
            >
              Сохранить
            </Button>
            <Button
              disabled={submitting}
              className={classes.submit}
              onClick={props.handleBack}
              color="secondary"
              variant="outlined"
            >
              Закрыть
            </Button>
          </Grid>
        </Grid>
      </form>
    </CardComponent>
  );
}

export default reduxForm({
  form: "TechnicianSaveForm",
  enableReinitialize: true,
})(TechnicianSaveForm);
