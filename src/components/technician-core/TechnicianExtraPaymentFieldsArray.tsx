import React, {Component, useEffect} from 'react';
import {
    makeStyles,
    Theme,
    Button,
    IconButton
} from "@material-ui/core";
import FormTextInputField from "../../widgets/redux/FormTextInputField";
import InputAdornment from "@material-ui/core/InputAdornment";
import AddIcon from "@material-ui/icons/Add";
import DeleteIcon from "@material-ui/icons/Delete";

const useStyles = makeStyles((theme: Theme) => ({
    root: {
        width: '100%'
    },
    addButton: {
        textTransform: "none",
        textDecoration: "underline",
        textDecorationStyle: "dotted",
        fontWeight: "bold"
    }
}));

export default function TechnicianExtraPaymentFieldsArray(props) {
    const {fields = [{}], services, selectedServices, ...rest} = props;
    console.log("slot field = ", rest, services, fields)
    const classes = useStyles(props)
    return (
        <table className={classes.root}>
            <tr>
                <td>Distance</td>
                <td>Percent</td>
                <td style={{minWidth: 60}}>&nbsp;</td>
            </tr>
            {fields.map((field, index) =>
                <DistanceAndPercentageRow
                    key={index}
                    fields={fields}
                    index={index}
                />
            )}
            <tr>
                <td>
                    <Button
                        color="primary"
                        className={classes.addButton}
                        onClick={() => fields.push({})}>
                        + Добавить
                    </Button>
                </td>
            </tr>
        </table>
    );
}

function DistanceAndPercentageRow({fields, index}) {
    return (
        <tr>
            <td>
                <FormTextInputField
                    name={`${fields.name}[${index}].distance`}
                    fullWidth={true}
                    placeholder="Расстояние"
                    InputProps={{
                        fullWidth: true,
                        variant: "outlined",
                        type: "number",
                        InputProps: {
                            endAdornment: (
                                <InputAdornment position="end">
                                    KM
                                </InputAdornment>
                            )
                        }
                    } as any}
                />
            </td>
            <td>
                <FormTextInputField
                    name={`${fields.name}[${index}].percentage`}
                    placeholder="Введите"
                    fullWidth={true}
                    InputProps={{
                        fullWidth: true,
                        variant: "outlined",
                        type: "number",
                        InputProps: {
                            endAdornment: (
                                <InputAdornment position="end">
                                    %
                                </InputAdornment>
                            )
                        }
                    } as any}
                />
            </td>
            <td>
                {index > 0 && <IconButton
                    aria-label="delete"
                    onClick={() => fields.remove(index)}
                    color="secondary"
                >
                    <DeleteIcon/>
                </IconButton>}
            </td>
        </tr>
    )
}