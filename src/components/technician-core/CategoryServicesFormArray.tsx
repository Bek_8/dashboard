import React, { Component, useEffect } from "react";
import { useDispatch, useSelector } from "react-redux";
import { FieldArray } from "redux-form";
import {
  Box,
  makeStyles,
  MenuItem,
  Theme,
  MenuList,
  Checkbox,
  FormControlLabel,
} from "@material-ui/core";
import { getServicesByCategory } from "../../redux/actions/TechnicianActions";
import SelectItem from "../../types/SelectItem";

const useStyles = makeStyles((theme: Theme) => ({
  root: {
    maxHeight: 300,
    overflow: "auto",
    minHeight: 200,
    border: "1px solid rgba(0, 0, 0, 0.23)",
    borderRadius: theme.shape.borderRadius,
  },
}));

function CategoryServicesFormArray(props) {
  const { fields, services = [], selectedServices = [], ...rest } = props;
  console.log("slot field = ", rest, services, fields);
  const classes = useStyles(props);
  return (
    <Box className={classes.root}>
      <MenuList>
        {services.map((service: any, index) => {
          const findIndex = selectedServices.indexOf(service.id);
          const included = findIndex > -1;
          return (
            <MenuItem key={service.id}>
              <FormControlLabel
                control={
                  <Checkbox
                    checked={included}
                    onChange={() =>
                      included
                        ? fields.remove(findIndex)
                        : fields.push(service.id)
                    }
                  />
                }
                label={service.categoryName}
              />
            </MenuItem>
          );
        })}
      </MenuList>
    </Box>
  );
}

interface IProps {
  name: string;
  categoryId: number | string;
  providerId: number | string;
  selectedServices: any[];
}

export default function CategoryServicesMultiSelectField(props: IProps) {
  const dispatch = useDispatch();
  const services = useSelector(
    (state: any) => state.technician.serviceListByCategory
  );

  useEffect(() => {
    if (props.categoryId) {
      dispatch(getServicesByCategory(props.categoryId, props.providerId));
    }
  }, [props.categoryId]);

  return (
    <FieldArray
      name={props.name}
      services={services}
      selectedServices={props.selectedServices}
      component={CategoryServicesFormArray}
    />
  );
}
