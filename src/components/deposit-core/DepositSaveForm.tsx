import React from "react";
import CardComponent from "../../widgets/CardComponent";
import makeStyles from "@material-ui/core/styles/makeStyles";
import { reduxForm } from "redux-form";
import Grid from "@material-ui/core/Grid";
import FormTextInputField from "../../widgets/redux/FormTextInputField";
import { isEmpty, isValidDate } from "../../widgets/redux/Validator";
import Button from "@material-ui/core/Button";
import FormDatePickerField from "../../widgets/redux/FormDatePickerField";

const useStyles = makeStyles((theme) => ({
  headerColor: {
    backgroundColor: theme.palette.primary.main,
    display: "flex",
    justifyContent: "space-between",
    alignItems: "center",
    padding: theme.spacing(0, 2),
  },
  form: {
    width: "100%", // Fix IE 11 issue.
    marginTop: theme.spacing(1),
  },
  submit: {
    margin: theme.spacing(0, 2, 0, 0),
  },
}));

interface IProps {
  handleBack: () => void;
  pristine?: boolean;
  submitting?: boolean;
  submitForm: (d: any) => void;
  handleSubmit?: any;
}

function DepositSaveForm(props: IProps) {
  const classes = useStyles(props);
  const { pristine, submitting, handleSubmit, submitForm } = props;

  return (
    <CardComponent title={true ? `Редактирование` : "Новая марка"}>
      <form
        className={classes.form}
        noValidate
        onSubmit={handleSubmit(submitForm)}
      >
        <Grid container alignItems="flex-start" spacing={3}>
          <Grid item xs={12} sm={4}>
            <FormTextInputField
              validate={isEmpty}
              name="amount"
              InputProps={
                {
                  fullWidth: true,
                  required: true,
                  label: "Summa",
                  variant: "outlined",
                } as any
              }
            />
            <FormTextInputField
              validate={isEmpty}
              name="providerId"
              InputProps={
                {
                  fullWidth: true,
                  label: "provier id",
                  required: true,
                  style: { display: "none" },
                  variant: "outlined",
                } as any
              }
            />
          </Grid>
          <Grid item xs={12} sm={12}>
            <Button
              disabled={pristine || submitting}
              className={classes.submit}
              type="submit"
              color="primary"
              variant="contained"
            >
              Сохранить
            </Button>
            <Button
              disabled={submitting}
              className={classes.submit}
              onClick={props.handleBack}
              color="secondary"
              variant="outlined"
            >
              Закрыть
            </Button>
          </Grid>
        </Grid>
      </form>
    </CardComponent>
  );
}

export default reduxForm({
  form: "DepositSaveForm",
  enableReinitialize: true,
})(DepositSaveForm);
