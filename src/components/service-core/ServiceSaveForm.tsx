import React from "react";
import CardComponent from "../../widgets/CardComponent";
import makeStyles from "@material-ui/core/styles/makeStyles";
import {reduxForm, formValueSelector} from "redux-form";
import Grid from "@material-ui/core/Grid";
import FormTextInputField from "../../widgets/redux/FormTextInputField";
import {isEmpty, isValidDate} from "../../widgets/redux/Validator";
import Button from "@material-ui/core/Button";
import FormDatePickerField from "../../widgets/redux/FormDatePickerField";
import FormSelectField from "../../widgets/redux/FormSelectField";
import FormCategorySelectField from "../form-elements/FormCategorySelectField";
import fp from "lodash/fp"
import {convertCodeToTitle} from "../../utils/FormatUtils";
import {createSelector} from "reselect";
import {useSelector} from "react-redux";
import FormAutoCompleteField from "../../widgets/redux/FormAutoCompleteField";
import FormServiceAutoCompleteField from "../form-elements/FormServiceAutoCompleteField";
import FormStatusSelectField from "../form-elements/FormStatusSelectField";

const useStyles = makeStyles(theme => ({
    headerColor: {
        backgroundColor: theme.palette.primary.main,
        display: "flex",
        justifyContent: "space-between",
        alignItems: "center",
        padding: theme.spacing(0, 2)
    },
    form: {
        width: "100%", // Fix IE 11 issue.
        marginTop: theme.spacing(1)
    },
    submit: {
        margin: theme.spacing(0, 2, 0, 0),
    },
}));

interface IProps {
    handleBack: () => void,
    pristine?: boolean,
    submitting?: boolean,
    submitForm: (d: any) => void
    handleSubmit?: any
};

const selector = formValueSelector('ServiceSaveForm')
const hasId = fp.has("id")

function ServiceSaveForm(props: IProps) {
    const classes = useStyles(props);
    const parentCategory = useSelector(state => selector(state, 'parentCategory')) || null;
    const providerItem = useSelector(createSelector(
        (state: any) => state.provider,
        (provider: any) => provider.item
    ));
    const {
        pristine,
        submitting,
        handleSubmit,
        submitForm,
    } = props;


    return (
            <form className={classes.form} noValidate onSubmit={handleSubmit(submitForm)}>
                <Grid
                    container
                    alignItems="flex-start"
                    spacing={3}>
                    {hasId(providerItem) && <Grid item xs={12} md={6}>
                        <FormCategorySelectField
                            name="parentCategory"
                            label="Категория"
                            parentCode={providerItem.categoryCode}
                            variant="outlined"
                            fullWidth={true}
                            compareOptions={(option, value) => (option.code === value.code)}
                            formatOption={v => v.name}
                        />
                        <FormTextInputField
                            name="providerId"
                            fullWidth={true}
                            InputProps={{
                                fullWidth: true,
                                variant: "outlined",
                                style: {display: "none"}
                            } as any}
                        />
                    </Grid>}
                    {Boolean(parentCategory) && <Grid item xs={12} md={6}>
                        <FormServiceAutoCompleteField
                            name="category"
                            parentCode={parentCategory.code}
                            label="Сервис"
                            compareOptions={(option, value) => (option.id == value.id)}
                            formatOption={v => v.nameRu}
                            fullWidth={true}
                        />
                    </Grid>}
                    <Grid item>
                        <Button
                            disabled={pristine || submitting}
                            className={classes.submit}
                            type="submit"
                            color="primary"
                            variant="contained">
                            Сохранить
                        </Button>
                        <Button
                            disabled={submitting}
                            className={classes.submit}
                            onClick={props.handleBack}
                            color="secondary"
                            variant="outlined">
                            Закрыть
                        </Button>
                    </Grid>

                </Grid>
            </form>
    )
}

export default reduxForm({
    form: "ServiceSaveForm",
    enableReinitialize: true
})(ServiceSaveForm);
