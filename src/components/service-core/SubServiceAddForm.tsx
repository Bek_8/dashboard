import React from "react";
import makeStyles from "@material-ui/core/styles/makeStyles";
import {formValueSelector, reduxForm, change} from "redux-form";
import Grid from "@material-ui/core/Grid";
import FormTextInputField from "../../widgets/redux/FormTextInputField";
import Button from "@material-ui/core/Button";
import fp from "lodash/fp"
import {useDispatch, useSelector} from "react-redux";
import FormServiceAutoCompleteField from "../form-elements/FormServiceAutoCompleteField";
import FormReferenceAutoCompleteField from "../form-elements/FormReferenceAutoCompleteField";

import AddIcon from '@material-ui/icons/Add';
import {isEmpty} from "../../widgets/redux/Validator";
import {Edit} from "@material-ui/icons";
import {Simulate} from "react-dom/test-utils";

const useStyles = makeStyles(theme => ({
    headerColor: {
        backgroundColor: theme.palette.primary.main,
        display: "flex",
        justifyContent: "space-between",
        alignItems: "center",
        padding: theme.spacing(0, 2)
    },
    form: {
        width: '100%',
    },
    submit: {
        margin: theme.spacing(0, 2, 0, 0),
    },
}));

interface IProps {
    handleBack: () => void,
    pristine?: boolean,
    submitting?: boolean,
    item?: any,
    submitForm: (d: any) => void
    handleSubmit?: any
};

const selector = formValueSelector('SubServiceAddForm')
// const hasId = fp.has("id")

function SubServiceAddForm(props: IProps) {
    const classes = useStyles(props);
    const dispatch = useDispatch();
    const selectedMake = useSelector(state => selector(state, 'make')) || null;
    const selectedModel = useSelector(state => selector(state, 'model')) || null;
    // const providerItem = useSelector(createSelector(
    //     (state: any) => state.provider,
    //     (provider: any) => provider.item
    // ));
    const {
        item,
        pristine,
        submitting,
        handleSubmit,
        submitForm,
    } = props;


    return (
        <form className={classes.form} noValidate onSubmit={handleSubmit(submitForm)}>
            <Grid
                container
                alignItems="flex-start"
                spacing={3}>
                <Grid item xs={12} md={3}>
                    <FormReferenceAutoCompleteField
                        name="make"
                        label="Марка"
                        validate={isEmpty}
                        parentCode={'_VEHICLE_MAKE'}
                        variant="outlined"
                        fullWidth={true}
                        onSelected={(v)=>{
                            dispatch(change('SubServiceAddForm','model', null))
                            dispatch(change('SubServiceAddForm','modification', null))
                        }}
                        compareOptions={(option, value) => (option.id === value.id)}
                        formatOption={v => v?.nameRu || ''}
                    />
                    <FormTextInputField
                        name="serviceId"
                        fullWidth={true}
                        InputProps={{
                            fullWidth: true,
                            variant: "outlined",
                            style: {display: "none"}
                        } as any}
                    />
                </Grid>
                <Grid item xs={12} md={3}>
                    <FormReferenceAutoCompleteField
                        name="model"
                        validate={isEmpty}
                        onSelected={(v)=>{
                            dispatch(change('SubServiceAddForm','modification', null))
                        }}
                        label="Модель"
                        parentCode={selectedMake?.code || '_VEHICLE_MODELS'}
                        variant="outlined"
                        fullWidth={true}
                        compareOptions={(option, value) => (option.id === value.id)}
                        formatOption={v => v?.nameRu || ''}
                    />
                </Grid>
                <Grid item xs={12} md={3}>
                    <FormServiceAutoCompleteField
                        name="modification"
                        parentCode={selectedModel?.code}
                        label="Модификация"
                        compareOptions={(option, value) => (option.id == value.id)}
                        formatOption={v => v?.nameRu || ''}
                        fullWidth={true}
                    />
                </Grid>
                <Grid item xs={12} md={2}>
                    <FormTextInputField
                        name="price"
                        label="Цена"
                        validate={isEmpty}

                        fullWidth={true}
                        InputProps={{
                            label: "Цена",
                            fullWidth: true,
                            variant: "outlined"
                        } as any}
                    />
                </Grid>
                <Grid item xs={12} md={1}>
                    <Button
                        disabled={pristine || submitting}
                        className={classes.submit}
                        style={{marginTop: 15, padding: 16.5}}
                        size="large"
                        type="submit"
                        color="primary"
                        variant="contained">
                        {item && item?.id? <Edit/>: <AddIcon/>}
                    </Button>
                </Grid>

            </Grid>
        </form>
    )
}

export default reduxForm({
    form: "SubServiceAddForm",
    enableReinitialize: true
})(SubServiceAddForm);
