import React, {useState} from "react";
import CardComponent from "../../widgets/CardComponent";
import makeStyles from "@material-ui/core/styles/makeStyles";
import {reduxForm, change} from "redux-form";
import Grid from "@material-ui/core/Grid";
import FormTextInputField from "../../widgets/redux/FormTextInputField";
import Button from "@material-ui/core/Button";
import {ImageInput} from "../../widgets/redux/FileInputField";
import {useDispatch} from "react-redux";

const useStyles = makeStyles(theme => ({
    headerColor: {
        backgroundColor: theme.palette.primary.main,
        display: "flex",
        justifyContent: "space-between",
        alignItems: "center",
        padding: theme.spacing(0, 2)
    },
    form: {
        width: "100%", // Fix IE 11 issue.
        marginTop: theme.spacing(1)
    },
    submit: {
        margin: theme.spacing(0, 2, 0, 0),
    },
}));

interface IProps {
    handleBack: () => void,
    pristine?: boolean,
    submitting?: boolean,
    submitForm: (d: any) => void
    handleSubmit?: any
    hasImage: boolean
    item: any
};

function AutoServiceSaveForm(props: IProps) {
    const [image, setImage] = useState(null)
    const dispatch = useDispatch();
    const classes = useStyles(props);
    const {
        pristine,
        submitting,
        handleSubmit,
        submitForm,
    } = props;


    return (
        <CardComponent title={true ? `Редактирование` : "Новая марка"}>
            <form className={classes.form} noValidate onSubmit={handleSubmit(submitForm)}>
                <Grid
                    container
                    alignItems="flex-start"
                    spacing={3}>
                    <Grid item xs={12} sm={6}>
                        <FormTextInputField
                            name="nameUz"
                            label="Название (uz)"
                            fullWidth={true}
                            InputProps={{
                                fullWidth: true,
                                label: "Название (uz)",
                                variant: "outlined"
                            } as any}
                        />
                    </Grid>
                    <Grid item xs={12} sm={6}>
                        <FormTextInputField
                            name="nameRu"
                            label="Название (ru)"
                            fullWidth={true}
                            InputProps={{
                                fullWidth: true,
                                label: "Название (ru)",
                                variant: "outlined"
                            } as any}
                        />
                    </Grid>
                    <Grid item xs={12} sm={6}>
                        <FormTextInputField
                            name="nameEn"
                            label="Название (en)"
                            fullWidth={true}
                            InputProps={{
                                fullWidth: true,
                                label: "Название (en)",
                                variant: "outlined"
                            } as any}
                        />
                    </Grid>
                    <Grid item xs={12} sm={6}>
                        <FormTextInputField
                            name="parentCode"
                            fullWidth={true}
                            InputProps={{
                                fullWidth: true,
                                type: 'hidden',
                                style: {display: 'none'},
                                variant: "outlined"
                            } as any}
                        />
                    </Grid>
                    {props.hasImage && <Grid item xs={12} sm={6}>
                        <ImageInput imagePath={image ? URL.createObjectURL(image) : props.item?.imageUrl}
                                    onChange={e => {
                                        console.log("file ", e.target, e.target.files, e)
                                        const file = e.target.files[0]
                                        setImage(file);
                                        dispatch(change('AutoServiceSaveForm', 'image', file))
                                    }}
                        />
                    </Grid>}
                    <Grid item>
                        <Button
                            disabled={pristine || submitting}
                            className={classes.submit}
                            type="submit"
                            color="primary"
                            variant="contained">
                            Сохранить
                        </Button>
                        <Button
                            disabled={submitting}
                            className={classes.submit}
                            onClick={props.handleBack}
                            color="secondary"
                            variant="outlined">
                            Закрыть
                        </Button>
                    </Grid>

                </Grid>
            </form>
        </CardComponent>
    )
}

export default reduxForm({
    form: "AutoServiceSaveForm",
    enableReinitialize: true
})(AutoServiceSaveForm);
