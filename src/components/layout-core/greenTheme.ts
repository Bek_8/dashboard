
import createMuiTheme from "@material-ui/core/styles/createMuiTheme";

const theme = createMuiTheme({
        palette: {
          primary: {
            main: '#388e3c',
          },
          secondary: {
            main: '#388e3c',
          },
        },
  });


  export default theme;