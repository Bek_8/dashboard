import React, { Children } from 'react';
import SnackbarComponent from "../common-core/SnackbarComponent";
import BackdropLoader from "../common-core/BackdropLoader";
import { MuiThemeProvider, createMuiTheme } from '@material-ui/core';
import blackTheme  from '../layout-core/blackTheme';
import mainTheme from '../layout-core/mainTheme';
import greenTheme from '../layout-core/greenTheme';
import { useSelector } from 'react-redux';
import { any } from 'prop-types';
import { createSelector } from 'reselect';


interface IProps {
    children: React.ReactNode
}

const theme = createMuiTheme({
	"palette": {
    }

});

const RootLayout = (props: IProps) => {
    const color = useSelector(createSelector(
        (state: any) => state.theme,
        (stateSection: any) => stateSection.color
    ));
    let selectedTheme = mainTheme;
    switch(color) {
        case 'green': selectedTheme=greenTheme;
        break;
        case 'black': selectedTheme=blackTheme;
        break;
     }     
    return (
        <MuiThemeProvider theme={selectedTheme}>
            {props.children}
            <SnackbarComponent/>
            <BackdropLoader/>
        </MuiThemeProvider>
        )
}

export default RootLayout;