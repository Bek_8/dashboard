
import createMuiTheme from "@material-ui/core/styles/createMuiTheme";

const theme = createMuiTheme({
        palette: {
          primary: {
            main: '#1976d2',
          },
          secondary: {
            main: '#1976d2',
          },
        },
  });

  export default theme;