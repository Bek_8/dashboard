
import createMuiTheme from "@material-ui/core/styles/createMuiTheme";

const theme = createMuiTheme({
    palette: {
        primary: {
          main: '#424242',
        },
        secondary: {
          main: '#424242',
        },
      },
  });

 

  export default theme;