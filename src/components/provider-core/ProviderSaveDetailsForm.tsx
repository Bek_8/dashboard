import React, {useState} from "react";
import {change, FieldArray, formValueSelector, reduxForm} from "redux-form";
import {makeStyles, Theme} from "@material-ui/core";
import CardComponent from "../../widgets/CardComponent";
import Grid from "@material-ui/core/Grid";
import FormTextInputField from "../../widgets/redux/FormTextInputField";
import Button from "@material-ui/core/Button";
import {Map, Placemark, YMaps} from 'react-yandex-maps';
// import TimeSlotListField from "./TimeSlotListField";
import FormCategorySelectField from "../form-elements/FormCategorySelectField";
import {useDispatch, useSelector} from "react-redux";
import TimeSlotListField from "./TimeSlotListField";
import IconButton from "@material-ui/core/IconButton";
import {PhotoCamera} from "@material-ui/icons";
import {ImageInput} from "../../widgets/redux/FileInputField";
import FormCommissionAutoCompleteField from "../form-elements/FormCommissionAutoCompleteField";
import FormOwnerAutoCompleteField from "../form-elements/FormOwnerAutoCompleteField";

type Props = {
    handleSubmit?: Function;
    providerItem: any;
    submitForm: string;
    pristine?: boolean;
    submitting?: boolean;
};
const validate = (values) => {
    const errors: any = {
        address: {},
    };
    if (!values.name) {
        errors.name = "обязательный";
    }
    if (!values.comission) {
        errors.comission = "обязательный";
    }
    // if (!values.address || !values.address.phone) {
    //     errors.address.phone = "обязательный";
    // }
    return errors;
};
const useStyles = makeStyles((theme: Theme) => ({
    headerColor: {
        backgroundColor: theme.palette.primary.main,
        display: "flex",
        justifyContent: "space-between",
        alignItems: "center",
        padding: theme.spacing(0, 2),
    },
    form: {
        width: "100%", // Fix IE 11 issue.
        marginTop: theme.spacing(1),
    },
    submit: {
        margin: theme.spacing(0, 2, 0, 0),
    },
}));


const selector = formValueSelector("ProviderSaveDetailsForm");


const ProviderSaveDetailsForm = (props: Props) => {
    const regionId = useSelector((state) => selector(state, "address.regionId"));
    const dispatch = useDispatch()
    const [image, setImage]=useState(null)
    const classes = useStyles(props);
    const isEdit = Boolean(props.providerItem?.id);
    const [marker, setMarker] = useState<number[]>(isEdit ? [props.providerItem.lat, props.providerItem.lng] : []);

    function onMapClick(event) {
        setMarker(event.get("coords"));
        console.log(event.get('coords'));
        const marker = event.get('coords');
        dispatch(change('ProviderSaveDetailsForm', 'lat', marker[0]))
        dispatch(change('ProviderSaveDetailsForm', 'lng', marker[1]))
    }

    return (
        <CardComponent>
            <form
                className={classes.form}
                onSubmit={props.handleSubmit(props.submitForm)}
            >
                <Grid container alignItems="flex-start" spacing={3}>
                    <Grid item xs={12} sm={6} md={4}>
                        <Button
                            className={classes.submit}
                            type="submit"
                            color="primary"
                            variant="contained"
                        >
                            Сохранить
                        </Button>
                    </Grid>
                </Grid>

                <Grid container alignItems="flex-start" spacing={3}>
                    <Grid item xs={12} sm={6} md={4}>
                        <FormTextInputField
                            name="name"
                            InputProps={{
                                fullWidth: true,
                                label: "Provider Name",
                                variant: "outlined",
                            }}
                        />

                    </Grid>
                </Grid>
                <Grid container alignItems="flex-start" spacing={3}>
                    <Grid item xs={12} sm={5} md={3}>
                        <FormCategorySelectField
                            name="categoryId"
                            label="Category"
                            variant="outlined"
                            parentCode="_SERVICE_PROVIDER_CATEGORIES"
                            compareOptions={(option, value) => option.id === value}
                            fullWidth={true}
                            formatOption={(v) => v.name}
                            formatValue={(v) => v.id}

                        />

                    </Grid>
                    <Grid item xs={12} sm={5} md={3}>
                        <FormCommissionAutoCompleteField
                            name="comission"
                            label="Commission"
                            variant="outlined"
                            compareOptions={(option, value) => {
                                console.log("Compare ---", option, value)
                                return option?.id === value.id}}
                            fullWidth={true}
                            formatOption={(v) => {console.log('formatOption -- ',v); return v?.name||''}}
                        />
                    </Grid>
                    <Grid item xs={12} sm={5} md={3}>
                        <FormOwnerAutoCompleteField
                            name="owner"
                            label="Owner"
                            variant="outlined"
                            compareOptions={(option, value) => {
                                console.log("Compare ---", option, value)
                                return option?.id === value.id}}
                            fullWidth={true}
                            formatOption={(v) => {console.log('formatOption -- ',v); return v?.fullName||''}}
                        />
                    </Grid>
                    <Grid item xs={12} sm={2} md={3}>
                       <ImageInput imagePath={image ? URL.createObjectURL(image) : props.providerItem?.imageUrl }
                                   onChange={e=>{
                                       console.log("file ", e.target, e.target.files, e)
                                       const file = e.target.files[0]
                                       setImage(file);
                                       dispatch(change('ProviderSaveDetailsForm', 'image', file))
                                   }}
                       />
                    </Grid>
                </Grid>

                <Grid container alignItems="flex-start" spacing={3}>
                    <Grid item sm={12} md={7}>
                        <FieldArray name="timeslots" component={TimeSlotListField}/>
                    </Grid>
                    <Grid item xs={"auto"} sm={"auto"} md={1}/>
                    <Grid item sm={12} md={4}>
                        <YMaps>
                            <Map
                                name="coordinate"
                                query={{
                                    apiKey: "b7af3a27-9b29-48aa-a577-41a46a55bd21",
                                }}
                                onClick={e => onMapClick(e)}
                                defaultState={{
                                    center: marker.length?[marker[0], marker[1]]:[],
                                    zoom: 9,
                                    controls: [],
                                }}
                                state={{
                                    center: marker.length?[marker[0], marker[1]]:[],
                                    zoom: 9,
                                    controls: [],
                                }}
                            >
                                {marker &&
                                <Placemark geometry={marker}
                                           options={{
                                               draggable: true,


                                           }}/>}
                                {/*<SearchControl*/}
                                {/*    options={{ float: 'none' }}*/}

                                {/*/>*/}
                            </Map>
                        </YMaps>

                        {/*<FormTextInputField*/}
                        {/*    name="address.address"*/}
                        {/*    InputProps={{*/}
                        {/*        fullWidth: true,*/}
                        {/*        label: "Address 1",*/}
                        {/*        variant: "outlined",*/}
                        {/*    }}*/}
                        {/*/>*/}
                        <FormTextInputField
                            name="lat"
                            InputProps={{
                                fullWidth: true,
                                label: "Latitude",
                                variant: "outlined",
                            }}

                            onChange={e => {

                                console.log('onChange --', e)
                                setMarker([Number(e.target.value), marker[1]])
                            }}
                        />
                        <FormTextInputField
                            name="lng"
                            InputProps={{
                                fullWidth: true,
                                label: "Longitude",
                                variant: "outlined",
                            }}
                            onChange={e => {
                                console.log('onChange --', e)
                                setMarker([marker[0], Number(e.target.value)])
                            }}
                        />
                    </Grid>
                </Grid>
                <hr/>
                <Grid container alignItems="flex-start" spacing={3}>
                    <Grid item xs={12} sm={6} md={4}>
                        Minimal order price:
                        <FormTextInputField
                            name="minimumPrice"
                            InputProps={{
                                fullWidth: true,
                                label: "Minimal price",
                                placeholder: "Enter",
                                variant: "outlined",
                            }}
                        />
                    </Grid>
                </Grid>
            </form>
        </CardComponent>
    );
};

export default reduxForm({
    form: "ProviderSaveDetailsForm",
    validate,
})(ProviderSaveDetailsForm);
