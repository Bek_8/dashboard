import React, {useEffect, useState} from "react";
import {useDispatch, useSelector} from "react-redux";
import {reset} from "redux-form";
import * as serviceAction from "../../redux/actions/ServiceActions";

import IconButton from "@material-ui/core/IconButton";
import {useHistory, useLocation, useParams} from "react-router-dom";
import DataTable, {DataCol} from "../../components/table-core/DataTable";
import useQueryFilter from "../../hooks/useQueryFilter";
import {Card, CardHeader} from "@material-ui/core";
import SubServiceAddForm from "../service-core/SubServiceAddForm";
import * as ServiceApi from "../../api/ServiceApi";
import {setDeleteConfirmationItem, toggleBackdropLoader, toggleSnackbar} from "../../redux/actions/MessageActions";
import DeleteIcon from "@material-ui/icons/Delete";
import {updateQuery} from "../../utils/UrlUtils";
import * as H from "history";
import DialogTitle from "@material-ui/core/DialogTitle";
import DialogContent from "@material-ui/core/DialogContent";
import DialogContentText from "@material-ui/core/DialogContentText";
import Dialog from "@material-ui/core/Dialog";
import {Edit} from "@material-ui/icons";
import {ListingFilter} from "../../types/ApiOjbects";

export default function ProviderServicePriceList(props: any) {
    const dispatch = useDispatch()
    const location = useLocation()
    const history: H.History = useHistory()
    const params: any = useParams();
    const filter = useQueryFilter(params);
    const [editItem, setEditItem] = useState<any>(null)
    filter.serviceId = filter.id;
    const list = useSelector((state: any) => state.service.subList)
    const loader = useSelector((state: any) => state.service.loader)
    const count = useSelector((state: any) => state.service.subCount)

    useEffect(() => {
        dispatch(serviceAction.getSubListAction(filter))
    }, [filter])

    return (
        <Card style={{marginTop: 15}}>
            <CardHeader title={"Цена"}
                        subheader={<SubServiceAddForm
                            submitForm={async data => {
                                try {
                                    const payload = {...data}
                                    console.log('onSubmit ---', payload);
                                    if (payload.make) {
                                        payload.makeId = payload.make?.id;
                                        delete payload.make;
                                    }
                                    if (payload.model) {
                                        payload.modelId = payload.model?.id;
                                        delete payload.model;
                                    }
                                    if (payload.modification) {
                                        payload.modificationId = payload.modification?.id;
                                        delete payload.modification;
                                    }
                                    // const resp =
                                    dispatch(toggleBackdropLoader(true));
                                    const resp:any = await ServiceApi.createSubItem(payload);
                                    dispatch(reset('SubServiceAddForm'));
                                    dispatch(serviceAction.getSubListAction(filter))
                                } catch (e) {
                                    console.error(e);
                                    if(e?.response?.data?.errorMessage){
                                        dispatch(toggleSnackbar(e?.response?.data?.errorMessage));
                                    }
                                } finally {
                                    dispatch(toggleBackdropLoader(false));
                                }
                            }}
                            initialValues={{
                                serviceId: Number(params?.id || 0)
                            }}
                        />}/>

            <Dialog
                open={Boolean(editItem)}
                onClose={() => setEditItem(null)}
                maxWidth={'md'}
                fullWidth={true}
                aria-labelledby="responsive-dialog-title">

                <DialogTitle>
                    Изменить цену
                </DialogTitle>

                <DialogContent style={{overflow:'hidden'}}>
                    <DialogContentText>
                        {editItem?.id && <SubServiceAddForm
                            item={editItem}
                            submitForm={async data => {
                                try {
                                    const payload = {...data};
                                    console.log('onSubmit ---', payload);
                                    if (payload.make) {
                                        payload.makeId = payload.make?.id;
                                        delete payload.make;
                                    }
                                    if (payload.model) {
                                        payload.modelId = payload.model?.id;
                                        delete payload.model;
                                    }
                                    if (payload.modification) {
                                        payload.modificationId = payload.modification?.id;
                                        delete payload.modification;
                                    }
                                    // const resp =
                                    dispatch(toggleBackdropLoader(true));
                                    const resp:any = await ServiceApi.updateSubItem(payload);
                                    // if(resp.data.)
                                    setEditItem(null);
                                    dispatch(reset('SubServiceAddForm'));
                                    dispatch(serviceAction.getSubListAction(filter))

                                } catch (e) {
                                    console.error(e);
                                    if(e?.response?.data?.errorMessage){
                                        dispatch(toggleSnackbar(e?.response?.data?.errorMessage));
                                    }
                                } finally {
                                    dispatch(toggleBackdropLoader(false));
                                }
                            }}
                            initialValues={{
                                ...editItem,
                                make: {id: editItem.makeId, nameRu: editItem.makeName, code: editItem.makeCode},
                                model: {id: editItem.modelId, nameRu: editItem.modelName, code: editItem.modelCode},
                                modification: {
                                    id: editItem.modificationId,
                                    nameRu: editItem.modificationName,
                                    code: editItem.modificationCode
                                },
                                serviceId: Number(params?.id || 0)
                            }}
                        />}
                    </DialogContentText>
                </DialogContent>
            </Dialog>
            <DataTable
                listing={{list, count}}
                filter={filter}
                loader={loader}
                onDeleteItem={(itemId) => dispatch(serviceAction.deleteSubItemAction(itemId))}
                getList={(filter) => dispatch(serviceAction.getSubListAction(filter))}
                onFilterChange={query => history.push(updateQuery(location, query))}
                showToolbar={false}
                showPagination={false}

            >
                <DataCol label={"Марка"}
                         sortable={true}
                         fieldKey={"makeName"}
                         cellRenderer={row => row.get("makeName")}/>

                <DataCol label={"Модель"}
                         sortable={true}
                         fieldKey={"modelName"}
                         cellRenderer={row => row.get("modelName")}/>

                <DataCol label={"Модификация"}
                         sortable={true}
                         fieldKey={"modificationName"}
                         cellRenderer={row => row.get("modificationName")}/>

                <DataCol label={"Цена"}
                         sortable={true}
                         fieldKey={"price"}
                         cellRenderer={row => row.get("price")}/>

                <DataCol label={"Действие"}
                         cellRenderer={row => (<>
                                 <IconButton aria-label="Edit"
                                             onClick={() => setEditItem(row.toJSON())}
                                             size="small">
                                     <Edit/>
                                 </IconButton>
                                 <IconButton aria-label="Удалить"
                                             onClick={() => dispatch(setDeleteConfirmationItem(row))}
                                             size="small">
                                     <DeleteIcon/>
                                 </IconButton>
                             </>
                         )}/>


            </DataTable>
        </Card>
    )
}
