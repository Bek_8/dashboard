import React, { useEffect} from "react";
import {useDispatch, useSelector} from "react-redux";
import * as serviceAction from "../../redux/actions/ServiceActions";

import IconButton from "@material-ui/core/IconButton";
import EditIcon from "@material-ui/icons/Edit";
import {Link, useHistory, useLocation, useParams} from "react-router-dom";
import {updateQuery} from "../../utils/UrlUtils";
import DataTable, {DataCol} from "../../components/table-core/DataTable";
import useQueryFilter from "../../hooks/useQueryFilter";
import {Card} from "@material-ui/core";

export default function ProviderServicesTab(props: any) {
    const dispatch = useDispatch()
    const history = useHistory()
    const location = useLocation()
    const params: any = useParams();
    const filter = useQueryFilter(params);
    const providerId = params.providerId

    const list = useSelector((state: any) => state.service.list)
    const loader = useSelector((state: any) => state.service.loader)
    const count = useSelector((state: any) => state.service.count)

    useEffect(() => {
        dispatch(serviceAction.listAction(filter))
    }, [filter])

    return (
        <Card>
            <DataTable
                listing={{list, count}}
                filter={filter}
                loader={loader}
                addLink={`/provider/${providerId}/service/add`}
                onDeleteItem={(itemId) => dispatch(serviceAction.deleteItemAction(itemId))}
                getList={(filter) => dispatch(serviceAction.listAction(filter))}
                onFilterChange={query => history.push(updateQuery(location, query))}
            >
                <DataCol label={"ID"}
                         sortable={true}
                         fieldKey={"id"}
                         cellRenderer={row => row.get("id")}/>

                <DataCol label={"Название"}
                         sortable={true}
                         fieldKey={"providerName"}
                         cellRenderer={row => row.get("providerName")}/>

                <DataCol label={"Категория"}
                         sortable={true}
                         fieldKey={"categoryName"}
                         cellRenderer={row => row.get("categoryName")}/>

                <DataCol label={"Цена"}
                         fieldKey={"price"}
                         cellRenderer={row => row.get("price")}/>

                <DataCol label={"Действие"}
                         cellRenderer={row => (
                             <IconButton aria-label="Редактировать"
                                         component={Link}
                                         to={`/provider/${providerId}/service/${row.get("id")}`}
                                         size="small">
                                 <EditIcon/>
                             </IconButton>
                         )}/>


            </DataTable>
        </Card>
    )
}
