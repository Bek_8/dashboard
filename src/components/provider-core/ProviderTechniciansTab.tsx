import React, {Fragment, useEffect, useState} from "react";
import {useDispatch, useSelector} from "react-redux";
import * as technicianAction from "../../redux/actions/TechnicianActions";

import IconButton from "@material-ui/core/IconButton";
import EditIcon from "@material-ui/icons/Edit";
import {Link, useHistory, useLocation, useParams} from "react-router-dom";
import {updateQuery} from "../../utils/UrlUtils";
import DataTable, {DataCol} from "../../components/table-core/DataTable";
import useQueryFilter from "../../hooks/useQueryFilter";
import CardComponent from "../../widgets/CardComponent";
import {Card} from "@material-ui/core";


export default function ProviderTechniciansTab(props: any) {
    const dispatch = useDispatch()
    const history = useHistory()
    const location = useLocation()
    const params: any = useParams();
    const filter = useQueryFilter(params);
    const providerId = params.providerId

    const list = useSelector((state: any) => state.technician.list)
    const loader = useSelector((state: any) => state.technician.loader)
    const count = useSelector((state: any) => state.technician.count)

    useEffect(() => {
        dispatch(technicianAction.listAction(filter))
    }, [filter])

    return (
        <Card>
            <DataTable
                listing={{list, count}}
                filter={filter}
                loader={loader}
                addLink={`/provider/${providerId}/technician/add`}
                onDeleteItem={(itemId) => dispatch(technicianAction.deleteItemAction(itemId))}
                getList={(filter) => dispatch(technicianAction.listAction(filter))}
                onFilterChange={query => history.push(updateQuery(location, query))}
            >
                <DataCol label={"ID"}
                         sortable={true}
                         fieldKey={"id"}
                         cellRenderer={row => row.get("id")}/>

                <DataCol label={"ФИО"}
                         sortable={true}
                         fieldKey={"firstName"}
                         cellRenderer={row => `${row.get("firstName")} ${row.get("lastName")} `}/>

                <DataCol label={"Категория"}
                         sortable={true}
                         fieldKey={"categoryName"}
                         cellRenderer={row => row.get("categoryName")}/>

                <DataCol label={"Комиссия"}
                         fieldKey={"comissionName"}
                         cellRenderer={row => row.get("comissionName") ? row.get("comissionName") : "Нет"}/>

                <DataCol label={"SOS"}
                         fieldKey={"sosAvailable"}
                         cellRenderer={row => row.get("sosAvailable") ? "Да" : "Нет"}/>

                <DataCol label={"Статус"}
                         fieldKey={"status"}
                         cellRenderer={row => row.get("status")}/>

                <DataCol label={"Действие"}
                         cellRenderer={row => (
                             <IconButton aria-label="Редактировать"
                                         component={Link}
                                         to={`/provider/${providerId}/technician/${row.get("id")}`}
                                         size="small">
                                 <EditIcon/>
                             </IconButton>
                         )}/>


            </DataTable>
        </Card>
    )
}
