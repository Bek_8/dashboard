import React, {Component} from 'react';
import {connect} from 'react-redux';
import FormCheckboxField from "../../widgets/redux/FormCheckboxField";
import FormTextInputField from "../../widgets/redux/FormTextInputField";
import FormTimePickerField from "../../widgets/redux/FormTimePickerField";
import {getTimeSlotList} from "../../utils/ProviderUtils";

const timeSlotList = getTimeSlotList()

function TimeSlotListField({ fields, ...rest}) {
    console.log("slot field = ", rest, fields)
    return (
        <table cellSpacing={20}>
            <thead>
            <tr>
                <th style={{width: "55%"}}>Word day</th>
                <th>From Time</th>
                <th>To Time</th>
            </tr>
            </thead>
            <tbody>
            {fields.map((timeslot, index) => (
                <tr key={index}>
                    <td>
                        <FormCheckboxField
                            name={`${timeslot}.selected`}
                            label={timeSlotList[index].weekday}
                        />

                        <FormTextInputField
                            name={`${timeslot}.weekday`}
                            InputProps={{
                                fullWidth: true,
                                type: "hidden",
                                variant: "outlined",
                                style: {display: 'none'}
                            }}
                        />
                        <FormTextInputField
                            name={`${timeslot}.id`}
                            InputProps={{
                                fullWidth: true,
                                type: "hidden",
                                variant: "outlined",
                                style: {display: 'none'}
                            }}
                        />
                    </td>
                    <td>
                        <FormTimePickerField
                            name={`${timeslot}.startTime`}
                            label="From"
                            fullWidth={true}
                            dateFormat={"HH:mm"}
                        />
                    </td>
                    <td>
                        <FormTimePickerField
                            name={`${timeslot}.endTime`}
                            label="To"
                            fullWidth={true}
                            dateFormat={"HH:mm"}
                        />
                    </td>
                </tr>))}
            </tbody>
        </table>
    );
}

export default TimeSlotListField;