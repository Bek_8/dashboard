import React from 'react';
import {reduxForm, FieldArray, formValueSelector} from "redux-form"
import {makeStyles, Theme} from "@material-ui/core";
import CardComponent from "../../widgets/CardComponent";
import Grid from "@material-ui/core/Grid";
import FormSelectField from "../../widgets/redux/FormSelectField";
import FormTextInputField from "../../widgets/redux/FormTextInputField";
import Button from "@material-ui/core/Button";
import TimeSlotListField from "./TimeSlotListField";


const useStyles = makeStyles((theme: Theme) => ({
    headerColor: {
        backgroundColor: theme.palette.primary.main,
        display: "flex",
        justifyContent: "space-between",
        alignItems: "center",
        padding: theme.spacing(0, 2)
    },
    form: {
        width: "100%", // Fix IE 11 issue.
        marginTop: theme.spacing(1)
    },
    submit: {
        margin: theme.spacing(0, 2, 0, 0),
    }
}));

const ProviderServicesTable = (props: any) => {
    const classes = useStyles(props);
    return (
        <CardComponent>

        </CardComponent>
    );
};

export default ProviderServicesTable