import  React from 'react';
import AppBar from "@material-ui/core/AppBar/AppBar";
import Tabs from "@material-ui/core/Tabs";
import {PROVIDER_ITEM_TABS} from "../../constants/ProviderConstants";
import Tab from "@material-ui/core/Tab";
import {Link} from "react-router-dom";
import {Location} from "history";

type Props = {
    selectedTab:string,
    location: Location
};

export default function ProviderItemTabs(props: Props) {
    return (
            <AppBar position="static" color="default">
                <Tabs
                    value={props.selectedTab}
                    indicatorColor="primary"
                    textColor="primary"
                    variant="scrollable"
                    scrollButtons="auto">
                    {PROVIDER_ITEM_TABS.map(tabItem => (
                        <Tab
                            disabled={tabItem.disabled}
                            label={tabItem.name}
                            key={tabItem.id}
                            component={Link}
                            to={`${props.location.pathname}#${tabItem.value}`}
                            value={tabItem.value}
                        />
                    ))}

                </Tabs>
            </AppBar>
    );
};