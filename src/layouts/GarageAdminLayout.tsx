import React from "react";
import {connect} from "react-redux";
import {makeStyles} from "@material-ui/core";
import MainHeader from "./MainHeader";
import GarageAdminSidebar from "./garage-admin/GarageAdminSidebar";
import CssBaseline from "@material-ui/core/CssBaseline";
import * as authActions from "../redux/actions/AuthActions";
import MainFooter from './MainFooter';

const useStyles = makeStyles(theme => ({
    root: {
        display: 'flex',
        background: theme.palette[100],
    },
    content: {
        flexGrow: 1,
        padding: theme.spacing(3),
    },
    toolbar: theme.mixins.toolbar,


}));

function GarageAdminLayout(props: any) {
    const classes = useStyles(props);
    const [open, setOpen] = React.useState(true);

    if (props.auth == null || props.auth.token == null) {
        props.dispatch(authActions.restoreAuth());
    }

    function handleLeftMenuState() {
        setOpen(!open);
    }

    function logout() {
        props.dispatch(authActions.logout());
    }

    return (
        <>
            <div className={classes.root}>
                <CssBaseline/>
                <MainHeader
                    open={open}
                    logout={logout}
                    handleDrawerOpen={handleLeftMenuState}
                />
                <GarageAdminSidebar
                    location={props.location}
                    open={open}
                />

                <main className={classes.content}>
                    <div className={classes.toolbar}/>
                    {props.children}
                </main>
            </div>
            <MainFooter/>
        </>
    )
}

export default connect(({dashboardLayout, auth}: any) => ({
    dashboardLayout,
    auth
}))(GarageAdminLayout);
