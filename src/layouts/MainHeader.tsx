import React from "react";
import AppBar from "@material-ui/core/AppBar";
import Toolbar from "@material-ui/core/Toolbar";
import IconButton from "@material-ui/core/IconButton";
import ExitToAppIcon from '@material-ui/icons/ExitToApp';
import makeStyles from "@material-ui/core/styles/makeStyles";
import MenuIcon from '@material-ui/icons/Menu';
import Typography from "@material-ui/core/Typography";
import clsx from "clsx";
import Tooltip from "@material-ui/core/Tooltip";
import {Link} from "react-router-dom";
import {useSelector} from "react-redux";
import {auth} from "../redux/actions/AuthActions";
import AccountCircleIcon from '@material-ui/icons/AccountCircle';
import {createSelector} from 'reselect'

const drawerWidth = 240;

const useStyles = makeStyles(theme => ({
    appBar: {
        background: theme.palette.primary[700],
        zIndex: theme.zIndex.drawer + 1,
        transition: theme.transitions.create(['width', 'margin'], {
            easing: theme.transitions.easing.sharp,
            duration: theme.transitions.duration.leavingScreen,
        }),
    },
    logoLink: {
        color: theme.palette.common.white,
        textDecoration: "none"
    },
    appBarShift: {
        marginLeft: drawerWidth,
        // width: `calc(100% - ${drawerWidth}px)`,
        width: "100%",
        transition: theme.transitions.create(['width', 'margin'], {
            easing: theme.transitions.easing.sharp,
            duration: theme.transitions.duration.enteringScreen,
        }),
    },
    menuButton: {
        marginRight: 36,
        color: theme.palette.common.white
    },
    hide: {
        display: 'none',
    },
    title: {
        flexGrow: 1,
        color: theme.palette.common.white
    },
    menuEnd: {
        color: theme.palette.common.white
    },
    profileTitle: {
        fontSize: 20,
        color: theme.palette.common.white,
        textDecoration: "none"
    }
}));

const getUserName = createSelector(
    (state:any) => state.auth,
    (auth:any) => auth.user,
)

export default function MainHeader(props: any) {
    const classes = useStyles(props);
    const {handleDrawerOpen, open, logout, getUser} = props;
    const color = useSelector((state:any) => state.theme.color);
    const item = useSelector(getUserName);
    return (
        <AppBar
            position="fixed"
            className={clsx(classes.appBar, {
                [classes.appBarShift]: open,
            })}
        >
            <Toolbar>
                <IconButton
                    aria-label="open drawer"
                    onClick={handleDrawerOpen}
                    edge="start"
                    className={clsx(classes.menuButton)}
                >
                    <MenuIcon/>
                </IconButton>
                <Typography
                    variant="h5"
                    noWrap
                    className={classes.title}
                >
                    <Link to="/" className={classes.logoLink}>AutoBio</Link>
                </Typography>
                <Tooltip title="Profile" aria-label="Выход" placement="bottom-end">
                    <IconButton
                        className={classes.menuEnd}
                    >
                        <Link to="/profile" className={classes.logoLink}>
                            <AccountCircleIcon/>
                        </Link>
                    </IconButton>
                </Tooltip>
                <Typography
                    variant="h6"
                    noWrap
                >
                    <Link
                        to="/profile"
                        className={classes.profileTitle}
                    >
                        {item.firstName} {item.lastName}
                    </Link>
                </Typography>
                <Tooltip title="Выход" aria-label="Выход" placement="top">
                    <IconButton
                        onClick={logout}
                        className={classes.menuEnd}
                    >
                        <ExitToAppIcon/>
                    </IconButton>
                </Tooltip>
            </Toolbar>
        </AppBar>
    );
}
