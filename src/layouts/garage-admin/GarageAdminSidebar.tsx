import React from "react";
import clsx from "clsx";
import Drawer from "@material-ui/core/Drawer";
import Divider from "@material-ui/core/Divider";
import List from "@material-ui/core/List";
import ListItem from "@material-ui/core/ListItem";
import ListItemIcon from "@material-ui/core/ListItemIcon";
import ListItemText from "@material-ui/core/ListItemText";
import { makeStyles } from "@material-ui/core";
import { Link } from "react-router-dom";
import { ExpandLess, ExpandMore } from "@material-ui/icons";
import Collapse from "@material-ui/core/Collapse";
import GroupIcon from "@material-ui/icons/PeopleOutline";
import DictionaryIcon from "@material-ui/icons/FormatListBulleted";
import ProviderIcon from "@material-ui/icons/People";
import SettingsApplicationsIcon from "@material-ui/icons/SettingsApplications";
import ShopIcon from "@material-ui/icons/Shop";
import BuildIcon from "@material-ui/icons/Build";
import FiberManualRecordIcon from "@material-ui/icons/FiberManualRecord";
import ExpenseTypeIcon from "@material-ui/icons/MonetizationOn";
import BusinessCenterIcon from "@material-ui/icons/BusinessCenter";
import LocalGasStationIcon from "@material-ui/icons/LocalGasStation";
import DirectionsCarIcon from "@material-ui/icons/DirectionsCar";

const drawerWidth = 240;

const useStyles = makeStyles((theme) => ({
  drawer: {
    width: drawerWidth,
    flexShrink: 0,
    whiteSpace: "nowrap",
  },
  drawerOpen: {
    width: drawerWidth,
    transition: theme.transitions.create("width", {
      easing: theme.transitions.easing.sharp,
      duration: theme.transitions.duration.enteringScreen,
    }),
  },
  drawerClose: {
    transition: theme.transitions.create("width", {
      easing: theme.transitions.easing.sharp,
      duration: theme.transitions.duration.leavingScreen,
    }),
    overflowX: "hidden",
    width: theme.spacing(7) + 1,
    [theme.breakpoints.up("sm")]: {
      width: theme.spacing(9) + 1,
    },
  },
  nested: {
    paddingLeft: theme.spacing(4),
  },
  toolbar: theme.mixins.toolbar,
}));

export default function GarageAdminSidebar(props: any) {
  const classes = useStyles(props);
  const [memberTabOpen, setOpen] = React.useState(false);
  const [dictionaryCollapse, setDictionaryCollapse] = React.useState(false);
  const { open, location } = props;

  function handleOpenMemberTabs() {
    setOpen(!memberTabOpen);
  }

  function handleOpenDictionaryCollapse() {
    setDictionaryCollapse(!dictionaryCollapse);
  }

  function isActive(view) {
    if (!view || !location || !location.pathname) {
      return false;
    }
    return view === location.pathname;
  }

  return (
    <Drawer
      variant="permanent"
      className={clsx(classes.drawer, {
        [classes.drawerOpen]: open,
        [classes.drawerClose]: !open,
      })}
      classes={{
        paper: clsx({
          [classes.drawerOpen]: open,
          [classes.drawerClose]: !open,
        }),
      }}
      open={open}
    >
      <div className={classes.toolbar} />
      <Divider />
      <List>
        <ListItem
          selected={isActive("/provider")}
          button
          key="Providers"
          component={Link}
          to="/provider"
        >
          <ListItemIcon>
            <ProviderIcon />
          </ListItemIcon>
          <ListItemText primary="Поставщик" />
        </ListItem>

        <Divider />

        <ListItem button key="Settings" component={Link} to="/settings">
          <ListItemIcon>
            <SettingsApplicationsIcon />
          </ListItemIcon>
          <ListItemText primary="Настройки" />
        </ListItem>
      </List>
      <Divider />
    </Drawer>
  );
}
