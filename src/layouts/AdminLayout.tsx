import React from "react";
import {connect} from "react-redux";
import {makeStyles} from "@material-ui/core";
import MainHeader from "./MainHeader";
import AdminSidebar from "./admin/AdminSidebar";
import CssBaseline from "@material-ui/core/CssBaseline";
import * as authActions from "../redux/actions/AuthActions";
import MainFooter from './MainFooter';
import {func} from "prop-types";
import {GET_USER_INFO_SUCCESS} from "../redux/actionTypes/AuthActionTypes";

const useStyles = makeStyles(theme => ({
    root: {
        display: 'flex',
        background: theme.palette[100],
    },
    content: {
        flexGrow: 1,
        padding: theme.spacing(3),
    },
    toolbar: theme.mixins.toolbar,


}));

function AdminLayout(props: any) {
    const classes = useStyles(props);
    const [open, setOpen] = React.useState(true);

    if (props.auth == null || props.auth.token == null) {
        props.dispatch(authActions.restoreAuth());
    }

    function handleLeftMenuState() {
        setOpen(!open);
    }

    function logout() {
        props.dispatch(authActions.logout());
    }
    function getUser() {
        props.dispatch(authActions.getUser(GET_USER_INFO_SUCCESS))
    }

    return (
        <>
            <div className={classes.root}>
                <CssBaseline/>
                <MainHeader
                    open={open}
                    logout={logout}
                    handleDrawerOpen={handleLeftMenuState}
                    getUser={getUser}
                />
                <AdminSidebar
                    location={props.location}
                    open={open}
                    logout={logout}
                />

                <main className={classes.content}>
                    <div className={classes.toolbar}/>
                    {props.children}

                </main>


            </div>
            <MainFooter/>
        </>
    )
}

export default connect(({dashboardLayout, auth}: any) => ({
    dashboardLayout,
    auth
}))(AdminLayout);
