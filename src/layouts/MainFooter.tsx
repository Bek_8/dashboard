import React from "react";
import Typography from "@material-ui/core/Typography";
import makeStyles from "@material-ui/core/styles/makeStyles";
import Grid from '@material-ui/core/Grid';
import cx from "classnames";

const useStyles = makeStyles(theme => ({
    footerStyle: {
        backgroundColor: 'rgba(0,0,0,0.05)',
        borderTop: '1px solid rgba(0,0,0,0.1)',
        marginTop: 60,
        marginLeft: 240,
        display: 'flex',
        padding: theme.spacing(2, 3)
    },
    typography: {
        fontSize: '13px',
    },
    typography2: {
        textAlign: 'right'
    }
}));

export default function MainFooter(props: any) {
    const classes = useStyles(props);

    return (
        <div className={classes.footerStyle}>
            <Grid item xs={6} sm={12}>
                <Typography className={classes.typography}>
                    ©AutoBio.uz
                </Typography>
            </Grid>
            <Grid item xs={6} sm={12}>
                <Typography className={cx(classes.typography, classes.typography2)}>
                    Toshkent - 2019
                </Typography>
            </Grid>
        </div>
    );

}
