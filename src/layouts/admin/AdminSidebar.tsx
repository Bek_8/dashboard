import React from "react";
import clsx from "clsx";
import Drawer from "@material-ui/core/Drawer";
import Divider from "@material-ui/core/Divider";
import List from "@material-ui/core/List";
import ListItem from "@material-ui/core/ListItem";
import ListItemIcon from "@material-ui/core/ListItemIcon";
import ListItemText from "@material-ui/core/ListItemText";
import { makeStyles } from "@material-ui/core";
import { Link } from "react-router-dom";
import { ExpandLess, ExpandMore } from "@material-ui/icons";
import Collapse from "@material-ui/core/Collapse";
import GroupIcon from "@material-ui/icons/PeopleOutline";
import DictionaryIcon from "@material-ui/icons/FormatListBulleted";
import ProviderIcon from "@material-ui/icons/People";
import ShopIcon from "@material-ui/icons/Shop";
import BuildIcon from "@material-ui/icons/Build";
import FiberManualRecordIcon from "@material-ui/icons/FiberManualRecord";
import ExpenseTypeIcon from "@material-ui/icons/MonetizationOn";
import BusinessCenterIcon from "@material-ui/icons/BusinessCenter";
import LocalGasStationIcon from "@material-ui/icons/LocalGasStation";
import AccountCircleIcon from "@material-ui/icons/AccountCircle";

import ExitToAppIcon from "@material-ui/icons/ExitToApp";
import Tooltip from "@material-ui/core/Tooltip";

const drawerWidth = 240;

const useStyles = makeStyles((theme) => ({
  drawer: {
    width: drawerWidth,
    flexShrink: 0,
    whiteSpace: "nowrap",
  },
  drawerOpen: {
    width: drawerWidth,
    transition: theme.transitions.create("width", {
      easing: theme.transitions.easing.sharp,
      duration: theme.transitions.duration.enteringScreen,
    }),
  },
  drawerClose: {
    transition: theme.transitions.create("width", {
      easing: theme.transitions.easing.sharp,
      duration: theme.transitions.duration.leavingScreen,
    }),
    overflowX: "hidden",
    width: theme.spacing(7) + 1,
    [theme.breakpoints.up("sm")]: {
      width: theme.spacing(9) + 1,
    },
  },
  nested: {
    paddingLeft: theme.spacing(4),
  },
  toolbar: theme.mixins.toolbar,
}));

export default function AdminSidebar(props: any) {
  const classes = useStyles(props);
  const [memberTabOpen, setOpen] = React.useState(false);
  const [dictionaryCollapse, setDictionaryCollapse] = React.useState(false);
  const [settingCollapseOpen, setSettingCollapseOpen] = React.useState(false);

  const { open, location } = props;

  function handleOpenMemberTabs() {
    setOpen(!memberTabOpen);
  }

  function handleOpenDictionaryCollapse() {
    setDictionaryCollapse(!dictionaryCollapse);
  }

  function handleOpenSettingsCollapse() {
    setSettingCollapseOpen(!settingCollapseOpen);
  }

  function isActive(view) {
    if (!view || !location || !location.pathname) {
      return false;
    }
    return view === location.pathname;
  }

  return (
    <Drawer
      variant="permanent"
      className={clsx(classes.drawer, {
        [classes.drawerOpen]: open,
        [classes.drawerClose]: !open,
      })}
      classes={{
        paper: clsx({
          [classes.drawerOpen]: open,
          [classes.drawerClose]: !open,
        }),
      }}
      open={open}
    >
      <div className={classes.toolbar} />
      <Divider />
      <List>
        <ListItem
          selected={isActive("/provider")}
          button
          key="Providers"
          component={Link}
          to="/provider"
        >
          <ListItemIcon>
            <ProviderIcon />
          </ListItemIcon>
          <ListItemText primary="Поставщики" />
        </ListItem>
        <ListItem
          selected={isActive("/users")}
          key="GarageUsers"
          button
          component={Link}
          to="/users"
        >
          <ListItemIcon>
            <GroupIcon />
          </ListItemIcon>
          <ListItemText primary="Пользователи" />
        </ListItem>
        {/* <ListItem button onClick={handleOpenMemberTabs}>
          <ListItemIcon>
            <GroupIcon />
          </ListItemIcon>
          <ListItemText primary="Пользователи" />
          {memberTabOpen ? <ExpandLess /> : <ExpandMore />}
        </ListItem>
        <Collapse in={memberTabOpen} timeout="auto" unmountOnExit>
         <ListItem
            selected={isActive("/admin")}
            button
            key="Admins"
            component={Link}
            to="/admin"
            className={clsx({ [classes.nested]: open })}
          >
            <ListItemIcon>
              <FiberManualRecordIcon />
            </ListItemIcon>
            <ListItemText primary="Администраторы" />
          </ListItem>
          <ListItem
            button
            selected={isActive("/motorist")}
            key="Motorist"
            component={Link}
            to="/motorist"
            className={clsx({ [classes.nested]: open })}
          >
            <ListItemIcon>
              <FiberManualRecordIcon />
            </ListItemIcon>
            <ListItemText primary="Автовладельцы" />
          </ListItem>
          <ListItem
            button
            selected={isActive("/garage-user")}
            key="GarageUsers"
            component={Link}
            to="/garage-user"
            className={clsx({ [classes.nested]: open })}
          >
            <ListItemIcon>
              <FiberManualRecordIcon />
            </ListItemIcon>
            <ListItemText primary="Пол-тели гаража" />
          </ListItem>
        </Collapse> */}

        <ListItem button onClick={handleOpenDictionaryCollapse}>
          <ListItemIcon>
            <DictionaryIcon />
          </ListItemIcon>
          <ListItemText primary="Справочники" />
          {dictionaryCollapse ? <ExpandLess /> : <ExpandMore />}
        </ListItem>
        <Collapse in={dictionaryCollapse} timeout="auto" unmountOnExit>
          <ListItem
            selected={isActive("/reference/expense-type")}
            button
            key="Vehicles"
            component={Link}
            className={clsx({ [classes.nested]: open })}
            to="/reference/expense-type"
          >
            <ListItemIcon>
              <ExpenseTypeIcon />
            </ListItemIcon>
            <ListItemText primary="Затраты" />
          </ListItem>

          <ListItem
            button
            selected={isActive("/reference/make")}
            key="Makes"
            component={Link}
            className={clsx({ [classes.nested]: open })}
            to="/reference/make"
          >
            <ListItemIcon>
              <BusinessCenterIcon />
            </ListItemIcon>
            <ListItemText primary="Марки" />
          </ListItem>

          <ListItem
            button
            selected={isActive("/reference/oil-station")}
            key="FuelTypes"
            component={Link}
            className={clsx({ [classes.nested]: open })}
            to="/reference/oil-station"
          >
            <ListItemIcon>
              <LocalGasStationIcon />
            </ListItemIcon>
            <ListItemText primary="Топливо" />
          </ListItem>

          <ListItem
            button
            key="Services"
            selected={isActive("/reference/auto-service")}
            component={Link}
            className={clsx({ [classes.nested]: open })}
            to="/reference/auto-service"
          >
            <ListItemIcon>
              <BuildIcon />
            </ListItemIcon>
            <ListItemText primary="Сервисы" />
          </ListItem>

        </Collapse>

        <Divider />

        <ListItem
            button
            selected={isActive("/settings/commission")}
            key="Commission"
            component={Link}
            to="/settings/commission"
        >
          <ListItemIcon>
            <FiberManualRecordIcon />
          </ListItemIcon>
          <ListItemText primary="Комиссия" />
        </ListItem>

        <Divider />

        <ListItem
            button key="Settings"
            onClick={handleOpenSettingsCollapse}
            component={Link}
            to="/settings">
          <ListItemIcon>
            <GroupIcon />
          </ListItemIcon>
          <ListItemText primary="Настройки" />

        </ListItem>


          {/* <ListItem
            selected={isActive("/admin")}
            button
            key="Настройки"
            component={Link}
            to="/settings"
            className={clsx({ [classes.nested]: open })}
          >
            <ListItemIcon>
              <FiberManualRecordIcon />
            </ListItemIcon>
            <ListItemText primary="Приложение" />
          </ListItem> */}

        {/* <Tooltip title="Профиль " aria-label="Профиль " placement="bottom">
          <ListItem button key="Logout" component={Link} to="/profile">
            <ListItemIcon>
              <AccountCircleIcon />
            </ListItemIcon>
            <ListItemText primary="Профиль " />
          </ListItem>
        </Tooltip> */}
        {/* <Tooltip title="Выход" aria-label="Выход" placement="bottom">
                    <ListItem button key="Logout" onClick={props.logout}>
                        <ListItemIcon><ExitToAppIcon/></ListItemIcon>
                        <ListItemText primary="Logout"/>
                    </ListItem>
                </Tooltip> */}
      </List>
      <Divider />
    </Drawer>
  );
}
