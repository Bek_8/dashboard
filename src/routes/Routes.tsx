import React from "react";
import {useSelector} from "react-redux";
import {Redirect, Route, Switch} from "react-router-dom";
import LoginPage from "../views/login/LoginPage";
import {createSelector} from "reselect";
import AdminRoutes from "./AdminRoutes";
import GarageAdminRoutes from "./GarageAdminRoutes";
import AuthRoute from "../utils/AuthRoute";

export const Routes = () => {
    const user = useSelector(createSelector(
        (state: any) => state.auth,
        (stateSection: any) => stateSection.user
    ))
    const isAdmin = user && user.userRole === "ADMIN"
    const isGarageAdmin = user && user.userRole === "GARAGE_ADMIN"
    const isLoggedIn = Boolean(user?.id);
    return (
        <Switch>
            <Route
                exact
                path="/login"
                component={LoginPage}
            />

            {isAdmin ? <AdminRoutes/> :
                isGarageAdmin ? <GarageAdminRoutes/> : null}

            {!isLoggedIn && <Redirect to='/login'/>}
        </Switch>
    )
}
