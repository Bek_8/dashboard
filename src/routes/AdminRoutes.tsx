import React from "react";
import { Redirect, Route, Switch } from "react-router-dom";
import AdminUserListContainer from "../views/admin-users/AdminUserListContainer";
import AdminUserAddContainer from "../views/admin-users/AdminUserAddContainer";
import AdminUserEditContainer from "../views/admin-users/AdminUserEditContainer";
import GarageUserListView from "../views/garage-user/GarageUserListView";
import GarageUserAddContainer from "../views/garage-user/GarageUserAddContainer";
import MotoristListView from "../views/motorist/MotoristListView";
import ExpenseTypeListContainer from "../views/references/expenseTypes/ExpenseTypeListContainer";
import ExpenseTypeAddContainer from "../views/references/expenseTypes/ExpenseTypeAddContainer";
import ExpenseTypeEditContainer from "../views/references/expenseTypes/ExpenseTypeEditContainer";
import AutoServiceListContainer from "../views/references/services/AutoServiceListContainer";
import AutoServiceAddContainer from "../views/references/services/AutoServiceAddContainer";
import AutoServiceEditContainer from "../views/references/services/AutoServiceEditContainer";
import AutoServiceChildAddContainer from "../views/references/services/AutoServiceChildAddContainer";
import AutoServiceChildEditContainer from "../views/references/services/AutoServiceChildEditContainer";
import MakeListContainer from "../views/references/makeModels/MakeListContainer";
import MakeAddContainer from "../views/references/makeModels/MakeAddContainer";
import MakeEditContainer from "../views/references/makeModels/MakeEditContainer";
import MakeChildAddContainer from "../views/references/makeModels/MakeChildAddContainer";
import MakeChildEditContainer from "../views/references/makeModels/MakeChildEditContainer";
import OilStationListContainer from "../views/references/fuelTypes/OilStationListContainer";
import OilStationAddContainer from "../views/references/fuelTypes/OilStationAddContainer";
import OilStationEditContainer from "../views/references/fuelTypes/OilStationEditContainer";
import OilStationChildAddContainer from "../views/references/fuelTypes/OilStationChildAddContainer";
import OilStationChildEditContainer from "../views/references/fuelTypes/OilStationChildEditContainer";
import ShopView from "../views/shop/ShopVIew";
import ProviderListContainer from "../views/provider/ProviderListContainer";
import ProviderAddContainer from "../views/provider/ProviderAddContainer";
import ProviderEditContainer from "../views/provider/ProviderEditContainer";
import DepositAddContainer from "../views/provider/DepositAddContainer";
import DepositEditContainer from "../views/provider/DepositEditContainer";
import ProviderDepositHistoryContainer from "../views/provider/ProviderDepositHistoryContainer";
import ProviderServiceAddContainer from "../views/provider/ProviderServiceAddContainer";
import ProviderServiceEditContainer from "../views/provider/ProviderServiceEditContainer";
import ProviderTechnicianAddContainer from "../views/provider/ProviderTechnicianAddContainer";
import ProviderTechnicianEditContainer from "../views/provider/ProviderTechnicianEditContainer";
import SettingsView from "../views/settings/SettingsView";
import VehicleListVIew from "../views/vehicle/VehicleListVIew";
import MakeListView from "../views/make/MakeListView";
import MakeEditView from "../views/make/MakeEditView";
import ModelEditView from "../views/make/model/ModelEditView";
import FuelTypeListView from "../views/fuel/FuelTypeListView";
import NotFoundView from "../views/other/NotFoundView";
import PrivateRoute from "../utils/PrivateRoute";
import LoginPage from "../views/login/LoginPage";
import DashboardLayout from "../layouts/AdminLayout";
import CommissionListContainer from "../views/settings/commissioins/CommissionListContainer";
import CommissionAddContainer from "../views/settings/commissioins/CommissionAddContainer";
import CommissionEditContainer from "../views/settings/commissioins/CommissionEditContainer";
import UserProfileContainer from "../views/garage_admin/UserProfileContainer";
import GarageUserEditContainer from "../views/garage-user/GarageUserEditContainer";

function AdminRoutes(props) {
  return (
    <DashboardLayout>
      <Switch>
        <PrivateRoute
          exact
          path={["/", "/provider"]}
          component={ProviderListContainer}
        />
        <PrivateRoute
          exact
          path="/provider/add"
          component={ProviderAddContainer}
        />
        <PrivateRoute
          exact
          path="/provider/:providerId?"
          component={ProviderEditContainer}
        />
        <PrivateRoute
          exact
          path="/provider/:providerId/deposit/add"
          component={DepositAddContainer}
        />
        <PrivateRoute
          exact
          path="/provider/:providerId/deposit/:id"
          component={DepositEditContainer}
        />
        <PrivateRoute
          exact
          path="/provider/:providerId/deposit-history"
          component={ProviderDepositHistoryContainer}
        />
        <PrivateRoute
          exact
          path="/provider/:providerId/service/add"
          component={ProviderServiceAddContainer}
        />
        <PrivateRoute
          exact
          path="/provider/:providerId/service/:id"
          component={ProviderServiceEditContainer}
        />

        <PrivateRoute
          exact
          path="/provider/:providerId/technician/add"
          component={ProviderTechnicianAddContainer}
        />
        <PrivateRoute
          exact
          path="/provider/:providerId/technician/:id"
          component={ProviderTechnicianEditContainer}
        />
        {/* <Redirect to="/provider" /> */}
        <PrivateRoute
          exact
          path="/admin-users"
          component={AdminUserListContainer}
        />
        <PrivateRoute
          exact
          path="/admin-user/add"
          component={AdminUserAddContainer}
        />

        <PrivateRoute
          exact
          path="/admin-user/:id"
          component={AdminUserEditContainer}
        />

        <PrivateRoute exact path="/users" component={GarageUserListView} />
        <PrivateRoute
          exact
          path="/garage-user/add"
          component={GarageUserAddContainer}
        />
        <PrivateRoute
          exact
          path="/garage-user/:id"
          component={GarageUserEditContainer}
        />
        <PrivateRoute
          exact
          path="/motorist/:view?"
          component={MotoristListView}
        />

        <PrivateRoute
          exact
          path="/reference/expense-type"
          component={ExpenseTypeListContainer}
        />
        <PrivateRoute
          exact
          path="/reference/expense-type/add"
          component={ExpenseTypeAddContainer}
        />
        <PrivateRoute
          exact
          path="/reference/expense-type/:id"
          component={ExpenseTypeEditContainer}
        />

        <PrivateRoute
          exact
          path="/reference/auto-service"
          component={AutoServiceListContainer}
        />
        <PrivateRoute
          exact
          path="/reference/auto-service/add"
          component={AutoServiceAddContainer}
        />
        <PrivateRoute
          exact
          path="/reference/auto-service/:id"
          component={AutoServiceEditContainer}
        />
        <PrivateRoute
          exact
          path="/reference/auto-service/:parentId/child/add"
          component={AutoServiceChildAddContainer}
        />
        <PrivateRoute
          exact
          path="/reference/auto-service/:parentId/child/:id"
          component={AutoServiceChildEditContainer}
        />

        <PrivateRoute
          exact
          path="/reference/make"
          component={MakeListContainer}
        />
        <PrivateRoute
          exact
          path="/reference/make/add"
          component={MakeAddContainer}
        />
        <PrivateRoute
          exact
          path="/reference/make/:id"
          component={MakeEditContainer}
        />
        <PrivateRoute
          exact
          path="/reference/make/:parentId/child/add"
          component={MakeChildAddContainer}
        />
        <PrivateRoute
          exact
          path="/reference/make/:parentId/child/:id"
          component={MakeChildEditContainer}
        />

        <PrivateRoute
          exact
          path="/reference/oil-station"
          component={OilStationListContainer}
        />
        <PrivateRoute
          exact
          path="/reference/oil-station/add"
          component={OilStationAddContainer}
        />
        <PrivateRoute
          exact
          path="/reference/oil-station/:id"
          component={OilStationEditContainer}
        />
        <PrivateRoute
          exact
          path="/reference/oil-station/:parentId/child/add"
          component={OilStationChildAddContainer}
        />
        <PrivateRoute
          exact
          path="/reference/oil-station/:parentId/child/:id"
          component={OilStationChildEditContainer}
        />

        <PrivateRoute
          exact
          path={"/profile"}
          component={UserProfileContainer}
        />

        <PrivateRoute exact path="/settings" component={SettingsView} />
        <PrivateRoute
          exact
          path="/settings/commission"
          component={CommissionListContainer}
        />
        <PrivateRoute
          exact
          path="/settings/commission/add"
          component={CommissionAddContainer}
        />
        <PrivateRoute
          exact
          path="/settings/commission/:id"
          component={CommissionEditContainer}
        />
        <PrivateRoute
          exact
          path="/vehicle/:view?"
          component={VehicleListVIew}
        />
        <PrivateRoute exact path="/make" component={MakeListView} />
        <PrivateRoute
          exact
          path="/make/item/:view?/:tab?"
          component={MakeEditView}
        />
        <PrivateRoute
          exact
          path="/model/item/:make/:view?/:tab?/:modificationId?"
          component={ModelEditView}
        />
        <PrivateRoute
          exact
          path="/fuel-type/:view?"
          component={FuelTypeListView}
        />
        <PrivateRoute
            exact
            path="*"
            component={NotFoundView}
        />

        <Redirect to="/404" />
      </Switch>
    </DashboardLayout>
  );
}

export default AdminRoutes;
