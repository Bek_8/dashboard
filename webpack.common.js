const path = require('path');
const {CleanWebpackPlugin} = require('clean-webpack-plugin');
const HtmlWebpackPlugin = require('html-webpack-plugin');
const constants = require('./webpack.constants');

// module.exports = {
//     entry: {
//         app: './src/index.js',
//     },
//     plugins: [
//         new CleanWebpackPlugin(),
//         new HtmlWebpackPlugin({
//             title: 'Production',
//         }),
//     ],
//     output: {
//         filename: '[name].bundle.js',
//         path: path.resolve(__dirname, 'dist'),
//     },
// };

const common = {
    entry: './src/index.tsx',
    output: {
        filename: '[name]-[hash].bundle.js',
        path: path.resolve(__dirname, 'static'),
    },
    context: constants.ROOT,
    resolve: {
        // Add '.ts' and '.tsx' as resolvable extensions.
        extensions: [".ts", ".tsx", ".js", ".json"],
    },
    module: {
        rules: [

            {
                test: /\.(ttf|eot|woff2?)$/i,
                loader: 'file?name=[name].[ext]'
            },
            {
                test: /\.(jpe?g|jpg|png|gif|svg)$/i,
                loaders: [
                    'file?name=[name].[ext]'
                ]
            },

            {test: /\.html$/, loader: 'html', exclude: /backend\.html$/},

            // All files with a '.ts' or '.tsx' extension will be handled by 'ts-loader'.
            {
                test: /\.tsx?$/,
                exclude: /node_modules/,
                use: [
                    {
                        loader: 'ts-loader',
                    }
                ]
            },
        ]
    },
    plugins: [
        new CleanWebpackPlugin(),
        new HtmlWebpackPlugin({
            template: 'backend.html',
            title: 'Development',
        }),
    ],
};
module.exports = common;
