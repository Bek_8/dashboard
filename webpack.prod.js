const merge = require('webpack-merge');
const common = require('./webpack.common.js');
const constants = require('./webpack.constants.js');


common.output.path = constants.OUTPUT_PATH;
module.exports = merge(common, {
    mode: 'production',
});