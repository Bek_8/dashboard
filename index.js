const constants = require("./webpack/webpack.constants");

// const path = require("path");
process.env.DEBUG = "Autobio";
const debug = require("debug");

const webpack = require("webpack");

debug.enable("Express Server:*");
debug.enable("App:*");

const logger = debug("Express Server:*");
logger("Starting Express Server with %s environment", process.env.NODE_ENV);

if (process.env.NODE_ENV === "development") {
    runServer();
} else {
    logger("This script is used only as dev server, use 'npm run-script build' to build modules");
}

function runServer() {
    const config = require("./webpack/webpack.config");
    const express = require("express");

    const compiler = webpack(config);
    const app = express();

    app.use(require("webpack-dev-middleware")(compiler, {
        noInfo: false,
        stats: "minimal",
        // stats: {
        //     colors: true,
        //     source: false,
        // },
        headers: { "Access-Control-Allow-Origin": "*" },

        publicPath: config.output.publicPath
    }));

    app.use(require("webpack-hot-middleware")(compiler));

    app.listen(constants.DEV_PORT, constants.HOST, error => {
        if (error) {
            logger("Error: ", error);
        } else {
            logger("Hot Loader run on %s port", "8081");
        }
    });
}
//test