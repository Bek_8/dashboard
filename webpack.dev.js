const merge = require('webpack-merge');
const common = require('./webpack.common.js');
// const constants = require('./webpack.constants.js');

module.exports = merge(common, {
    mode: 'development',
    devtool: 'inline-source-map',
});