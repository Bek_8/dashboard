// import {CleanWebpackPlugin} from "clean-webpack-plugin";
const HtmlWebpackPlugin =  require("html-webpack-plugin");

const path = require('path');

// module.exports = function (env, argv) {

// default to the server configuration
const base = {
    entry: './src/index.tsx',
    output: {
        path: path.resolve(__dirname, 'build'),
        filename: '[name].js',
        publicPath: '/',
    },
    mode: 'development',
    // Enable sourcemaps for debugging webpack's output.
    devtool: 'cheap-module-eval-source-map',
    resolve: {
        // Add '.ts' and '.tsx' as resolvable extensions.
        extensions: [".ts", ".tsx", ".js", ".json"],
    },
    module: {
        rules: [

            {
                test: /\.(ttf|eot|woff2?)$/i,
                loader: 'file?name=[name].[ext]'
            },
            {
                test: /\.(jpe?g|jpg|png|gif|svg)$/i,
                loaders: [
                    'file?name=[name].[ext]'
                ]
            },

            {test: /\.html$/, loader: 'html', exclude: /backend\.html$/},

            // All files with a '.ts' or '.tsx' extension will be handled by 'ts-loader'.
            {
                test: /\.tsx?$/,
                exclude: /node_modules/,
                use: [
                    {
                        loader: 'ts-loader',
                    }
                ]
            },
        ]
    },
    devServer: {
        contentBase: './dist',
    },
    plugins: [
        // new CleanWebpackPlugin(),
        new HtmlWebpackPlugin({
            template: 'backend.html',
            title: 'Development',
        }),
    ],
};

// server-specific configuration
// if (env.platform === 'server') {
//     base.target = 'node';
// }
//
// // client-specific configurations
// if (env.platform === 'client') {
//     base.entry = '../src/index.tsx';
//     base.output.filename = '[name].js';
// }

module.exports = base;
// }